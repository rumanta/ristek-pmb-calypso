export const isLoading = state => state.taskPageReducer.get("isLoading");

export const isLoaded = state => state.taskPageReducer.get("isLoaded");

export const getTasks = state => state.taskPageReducer.get("tasks");

export const error = state => state.taskPageReducer.get("error");

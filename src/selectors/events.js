export const getInternalEvents = state => state.eventList.get("internalEvents");
export const getExternalEvents = state => state.eventList.get("externalEvents");
export const getAllEvents = state => state.eventList.get("allEvents");
export const isEventsLoaded = state => state.eventList.get("isLoaded");
export const isEventsLoading = state => state.eventList.get("isLoading");

export const getEvent = state => state.eventDetail.get("event");
export const isEventLoaded = state => state.eventDetail.get("isLoaded");
export const isEventLoading = state => state.eventDetail.get("isLoading");
export const getEventError = state => state.eventDetail.get("error");

export const getNotificationPageReducer = state =>
  state.notificationPageReducer.toJS();
export const getNotifications = state =>
  getNotificationPageReducer(state).notifications;
export const isNotificationsLoading = state =>
  getNotificationPageReducer(state).isLoading;
export const isNotificationsLoaded = state =>
  getNotificationPageReducer(state).isLoaded;
export const isRemoveNotificationLoading = state =>
  getNotificationPageReducer(state).isRemoveLoading;

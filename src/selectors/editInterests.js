export const getInterestList = state =>
  state.editProfileInterestsReducer.get("interestList");

export const isEditInterestLoading = state =>
  state.editProfileInterestsReducer.get("isLoading");

export const isEditInterestLoaded = state =>
  state.editProfileInterestsReducer.get("isLoaded");

export const getEditInterestError = state =>
  state.editProfileInterestsReducer.get("error");

export const getPostInterestResponse = state =>
  state.editProfileInterestsReducer.get("postInterestResponse");

export const getDeleteInterestResponse = state =>
  state.editProfileInterestsReducer.get("deleteInterestResponse");

export const getPatchInterestResponse = state =>
  state.editProfileInterestsReducer.get("patchInterestResponse");

export const getTokenGeneratorReducer = state =>
  state.tokenGeneratorReducer.toJS();
export const getToken = state => getTokenGeneratorReducer(state).token;
export const isTokenLoading = state =>
  getTokenGeneratorReducer(state).isLoading;
export const isTokenLoaded = state => getTokenGeneratorReducer(state).isLoaded;

export const getTokenInputReducer = state => state.tokenInputReducer.toJS();
export const getKenalan = state => getTokenInputReducer(state).kenalan;
export const isKenalanLoaded = state => getTokenInputReducer(state).isLoaded;
export const isKenalanLoading = state => getTokenInputReducer(state).isLoading;

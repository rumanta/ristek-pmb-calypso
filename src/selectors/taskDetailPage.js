export const isTaskLoading = state =>
  state.taskDetailPageReducer.get("isTaskLoading");

export const isTaskLoaded = state =>
  state.taskDetailPageReducer.get("isTaskLoaded");

export const task = state => state.taskDetailPageReducer.get("task");

export const error = state => state.taskDetailPageReducer.get("error");

export const isSubmissionLoading = state =>
  state.taskDetailPageReducer.get("isSubmissionLoading");

export const isSubmissionLoaded = state =>
  state.taskDetailPageReducer.get("isSubmissionLoaded");

export const submissions = state =>
  state.taskDetailPageReducer.get("submissions");

export const submissionResponse = state =>
  state.taskDetailPageReducer.get("submissionResponse");

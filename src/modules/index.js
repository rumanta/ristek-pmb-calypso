/* eslint-disable */
import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import globalReducer from "globalReducer";
import friendPageReducer from "containers/FriendPage/reducer";
import taskPageReducer from "containers/TaskPage/reducer";
import taskDetailPageReducer from "containers/TaskDetailPage/reducer";
import formFriendPageReducer from "containers/FormFriendPage/reducer";
import profilePageReducer from "containers/ProfilePage/reducer";
import editProfileReducer from "containers/EditProfilePage/reducer";
import announcementListReducer from "containers/AnnouncementListPage/reducer";
import eventListPageReducer from "containers/EventListPage/reducer";
import eventDetailPageReducer from "containers/EventDetailPage/reducer";
import announcementDetailPageReducer from "../containers/AnnouncementDetailPage/reducer";
import editProfileInterestsReducer from "containers/EditProfileInterestPage/reducer";
import tokenGeneratorReducer from "containers/TokenGenerator/reducer";
import tokenInputReducer from "containers/TokenInput/reducer";
import feedbackTabReducer from "containers/FeedbackTab/reducer";
import postStoryReducer from "containers/NewStoryPage/reducer";
import editStoryReducer from "containers/EditStoryPage/reducer";
import storyTabReducer from "containers/StoryTab/reducer";
import storyPageReducer from "containers/StoryPage/reducer";
import storyWidgetReducer from "containers/StoryWidget/reducer";
import notificationPageReducer from "containers/NotificationPage/reducer";

export default history =>
  combineReducers({
    router: connectRouter(history),
    friendPageReducer: friendPageReducer,
    formFriendPageReducer: formFriendPageReducer,
    global: globalReducer,
    taskPageReducer: taskPageReducer,
    taskDetailPageReducer: taskDetailPageReducer,
    profileReducer: profilePageReducer,
    editProfileReducer: editProfileReducer,
    announcementReducer: announcementListReducer,
    announcementDetail: announcementDetailPageReducer,
    eventList: eventListPageReducer,
    eventDetail: eventDetailPageReducer,
    editProfileInterestsReducer: editProfileInterestsReducer,
    tokenGeneratorReducer,
    tokenInputReducer,
    feedbackTab: feedbackTabReducer,
    storyPageReducer,
    storyTabReducer,
    storyWidgetReducer,
    postStoryReducer: postStoryReducer,
    editStoryReducer: editStoryReducer,
    notificationPageReducer
  });

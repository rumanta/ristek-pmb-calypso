import auth from "./auth";
import axios from "axios";
import { history } from "store";

// const API_PREFIX_URL = "http://45.77.169.104";
const API_PREFIX_URL = "https://pmb.novi-bot.online";
// const API_PREFIX_URL = "http://localhost:8000";

// if (process.env.NODE_ENV === "production") {
//   API_PREFIX_URL = "http://104.215.144.96/pmb-api";
// }

axios.interceptors.request.use(
  config => {
    const user = auth.loggedIn();
    if (user) {
      const token = user.token;

      if (token) {
        config.headers.Authorization = `JWT ${token}`;
      }
    }

    return config;
  },
  error => Promise.reject(error)
);

axios.interceptors.response.use(
  response => response,
  error => {
    const statusCode = error.response.status;
    const token = localStorage.pmb2019;

    if (statusCode === 401 && token) {
      auth.logout();
      history.push("/login");
    }

    return Promise.reject(error);
  }
);

export const loggingInApi = `${API_PREFIX_URL}/akun/login`;
export const loggingOutApi = `${API_PREFIX_URL}/akun/logout`;

export const fetchProfileApi = `${API_PREFIX_URL}/akun/profil/me`;
export const fetchGroupApi = `${API_PREFIX_URL}/akun/kelompok`;
export const postEditProfileApi = `${API_PREFIX_URL}/akun/profil/me`;
export const uploadPictureApi = `${API_PREFIX_URL}/situs/upload-file`;
export const fetchAnnouncementsApi = `${API_PREFIX_URL}/situs/announcements`;
export const fetchEventsApi = `${API_PREFIX_URL}/situs/events`;
export const fetchEventDetailApi = id => `${API_PREFIX_URL}/situs/events/${id}`;
export const fetchAnnouncementDetailApi = id =>
  `${API_PREFIX_URL}/situs/announcements/${id}`;
export const fetchServerTime = `${API_PREFIX_URL}/situs/server-time/`;

export const getTaskListApi = `${API_PREFIX_URL}/situs/tasks`;
export function getTaskApi(id) {
  return `${API_PREFIX_URL}/situs/tasks/${id}`;
}
export function taskSubmissionsApi(id) {
  return `${API_PREFIX_URL}/situs/tasks/${id}/submissions`;
}
export const uploadApi = `${API_PREFIX_URL}/situs/upload-file`;

export const getFeedbacks = `${API_PREFIX_URL}/situs/feedbacks`;
export const postCommentApi = `${API_PREFIX_URL}/situs/qnas`;

export const fetchFriendListApi = `${API_PREFIX_URL}/teman/kenalan`;
export const fetchFriendApi = id => `${API_PREFIX_URL}/teman/kenalan/${id}`;
export const fetchInterestApi = id =>
  `${API_PREFIX_URL}/akun/interests?user=${id}`;
export const fetchStatisticKenalanApi = `${API_PREFIX_URL}/teman/kenalan-user-statistics`;

export const editKenalanApi = id => `${API_PREFIX_URL}/teman/kenalan/${id}`;

export const interestApi = `${API_PREFIX_URL}/akun/interests`;
export function getUserInterestApi(id) {
  return `${API_PREFIX_URL}/akun/interests?user=${id}`;
}
export function deleteInterestApi(id) {
  return `${API_PREFIX_URL}/akun/interests/${id}`;
}
export function patchInterestApi(id) {
  return `${API_PREFIX_URL}/akun/interests/${id}`;
}
export const generateTokenApi = limit =>
  `${API_PREFIX_URL}/teman/token?count=${limit}`;
export const autoGenerateTokenApi = `${API_PREFIX_URL}/teman/token`;
export const postKenalanApi = `${API_PREFIX_URL}/teman/kenalan`;
export const deleteKenalanApi = id => `${API_PREFIX_URL}/teman/kenalan/${id}`;

export const getRecentStories = `${API_PREFIX_URL}/situs/stories`;
export const getMyStories = `${API_PREFIX_URL}/situs/stories?user=me`;
export const fetchStory = `${API_PREFIX_URL}/situs/stories`;
export const editStoryApi = id => `${API_PREFIX_URL}/situs/stories/${id}`;
export const deleteStoryApi = id => `${API_PREFIX_URL}/situs/stories/${id}`;
export const fetchNotificationsApi = `${API_PREFIX_URL}/situs/notifications`;
export const setNotificationAsViewedApi = id =>
  `${API_PREFIX_URL}/situs/notifications/${id}`;

export const fetchFindFriendsApi = query =>
  `${API_PREFIX_URL}/teman/find-friends?interest=${query}`;

import styled from "styled-components";

export const FormFriendPageStyle = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;

  .formHeader {
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-top: 1rem;
  }

  .formContainer {
    width: 55vw;
  }

  @media (max-width: 1216px) {
  }

  @media (max-width: 1024px) {
    .formContainer {
      width: 87.5vw;
    }
  }

  @media (max-width: 580px) {
  }

  @media (max-width: 480px) {
  }
`;

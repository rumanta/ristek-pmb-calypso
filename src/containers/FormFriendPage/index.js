/* eslint-disable consistent-return */
import React, { Component } from "react";
import { connect } from "react-redux";
import { push } from "connected-react-router";
import PropTypes from "prop-types";
import swal from "sweetalert";
import FullscreenSpinner from "components/LoadingFullscreen";
import Spinner from "components/Loading";
import EmptyCard from "components/EmptyCard";

import FriendCard from "../../components/FriendCard";
import Section from "../../components/Section";
import { FormFriendPageStyle } from "./style";

import {
  isEditSuccess,
  isEditPosting,
  isFriendLoading,
  isFriendLoaded,
  isInterestsLoading,
  isInterestsLoaded,
  friend,
  interests,
  savedFriendsCount,
  isLoadedSavedFriendsCount
} from "selectors/formFriendPage";
import {
  fetchFriend,
  editFriend,
  editApproveKenalan,
  clearSubmitSuccess,
  fetchSavedFriendCount,
  clearSavedFriendsCountIsLoaded
} from "./actions";

class FormFriendPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      description: "",
      rejection_message: "",
      status: ""
    };

    this.handleChange = this.handleChange.bind(this);
    this.submit = this.submit.bind(this);
    this.resize = this.resize.bind(this);
  }

  UNSAFE_componentWillReceiveProps() {
    if (this.props.friend) {
      if (this.props.friend.status === "NOT_FRIEND") {
        this.setState({
          description: "",
          rejection_message: ""
        });
      } else {
        this.setState({
          description: this.props.friend.description,
          rejection_message: ""
        });
      }
    }
  }

  componentDidUpdate() {
    if (this.props.isEditSuccess) {
      this.props.clearSubmitSuccess();
      this.props.push("/friends");
    }
    if (this.props.isUserMaba && !this.props.isLoadedSavedFriendsCount) {
      this.fetchSavedFriendsCountOnce();
    }
  }

  fetchSavedFriendsCountOnce = (() => {
    let executed = false;

    return () => {
      if (!executed) {
        executed = true;
        this.props.fetchSavedFriendCount();
      }
    };
  })();

  UNSAFE_componentWillMount() {
    this.resize();
  }

  componentDidMount() {
    window.addEventListener("resize", this.resize.bind(this));
    const id = this.props.match.params.id;
    this.props.fetchFriend(id, true);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.resize.bind(this));
    this.props.clearSavedFriendsCountIsLoaded();
  }

  resize = () => {
    this.setState({ isDesktop: window.innerWidth >= 1024 });
  };

  handleChange = event => {
    const target = event.target.value;
    if (this.props.isUserMaba) {
      this.setState({ description: target });
    } else {
      this.setState({ rejection_message: target });
    }
  };

  submit = () => {
    const id = this.props.match.params.id;
    const status = this.props.isUserMaba ? "PENDING" : false;

    if (this.props.isUserMaba) {
      if (
        !this.state.description ||
        (this.state.description && this.state.description.trim().length === 0)
      ) {
        return swal(
          "Error",
          "Please fill the description before sending friend request",
          "error"
        );
      }

      this.props.editFriend(id, this.state.description, status);
    } else {
      if (
        !this.state.rejection_message ||
        (this.state.rejection_message &&
          this.state.rejection_message.trim().length === 0)
      ) {
        return swal(
          "Error",
          "Rejection message cannot be empty. Please let your friend know what your reason",
          "error"
        );
      }
      this.props.editApproveKenalan(id, status, this.state.rejection_message);
    }
  };

  handleSaveKenalan = () => {
    const id = this.props.match.params.id;
    this.props.editFriend(id, this.state.description, "SAVED");
  };

  render() {
    const textDecider = () => {
      if (this.props.isUserMaba) {
        return this.state.description;
      }

      return this.state.rejection_message;
    };

    if (this.props.isEditPosting) {
      return <FullscreenSpinner />;
    }

    if (this.props.isFriendLoading || this.props.isInterestsLoading) {
      return <Spinner />;
    }

    if (
      !this.props.isFriendLoading &&
      !this.props.isInterestsLoading &&
      !this.props.isFriendLoaded &&
      !this.props.isInterestsLoaded
    ) {
      return <EmptyCard text="Friend not found" />;
    }

    return (
      <FormFriendPageStyle>
        <div className="formHeader">
          <Section title={"Edit Description"}>
            <div className="formContainer">
              <FriendCard
                friend={this.props.friend}
                editable={true}
                interests={this.props.interests}
                descriptionChange={this.handleChange}
                submit={this.submit}
                onSave={this.handleSaveKenalan}
                isUserMaba={this.props.isUserMaba}
                textValue={textDecider()}
                isEditMaba={this.props.isUserMaba}
                isDesktop={this.state.isDesktop}
                savedFriendsCount={this.props.savedFriendsCount}
              />
            </div>
          </Section>
        </div>
      </FormFriendPageStyle>
    );
  }
}

FormFriendPage.propTypes = {
  push: PropTypes.func,
  isUserMaba: PropTypes.bool,
  friend: PropTypes.object,
  interests: PropTypes.array,
  isEditSuccess: PropTypes.bool,
  isEditPosting: PropTypes.bool,
  isFriendLoading: PropTypes.bool,
  isInterestsLoading: PropTypes.bool,
  isFriendLoaded: PropTypes.bool,
  isInterestsLoaded: PropTypes.bool,
  editApproveKenalan: PropTypes.func,
  clearSubmitSuccess: PropTypes.func,
  editFriend: PropTypes.func,
  fetchFriend: PropTypes.func,
  match: PropTypes.object,
  savedFriendsCount: PropTypes.number,
  fetchSavedFriendCount: PropTypes.func,
  isLoadedSavedFriendsCount: PropTypes.bool,
  clearSavedFriendsCountIsLoaded: PropTypes.func
};

function mapStateToProps(state) {
  return {
    isEditSuccess: isEditSuccess(state),
    isEditPosting: isEditPosting(state),
    isFriendLoaded: isFriendLoaded(state),
    isFriendLoading: isFriendLoading(state),
    isInterestsLoaded: isInterestsLoaded(state),
    isInterestsLoading: isInterestsLoading(state),
    friend: friend(state),
    interests: interests(state),
    isUserMaba: state.profileReducer.get("isUserMaba"),
    savedFriendsCount: savedFriendsCount(state),
    isLoadedSavedFriendsCount: isLoadedSavedFriendsCount(state),
    clearSavedFriendsCountIsLoaded: clearSavedFriendsCountIsLoaded(state)
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchFriend: id => dispatch(fetchFriend(id, true)),
    editFriend: (id, description, status) =>
      dispatch(editFriend(id, description, status)),
    editApproveKenalan: (id, is_approved, rejection_message) =>
      dispatch(editApproveKenalan(id, is_approved, rejection_message)),
    clearSubmitSuccess: () => dispatch(clearSubmitSuccess()),
    push: url => dispatch(push(url)),
    fetchSavedFriendCount: () => dispatch(fetchSavedFriendCount()),
    clearSavedFriendsCountIsLoaded: () =>
      dispatch(clearSavedFriendsCountIsLoaded())
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FormFriendPage);

import { fromJS } from "immutable";
import {
  FETCH_FRIEND,
  FETCH_FRIEND_SUCCESS,
  FETCH_FRIEND_FAILED,
  FETCH_INTEREST,
  FETCH_INTEREST_SUCCESS,
  FETCH_INTEREST_FAILED,
  PATCH_FRIEND,
  PATCH_FRIEND_SUCCESS,
  PATCH_FRIEND_FAILED,
  PATCH_FRIEND_MABA,
  PATCH_FRIEND_SUCCESS_MABA,
  PATCH_FRIEND_FAILED_MABA,
  CLEAR_SUBMIT_SUCCESS,
  FETCH_SAVED_FRIENDS_COUNT,
  FETCH_SAVED_FRIENDS_COUNT_SUCCESS,
  FETCH_SAVED_FRIENDS_COUNT_FAILED,
  CLEAR_SAVED_FRIENDS_COUNT_IS_LOADED
} from "./constant";

const initialState = fromJS({
  error: null,
  isFriendLoaded: false,
  isFriendLoading: false,
  isInterestsLoaded: false,
  isInterestsLoading: false,
  isEditPosting: false,
  isEditSuccess: false,
  friend: null,
  interests: null,
  patchedFriend: null,
  isFetchingSavedFriendsCount: null,
  savedFriendsCount: null,
  isLoadedSavedFriendsCount: false
});

function formFriendPageReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_FRIEND:
      return state.set("isFriendLoading", true);
    case FETCH_INTEREST:
      return state.set("isInterestLoading", true);
    case FETCH_FRIEND_SUCCESS:
      return state
        .set("friend", action.payload)
        .set("isFriendLoading", false)
        .set("isFriendLoaded", true);
    case FETCH_INTEREST_SUCCESS:
      return state
        .set("interests", action.payload)
        .set("isInterestsLoading", false)
        .set("isInterestsLoaded", true);
    case FETCH_INTEREST_FAILED:
      return state
        .set("error", action.payload)
        .set("isInterestsLoading", false)
        .set("isInterestsLoaded", false);
    case FETCH_FRIEND_FAILED:
      return state
        .set("error", action.payload)
        .set("isFriendLoaded", false)
        .set("isFriendLoading", false);
    case PATCH_FRIEND:
      return state.set("isEditPosting", true);
    case PATCH_FRIEND_SUCCESS:
      return state.set("isEditSuccess", true).set("isEditPosting", false);
    case PATCH_FRIEND_FAILED:
      return state.set("isEditSuccess", false).set("isEditPosting", false);

    case PATCH_FRIEND_MABA:
      return state.set("isEditPosting", true);
    case PATCH_FRIEND_SUCCESS_MABA:
      return state
        .set("isEditSuccess", true)
        .set("isEditPosting", false)
        .set("patchedFriend", action.payload);
    case PATCH_FRIEND_FAILED_MABA:
      return state.set("isEditSuccess", false).set("isEditPosting", false);
    case CLEAR_SUBMIT_SUCCESS:
      return state.set("isEditSuccess", false).set("isEditPosting", false);
    case FETCH_SAVED_FRIENDS_COUNT:
      return state.set("isFetchingSavedFriendsCount", true);
    case FETCH_SAVED_FRIENDS_COUNT_SUCCESS:
      return state
        .set("isFetchingSavedFriendsCount", false)
        .set("isLoadedSavedFriendsCount", true)
        .set("savedFriendsCount", action.payload);
    case FETCH_SAVED_FRIENDS_COUNT_FAILED:
      return state
        .set("isFetchingSavedFriendsCount", false)
        .set("isLoadedSavedFriendsCount", false);
    case CLEAR_SAVED_FRIENDS_COUNT_IS_LOADED:
      return state
        .set("isLoadedSavedFriendsCount", false)
        .set("savedFriendsCount", null);
    default:
      return state;
  }
}

export default formFriendPageReducer;

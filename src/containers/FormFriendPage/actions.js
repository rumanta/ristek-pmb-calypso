import axios from "axios";
import swal from "sweetalert";
import {
  FETCH_FRIEND,
  FETCH_FRIEND_SUCCESS,
  FETCH_FRIEND_FAILED,
  FETCH_INTEREST,
  FETCH_INTEREST_SUCCESS,
  FETCH_INTEREST_FAILED,
  PATCH_FRIEND,
  PATCH_FRIEND_SUCCESS,
  PATCH_FRIEND_FAILED,
  PATCH_FRIEND_MABA,
  PATCH_FRIEND_SUCCESS_MABA,
  PATCH_FRIEND_FAILED_MABA,
  CLEAR_SUBMIT_SUCCESS,
  FETCH_SAVED_FRIENDS_COUNT,
  FETCH_SAVED_FRIENDS_COUNT_SUCCESS,
  FETCH_SAVED_FRIENDS_COUNT_FAILED,
  CLEAR_SAVED_FRIENDS_COUNT_IS_LOADED
} from "./constant";
import { UPDATE_APPROVED_KENALAN } from "../FriendPage/constants";

import {
  fetchInterestApi,
  fetchFriendApi,
  editKenalanApi,
  fetchFriendListApi
} from "api";

export function fetchFriend(id, isUserMaba) {
  return dispatch => {
    dispatch({ type: FETCH_FRIEND });
    dispatch({ type: FETCH_INTEREST });
    axios
      .get(fetchFriendApi(id))
      .then(response => {
        const fetchedFriendId = isUserMaba
          ? response.data.user_elemen.profil.user.id
          : response.data.user_maba.profil.user.id;
        dispatch({
          payload: response.data,
          type: FETCH_FRIEND_SUCCESS
        });

        return axios.get(fetchInterestApi(fetchedFriendId));
      })
      .then(response => {
        dispatch({
          payload: response.data,
          type: FETCH_INTEREST_SUCCESS
        });
      })
      .catch(error => {
        dispatch({
          payload: error,
          type: FETCH_FRIEND_FAILED
        });
        dispatch({
          payload: error,
          type: FETCH_INTEREST_FAILED
        });
      });
  };
}

export function editFriend(id, description, status) {
  return dispatch => {
    dispatch({ type: PATCH_FRIEND });

    axios
      .patch(editKenalanApi(id), {
        description,
        status
      })
      .then(response => {
        dispatch({
          payload: response.data,
          type: PATCH_FRIEND_SUCCESS
        });
        if (status === "SAVED") {
          swal("Success", "Your friend has been saved!", "success");
        }
        if (status === "PENDING") {
          swal("Success", "Your friend request has been sent!", "success");
        }
      })
      .catch(error => {
        dispatch({
          payload: error,
          type: PATCH_FRIEND_FAILED
        });
        swal("Error", "Oops! Something went wrong!", "error");
      });
  };
}

export function editApproveKenalan(id, is_approved, rejection_message) {
  return dispatch => {
    dispatch({ type: PATCH_FRIEND_MABA });

    axios
      .patch(editKenalanApi(id), {
        is_approved,
        rejection_message
      })
      .then(response => {
        dispatch({
          payload: response.data,
          type: PATCH_FRIEND_SUCCESS_MABA
        });
        // update state in FriendPage reducer if approval
        if (is_approved) {
          dispatch({
            payload: response.data,
            type: UPDATE_APPROVED_KENALAN
          });
        }
        swal({
          title: "Success!",
          text: is_approved
            ? "You have accepted a new friend request!"
            : "Your friend request has been rejected!",
          icon: "success",
          button: "ok"
        });
      })
      .catch(error => {
        dispatch({
          payload: error,
          type: PATCH_FRIEND_FAILED_MABA
        });
        swal("Error", "Oops! Something went wrong!", "error");
      });
  };
}

export function clearSubmitSuccess() {
  return dispatch => {
    dispatch({ type: CLEAR_SUBMIT_SUCCESS });
  };
}

export function fetchSavedFriendCount() {
  return dispatch => {
    dispatch({ type: FETCH_SAVED_FRIENDS_COUNT });

    axios
      .get(fetchFriendListApi)
      .then(response => {
        dispatch({
          payload: response.data
            .filter(friend => friend.status === "SAVED")
            .reduce(count => count + 1, 0),
          type: FETCH_SAVED_FRIENDS_COUNT_SUCCESS
        });
      })
      .catch(error => {
        dispatch({
          payload: error,
          type: FETCH_SAVED_FRIENDS_COUNT_FAILED
        });
        // swal("Error", "Failed to get saved friends count", "error");
      });
  };
}

export function clearSavedFriendsCountIsLoaded() {
  return dispatch => {
    dispatch({ type: CLEAR_SAVED_FRIENDS_COUNT_IS_LOADED });
  };
}

import axios from "axios";
import {
  FETCH_ANNOUNCEMENTS,
  FETCH_ANNOUNCEMENTS_SUCCESS,
  FETCH_ANNOUNCEMENTS_FAILED
} from "./constants";

import { fetchAnnouncementsApi } from "api";

export function fetchAnnouncements() {
  return dispatch => {
    dispatch({ type: FETCH_ANNOUNCEMENTS });
    axios
      .get(fetchAnnouncementsApi)
      .then(response => {
        dispatch({
          payload: response.data,
          type: FETCH_ANNOUNCEMENTS_SUCCESS
        });
      })
      .catch(error => {
        dispatch({
          payload: error,
          type: FETCH_ANNOUNCEMENTS_FAILED
        });
      });
  };
}

/* eslint-disable array-callback-return */
import React, { Component } from "react";
import { AnnouncementListContainer } from "./style";
import PropTypes from "prop-types";
import { push } from "connected-react-router";
import { connect } from "react-redux";
import AnnouncementListCard from "components/AnnouncementListCard";
import PaginationContainer from "components/PaginationContainer";

import { fetchAnnouncements } from "./actions";
import {
  getAnnouncements,
  isAnnouncementsLoaded,
  isAnnouncementsLoading
} from "selectors/announcements";

class AnnouncementListPage extends Component {
  componentDidMount() {
    this.props.fetchAnnouncements();
  }

  renderAnnouncementListCards = () => {
    const announcementCards = [];

    if (this.props.announcements) {
      this.props.announcements.map(announcement => {
        announcementCards.push(
          <AnnouncementListCard
            key={`announcement-list-card-${announcement.id}`}
            title={announcement.title}
            date={announcement.date}
            onClick={() => this.props.push(`/announcements/${announcement.id}`)}
          />
        );
      });
    }

    return announcementCards;
  };

  render() {
    return (
      <AnnouncementListContainer>
        <div className="announcement-list-container">
          <PaginationContainer
            header={<p className="title">Announcements</p>}
            isLoading={this.props.isLoadingAnnouncements}
            emptyText={"There isn't any Announcement"}
          >
            {this.renderAnnouncementListCards()}
          </PaginationContainer>
        </div>
      </AnnouncementListContainer>
    );
  }
}

AnnouncementListPage.propTypes = {
  announcements: PropTypes.array.isRequired,
  isLoadedAnnouncements: PropTypes.bool.isRequired,
  isLoadingAnnouncements: PropTypes.bool.isRequired,
  fetchAnnouncements: PropTypes.func.isRequired,
  push: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  announcements: getAnnouncements(state),
  isLoadedAnnouncements: isAnnouncementsLoaded(state),
  isLoadingAnnouncements: isAnnouncementsLoading(state)
});

const mapDispatchToProps = dispatch => ({
  fetchAnnouncements: () => dispatch(fetchAnnouncements()),
  push: url => dispatch(push(url))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AnnouncementListPage);

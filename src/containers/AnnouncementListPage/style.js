import styled from "styled-components";

export const AnnouncementListContainer = styled.div`
  .announcement-list-container {
    .title {
      font-size: 20px;
    }
  }

  @media (max-width: 600px) {
    .announcement-list-container {
      .title {
        font-size: 16px;
      }
    }
  }
`;

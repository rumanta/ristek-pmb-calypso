import axios from "axios";

import {
  PATCH_LINK,
  PATCH_LINK_SUCCESS,
  PATCH_LINK_FAILED,
  RESET_STATE
} from "./constants";

import { editStoryApi } from "api";

import swal from "sweetalert";

export function resetState() {
  return dispatch =>
    dispatch({
      type: RESET_STATE
    });
}

export function editStory(obj, id) {
  return dispatch => {
    dispatch({
      type: PATCH_LINK
    });
    axios
      .patch(editStoryApi(id), obj)
      .then(response => {
        dispatch({
          payload: response.data,
          type: PATCH_LINK_SUCCESS
        });
        swal("Successful", "Your story has been edited!", "success");
      })
      .catch(error => {
        dispatch({
          payload: error,
          type: PATCH_LINK_FAILED
        });
        swal(
          "Error",
          error.message === "Request failed with status code 406"
            ? `Failed to edit your story, URL is not a valid Medium Post URL`
            : error.message,
          "error"
        );
      });
  };
}

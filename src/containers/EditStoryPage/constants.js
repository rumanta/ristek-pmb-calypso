export const PATCH_LINK = "src/EditStoryPage/PATCH_LINK";

export const PATCH_LINK_SUCCESS = "src/EditStoryPage/PATCH_LINK_SUCCESS";

export const PATCH_LINK_FAILED = "src/EditStoryPage/PATCH_LINK_FAILED";

export const RESET_STATE = "src/EditStoryPage/RESET_STATE";

import {
  PATCH_LINK,
  PATCH_LINK_SUCCESS,
  PATCH_LINK_FAILED,
  RESET_STATE
} from "./constants";
import { fromJS } from "immutable";

const initialState = fromJS({
  error: null,
  isLoading: false,
  link: null,
  isSuccess: false
});

export default function editStoryReducer(state = initialState, action) {
  switch (action.type) {
    case PATCH_LINK:
      return state.set("isLoading", true);
    case PATCH_LINK_SUCCESS:
      return state
        .set("isLoading", false)
        .set("link", action.payload)
        .set("isSuccess", true);
    case PATCH_LINK_FAILED:
      return state.set("error", action.payload).set("isLoading", false);
    case RESET_STATE:
      return state
        .set("error", null)
        .set("isLoading", false)
        .set("isSuccess", false);
    default:
      return state;
  }
}

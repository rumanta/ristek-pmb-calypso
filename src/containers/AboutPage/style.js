import styled from "styled-components";

export const AboutPageContainer = styled.div`
  min-height: 100vh;
  overflow-x: hidden;
  padding: 5rem 10rem 2rem 10rem;
  background-color: ${props => props.theme.colors.boneWhite};

  h1 {
    margin: 0.5rem 0;
    color: ${props => props.theme.colors.blue};
    font-weight: ${props => props.theme.weight.bold};
  }

  p {
    font-size: 18px;
    line-height: 24px;
    margin: 0.5rem 0;
  }

  .footer {
    background-image: url("assets/Footer.png");
    background-repeat: no-repeat;
  }

  .flex-column {
    display: flex;
    flex-direction: column;
  }

  .flex-row {
    display: flex;
    flex-direction: row;
  }

  .align-center {
    align-items: center;
  }

  .justify-center {
    justify-content: center;
  }

  .logo-pmb {
    height: 110px;
    margin: 1rem;
  }

  .content-filosofi {
    margin: 1rem 1.5rem;
  }

  .content-visi-misi {
    margin: 1.5rem;
  }

  .content-nilai {
    margin: 0 1.5rem;
  }

  .nilai-pmb {
    margin-right: 2rem;
    font-weight: ${props => props.theme.weight.bold};
  }

  @media screen and (max-width: 1024px) {
    min-height: auto;
    padding: 2rem;

    h1,
    nilai-pmb {
      text-align: center;
      margin-bottom: 1.5rem;
    }

    .logo-pmb {
      height: 80px;
      margin: 1rem;
    }

    .nilai-pmb {
      margin: 0.5rem 2rem;
    }

    .flex-row {
      flex-wrap: wrap;
    }

    .nilai-box {
      justify-content: center;
      align-items: center;
    }
  }
`;

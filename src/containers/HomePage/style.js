import styled from "styled-components";

export const HomePageStyle = styled.div`
  .token-container {
    display: none;
  }

  @media (max-width: 1023px) {
    .token-container {
      display: block;
    }
  }
`;

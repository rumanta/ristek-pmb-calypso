/* eslint-disable no-confusing-arrow */
import React, { Component } from "react";
import { connect } from "react-redux";
import { push } from "connected-react-router";
import PropTypes from "prop-types";

import TokenGenerator from "containers/TokenGenerator";
import TokenInput from "containers/TokenInput";
import AnnouncementListCard from "../../components/AnnouncementListCard";
import DisplayModifier from "../../components/DisplayModifier";
import EventListCard from "../../components/EventListCard";
import OrderModifierOnMobile from "../../components/OrderModifierOnMobile";
import Section from "../../components/Section";
import TaskListCard from "../../components/TaskListCard";
import Spinner from "components/Loading";

import { getUser, getTime, isMaba } from "selectors/user";
import {
  getAnnouncements,
  isAnnouncementsLoading
} from "selectors/announcements";
import { getTasks, isLoading } from "selectors/taskPage";
import { getAllEvents, isEventsLoading } from "selectors/events";
import { fetchNotificationsList } from "containers/NotificationPage/actions";

import { fetchAnnouncements } from "containers/AnnouncementListPage/actions";
import { fetchEvents } from "containers/EventListPage/actions";
import { fetchTasksList } from "containers/TaskPage/actions";

import { HomePageStyle } from "./style";
import EmptyCard from "components/EmptyCard";

class HomePage extends Component {
  getNearestUpcomingItems(
    arrayOfItems,
    funcToGetDatetimeString,
    numberOfItemsToGet
  ) {
    const mockServerTime = new Date();
    const serverTimeInMilli = new Date(mockServerTime).getTime();

    return JSON.parse(JSON.stringify(arrayOfItems))
      .filter(
        elem =>
          new Date(funcToGetDatetimeString(elem)).getTime() -
            serverTimeInMilli >=
          0
      )
      .sort(
        (elem1, elem2) =>
          new Date(funcToGetDatetimeString(elem1)).getTime() -
          new Date(funcToGetDatetimeString(elem2)).getTime()
      )
      .slice(0, numberOfItemsToGet);
  }

  getEvent = () =>
    this.getNearestUpcomingItems(this.props.events, elem => elem.date, 1);

  getTasks = () =>
    this.getNearestUpcomingItems(this.props.tasks, elem => elem.end_date, 3);

  getAnnouncements = () =>
    this.getNearestUpcomingItems(
      this.props.announcements,
      elem => elem.date,
      2
    );

  componentDidMount() {
    this.props.fetchAnnouncements();
    this.props.fetchEvents();
    this.props.fetchTasksList();
    this.props.fetchNotificationsList();
  }

  render() {
    return (
      <HomePageStyle>
        <div className="HomePage">
          <div className="token-container">
            <Section>
              {this.props.isMaba ? <TokenInput /> : <TokenGenerator />}
            </Section>
          </div>
          <OrderModifierOnMobile order={3}>
            <Section title="upcoming event">
              {this.props.isLoadingEvents ? (
                <Spinner /> // eslint-disable-next-line no-confusing-arrow
              ) : (
                (result =>
                  result.length !== 0 ? (
                    result.map(event => (
                      <EventListCard
                        key={event.id}
                        title={event.title}
                        date={new Date(event.date)}
                        place={event.place}
                        content={event.description}
                        onClick={() => this.props.push(`/events/${event.id}`)}
                      />
                    ))
                  ) : (
                    <EmptyCard text="There isn't any Upcoming Event" />
                  ))(this.getEvent())
              )}
            </Section>
          </OrderModifierOnMobile>
          <OrderModifierOnMobile order={2}>
            <Section title="ongoing task">
              {this.props.isLoadingEvents ? (
                <Spinner /> // eslint-disable-next-line no-confusing-arrow
              ) : (
                (result =>
                  result.length !== 0 ? (
                    result.map(task => (
                      <TaskListCard
                        key={task.id}
                        title={task.title}
                        date={new Date(task.end_date)}
                        displaySubmissionStatus={task.can_have_submission}
                        isSubmitted={task.attachment_link !== null}
                        onClick={() => this.props.push(`/tasks/${task.id}`)}
                      />
                    ))
                  ) : (
                    <EmptyCard text="There isn't any Ongoing Task" />
                  ))(this.getTasks())
              )}
            </Section>
          </OrderModifierOnMobile>
          <DisplayModifier showOn="desktop">
            <Section title="announcement">
              {this.props.isLoadingEvents ? (
                <Spinner /> // eslint-disable-next-line no-confusing-arrow
              ) : (
                (result =>
                  result.length !== 0 ? (
                    result.map(announcement => (
                      <AnnouncementListCard
                        key={announcement.id}
                        title={announcement.title}
                        date={announcement.date}
                        onClick={() =>
                          this.props.push(`/announcements/${announcement.id}`)
                        }
                      />
                    ))
                  ) : (
                    <EmptyCard text="There isn't any Announcements" />
                  ))(this.getAnnouncements())
              )}
            </Section>
          </DisplayModifier>
        </div>
      </HomePageStyle>
    );
  }
}

HomePage.propTypes = {
  user: PropTypes.shape().isRequired,
  push: PropTypes.func.isRequired,
  fetchAnnouncements: PropTypes.func.isRequired,
  fetchEvents: PropTypes.func.isRequired,
  fetchTasksList: PropTypes.func.isRequired,
  announcements: PropTypes.array.isRequired,
  isLoadingAnnouncements: PropTypes.bool.isRequired,
  isLoadingEvents: PropTypes.bool.isRequired,
  isLoadingTasks: PropTypes.bool.isRequired,
  events: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  tasks: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  isMaba: PropTypes.bool.isRequired,
  fetchNotificationsList: PropTypes.func
};

function mapStateToProps(state) {
  return {
    user: getUser(state),
    announcements: getAnnouncements(state),
    events: getAllEvents(state),
    tasks: getTasks(state),
    serverTime: getTime(state),
    isLoadingAnnouncements: isAnnouncementsLoading(state),
    isLoadingEvents: isEventsLoading(state),
    isLoadingTasks: isLoading(state),
    isMaba: isMaba(state)
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchAnnouncements: () => dispatch(fetchAnnouncements()),
    fetchEvents: () => dispatch(fetchEvents()),
    fetchTasksList: () => dispatch(fetchTasksList()),
    fetchNotificationsList: () => dispatch(fetchNotificationsList()),
    push: url => dispatch(push(url))
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomePage);

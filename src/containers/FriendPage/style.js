import styled from "styled-components";

export const FriendPageStyle = styled.div`
  .FriendPage__search {
    display: flex;
    position: relative;
    align-items: stretch;

    > div:nth-child(1) {
      flex-grow: 1;
    }

    > div:nth-child(2) {
      margin-left: 1rem;

      & > button {
        font-size: 1rem;
        height: calc(100% - 1rem);
        padding-left: 1.25rem;
        padding-right: 1.25rem;
      }
    }
  }

  .FriendPage__subheading {
    margin-bottom: 1rem;
    color: ${props => props.theme.colors.gray};
  }

  .FriendPage__controls__dropdowns {
    display: flex;
  }

  .FriendPage__controls__dropdowns > * {
    margin-right: 1rem;
    &:last-of-type {
      margin-right: 0;
    }

    @media screen and (min-width: ${props =>
        props.theme.sizes.desktop}) and (max-width: ${props =>
        props.theme.sizes.widescreen}) {
      margin-right: 0.5rem;
    }
  }

  .FriendPage__controls__pagination {
    width: min-content;
  }

  .FriendPage__event-tab-container {
    display: flex;
    flex-direction: row;
    margin-bottom: 1rem;
    border-bottom: 2px solid ${props => props.theme.colors.darkBlue};

    .FriendPage__tab {
      padding: 0.4rem 2.5rem;
      color: ${props => props.theme.colors.darkBlue};
      font-weight: ${props => props.theme.weight.bold};
      cursor: pointer;
    }

    .FriendPage__tab:hover {
      background: ${props => props.theme.colors.darkBlue};
      color: ${props => props.theme.colors.white};
    }

    .FriendPage__tab__active {
      color: ${props => props.theme.colors.white};
      background-color: ${props => props.theme.colors.darkBlue};
    }
  }

  .search-result {
    background: ${props => props.theme.colors.fieldGray};
    position: absolute;
    margin: 0;
    padding: 0.5rem;
    top: 100%;
    left: 0;
    right: 0;
    border: 1px solid ${props => props.theme.colors.lightGray};
    border-radius: 5px;
    overflow-y: auto;
    max-height: 10rem;
    cursor: pointer;
    z-index: 100;

    .search-result-item {
      padding: 0.5rem 0;
      background: ${props => props.theme.colors.fieldGray};
      width: 100%;
      list-style-type: none;
      border-bottom: 1px solid ${props => props.theme.colors.lightGray};
      z-index: 100;
    }

    .search-result-item:last-child {
      border-bottom: none;
      padding-top: 0.5rem;
    }

    .search-result-item:hover {
      background: ${props => props.theme.colors.gray};
    }
  }

  @media (max-width: ${props => props.theme.sizes.desktop}) {
    .FriendPage__tab {
      width: 50%;
    }
  }
`;

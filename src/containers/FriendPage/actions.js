import axios from "axios";
import {
  FETCH_FRIENDS,
  FETCH_FRIENDS_SUCCESS,
  FETCH_FRIENDS_FAILED,
  FETCH_INTERESTS,
  FETCH_INTERESTS_SUCCESS,
  FETCH_INTERESTS_FAILED,
  PROCESS_FRIEND_INTERESTS_SUCCESS,
  PROCESS_FRIEND_INTERESTS_FAILED,
  FETCH_STATISTIC_KENALAN,
  FETCH_STATISTIC_KENALAN_SUCCESS,
  FETCH_STATISTIC_KENALAN_FAILED,
  DELETE_KENALAN,
  DELETE_KENALAN_SUCCESS,
  DELETE_KENALAN_FAILED,
  DELETE_SAVED_KENALAN,
  CLEAR_STATISTICS_IS_LOADED,
  FETCH_FIND_FRIEND,
  FETCH_FIND_FRIEND_SUCCESS,
  FETCH_FIND_FRIEND_FAILED
} from "./constants";

import {
  fetchFriendListApi,
  fetchInterestApi,
  fetchStatisticKenalanApi,
  deleteKenalanApi,
  fetchFindFriendsApi
} from "api";
import swal from "sweetalert";

// returns array of unique user_elemen and user_maba ids
const getElemenAndMabaIds = friends => {
  const ids = [];
  friends.forEach(friend => {
    ids.push(friend.user_elemen.profil.user.id);
    ids.push(friend.user_maba.profil.user.id);
  });

  return [...new Set(ids)];
};

// returns dictionary of only interest information
const getInterestInfo = interestData => {
  const fieldsCopied = [
    "interest_name",
    "interest_category",
    "is_top_interest",
    "created_at",
    "updated_at"
  ];
  const result = {};
  fieldsCopied.forEach(
    fieldName => (result[fieldName] = interestData[fieldName])
  );

  return result;
};

// returns dictionary of user id to list of interests
const getUserIdToInterests = interestsData => {
  const userIdToInterests = {};
  interestsData.forEach(interestData => {
    const interest = getInterestInfo(interestData);

    // eslint-disable-next-line no-unused-expressions
    userIdToInterests[interestData.profil.user.id] === undefined
      ? (userIdToInterests[interestData.profil.user.id] = [interest])
      : userIdToInterests[interestData.profil.user.id].push(interest);
  });

  return userIdToInterests;
};

/*
  returns object similar to GET /teman/kenalan with user_elemen and user_maba
  having new field 'interests' containing list of interests
*/
const combineFriendsWithInterests = (friends, interests) => {
  const friendIdToFriendWithInterests = {};
  const userIdToInterests = getUserIdToInterests(interests);
  friends.forEach(friend => {
    [friend.user_elemen, friend.user_maba].forEach(profile => {
      const userId = profile.profil.user.id;
      profile.interests =
        userIdToInterests[userId] !== undefined
          ? userIdToInterests[userId]
          : [];
      // eslint-disable-next-line no-unused-expressions
      friendIdToFriendWithInterests[userId] === undefined &&
        (friendIdToFriendWithInterests[userId] = friend);
    });
  });

  // returns unique dictionary values
  return [...new Set(Object.values(friendIdToFriendWithInterests))];
};

export function fetchFriends() {
  let fetchFriendsResult = null;

  return dispatch => {
    dispatch({ type: FETCH_FRIENDS });
    dispatch({ type: FETCH_INTERESTS });

    axios
      .get(fetchFriendListApi)
      .then(response => {
        dispatch({
          payload: response.data,
          type: FETCH_FRIENDS_SUCCESS
        });
        fetchFriendsResult = response.data;

        return axios.all(
          getElemenAndMabaIds(response.data).map(id =>
            axios.get(fetchInterestApi(id))
          )
        );
      })
      .then(responses => {
        const interestsData =
          responses.length !== 0
            ? responses
                .map(response => response.data)
                .reduce((accumulator, value) => accumulator.concat(value))
            : [];
        dispatch({
          payload: interestsData,
          type: FETCH_INTERESTS_SUCCESS
        });
        dispatch({
          payload: combineFriendsWithInterests(
            fetchFriendsResult,
            interestsData
          ),
          type: PROCESS_FRIEND_INTERESTS_SUCCESS
        });
      })
      .catch(error => {
        dispatch({
          payload: error,
          type: FETCH_FRIENDS_FAILED
        });
        dispatch({
          payload: error,
          type: FETCH_INTERESTS_FAILED
        });
        dispatch({
          payload: error,
          type: PROCESS_FRIEND_INTERESTS_FAILED
        });
      });
  };
}

export function fetchStatisticKenalan() {
  return dispatch => {
    dispatch({ type: FETCH_STATISTIC_KENALAN });
    axios
      .get(fetchStatisticKenalanApi)
      .then(response => {
        dispatch({
          payload: response.data,
          type: FETCH_STATISTIC_KENALAN_SUCCESS
        });
      })
      .catch(error => {
        dispatch({
          payload: error,
          type: FETCH_STATISTIC_KENALAN_FAILED
        });
      });
  };
}

export function deleteKenalan(id) {
  return dispatch => {
    dispatch({ type: DELETE_KENALAN });
    axios
      .delete(deleteKenalanApi(id))
      .then(response => {
        dispatch({
          payload: response.data,
          type: DELETE_KENALAN_SUCCESS
        });
        dispatch({
          payload: id,
          type: DELETE_SAVED_KENALAN
        });
      })
      .catch(error => {
        dispatch({
          payload: error,
          type: DELETE_KENALAN_FAILED
        });
        swal({
          title: "Error",
          text: "Something went wrong :(",
          icon: "error",
          button: "ok"
        });
      });
  };
}
export function clearStatisticsIsLoaded() {
  return dispatch => {
    dispatch({ type: CLEAR_STATISTICS_IS_LOADED });
  };
}

export function fetchFindFriend(query) {
  return dispatch => {
    dispatch({ type: FETCH_FIND_FRIEND });
    axios
      .get(fetchFindFriendsApi(query))
      .then(response => {
        dispatch({
          payload: response.data,
          type: FETCH_FIND_FRIEND_SUCCESS
        });
      })
      .catch(error => {
        dispatch({
          payload: error,
          type: FETCH_FIND_FRIEND_FAILED
        });
      });
  };
}

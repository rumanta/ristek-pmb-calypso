/* eslint-disable no-case-declarations */
import { fromJS } from "immutable";
import {
  FETCH_FRIENDS,
  FETCH_FRIENDS_SUCCESS,
  FETCH_FRIENDS_FAILED,
  FETCH_INTERESTS,
  FETCH_INTERESTS_SUCCESS,
  FETCH_INTERESTS_FAILED,
  PROCESS_FRIEND_INTERESTS_SUCCESS,
  PROCESS_FRIEND_INTERESTS_FAILED,
  FETCH_STATISTIC_KENALAN,
  FETCH_STATISTIC_KENALAN_SUCCESS,
  FETCH_STATISTIC_KENALAN_FAILED,
  UPDATE_APPROVED_KENALAN,
  DELETE_SAVED_KENALAN,
  CLEAR_STATISTICS_IS_LOADED,
  FETCH_FIND_FRIEND,
  FETCH_FIND_FRIEND_SUCCESS,
  FETCH_FIND_FRIEND_FAILED
} from "./constants";

const initialState = fromJS({
  friends: null,
  findFriends: null,
  interests: null,
  friendsWithInterests: null,
  error: false,
  fetching: true,
  statistic: {},
  isLoadingStatistic: false,
  isLoadedStatistic: false,
  isUpdatingApproved: false
});

function friendPageReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_FRIENDS:
      return state.set("fetching", true);
    case FETCH_FRIENDS_SUCCESS:
      return state.set("friends", action.payload);
    case FETCH_FRIENDS_FAILED:
      return state.set("error", true).set("fetching", false);
    case FETCH_INTERESTS:
      return state.set("fetching", true);
    case FETCH_INTERESTS_SUCCESS:
      return state.set("interests", action.payload);
    case FETCH_INTERESTS_FAILED:
      return state.set("error", true).set("fetching", false);
    case PROCESS_FRIEND_INTERESTS_SUCCESS:
      return state
        .set("friendsWithInterests", action.payload)
        .set("fetching", false);
    case PROCESS_FRIEND_INTERESTS_FAILED:
      return state.set("error", true).set("fetching", false);
    case FETCH_STATISTIC_KENALAN:
      return state.set("isLoadingStatistic", true);
    case FETCH_STATISTIC_KENALAN_SUCCESS:
      const statistics = action.payload;

      statistics.sort(
        (stat1, stat2) =>
          new Date(stat1.kenalan_task.created_at) -
          new Date(stat2.kenalan_task.created_at)
      );

      return state
        .set("isLoadingStatistic", false)
        .set("statistic", statistics[statistics.length - 1])
        .set("isLoadedStatistic", true);
    case FETCH_STATISTIC_KENALAN_FAILED:
      return state
        .set("isLoadingStatistic", false)
        .set("error", action.payload);
    case UPDATE_APPROVED_KENALAN:
      // eslint-disable-next-line no-case-declarations
      const approvedFriend = action.payload;
      // eslint-disable-next-line no-case-declarations
      const updatedFriendsWithInterests = state
        .get("friendsWithInterests")
        // eslint-disable-next-line no-confusing-arrow
        .map(friend =>
          friend.id === approvedFriend.id
            ? Object.assign(friend, { status: approvedFriend.status })
            : friend
        );

      return state
        .set("isUpdatingApproved", true)
        .set("friendsWithInterests", updatedFriendsWithInterests)
        .set("isUpdatingApproved", false);
    case DELETE_SAVED_KENALAN:
      return state.set(
        "friendsWithInterests",
        state
          .get("friendsWithInterests")
          .filter(friend => friend.id !== action.payload)
      );
    case CLEAR_STATISTICS_IS_LOADED:
      return state
        .set("isLoadedStatistic", false)
        .set("isLoadingStatistic", false);
    case FETCH_FIND_FRIEND:
      return state.set("fetching", true);
    case FETCH_FIND_FRIEND_SUCCESS:
      return state.set("findFriends", action.payload).set("fetching", false);
    case FETCH_FIND_FRIEND_FAILED:
      return state.set("error", true).set("fetching", false);
    default:
      return state;
  }
}

export default friendPageReducer;

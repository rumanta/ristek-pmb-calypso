import axios from "axios";

import {
  FETCH_STORIES,
  FETCH_STORIES_SUCCESS,
  FETCH_STORIES_FAILED,
  DELETE_STORY,
  DELETE_STORY_SUCCESS,
  DELETE_STORY_FAILED
} from "./constants";

import { getRecentStories, getMyStories, deleteStoryApi } from "api";

export function fetchRecentStories() {
  return dispatch => {
    dispatch({
      type: FETCH_STORIES
    });
    axios
      .get(getRecentStories)
      .then(response => {
        dispatch({
          payload: response.data,
          type: FETCH_STORIES_SUCCESS
        });
      })
      .catch(error => {
        dispatch({
          payload: error,
          type: FETCH_STORIES_FAILED
        });
      });
  };
}

export function fetchMyStories() {
  return dispatch => {
    dispatch({
      type: FETCH_STORIES
    });
    axios
      .get(getMyStories)
      .then(response => {
        dispatch({
          payload: response.data,
          type: FETCH_STORIES_SUCCESS
        });
      })
      .catch(error => {
        dispatch({
          payload: error,
          type: FETCH_STORIES_FAILED
        });
      });
  };
}

export function deleteMyStory() {
  return dispatch => {
    dispatch({
      type: DELETE_STORY
    });
    axios
      .delete(deleteStoryApi)
      .then(response => {
        dispatch({
          payload: response.data,
          type: DELETE_STORY_SUCCESS
        });
      })
      .catch(error => {
        dispatch({
          payload: error,
          type: DELETE_STORY_FAILED
        });
      });
  };
}

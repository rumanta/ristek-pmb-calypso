import { fromJS } from "immutable";
import {
  FETCH_STORIES,
  FETCH_STORIES_SUCCESS,
  FETCH_STORIES_FAILED,
  DELETE_STORY,
  DELETE_STORY_SUCCESS,
  DELETE_STORY_FAILED
} from "./constants";

const initialState = fromJS({
  error: null,
  isLoading: false,
  stories: []
});

// const globalStoryReducer = (storyType) => (state = initialState, action) => {
//   switch (action.type) {
//     case FETCH_STORIES:
//       return state.set("isLoading", true);
//     case FETCH_STORIES_SUCCESS:
//       return state.set("stories", action.payload).set("isLoading", false);
//     case FETCH_STORIES_FAILED:
//       return state.set("error", action.payload).set("isLoading", false);
//     default:
//       return state;
//   }
// }

function globalStoryReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_STORIES:
      return state.set("isLoading", true);
    case FETCH_STORIES_SUCCESS:
      return state.set("stories", action.payload).set("isLoading", false);
    case FETCH_STORIES_FAILED:
      return state.set("error", action.payload).set("isLoading", false);
    case DELETE_STORY:
      return state.set("isLoading", true);
    case DELETE_STORY_SUCCESS:
      return state.set("stories", action.payload).set("isLoading", false);
    case DELETE_STORY_FAILED:
      return state.set("error", action.payload).set("isLoading", false);
    default:
      return state;
  }
}

export default globalStoryReducer;

/* eslint-disable no-confusing-arrow */
import React from "react";
import { GlobalStoryContainer } from "./style";
import { connect } from "react-redux";
import StoryProfileTabCard from "components/StoryProfileTabCard";
import StoryWidgetCard from "components/StoryWidgetCard";
import EmptyCard from "components/EmptyCard";
import PropTypes from "prop-types";
import { fetchRecentStories, fetchMyStories } from "./actions";
import Loading from "components/Loading";

const STORY_WIDGET = "STORY_WIDGET";
const STORY_TAB_PROFILE = "STORY_TAB_PROFILE";
const STORY_PAGE_HOME = "STORY_PAGE_HOME";

class GlobalStory extends React.Component {
  static get STORY_WIDGET() {
    return STORY_WIDGET;
  }

  static get STORY_TAB_PROFILE() {
    return STORY_TAB_PROFILE;
  }

  static get STORY_PAGE_HOME() {
    return STORY_PAGE_HOME;
  }

  constructor(props, context) {
    super(props, context);
    this.state = {
      cardsToShow: 2,
      expanded: false,
      stories: []
    };
    this.loadMore = this.loadMore.bind(this);
    this.produceCards = this.produceCards.bind(this);
  }

  loadMore(itemLength) {
    if (this.state.cardsToShow === 2) {
      this.setState({
        cardsToShow: itemLength,
        expanded: true
      });
    } else {
      this.setState({
        cardsToShow: 2,
        expanded: false
      });
    }
  }

  componentDidMount() {
    if (this.props.storyType === STORY_TAB_PROFILE) {
      this.props.fetchMyStories();
    } else {
      this.props.fetchRecentStories();
    }
  }

  splitObjects(object) {
    return [
      {
        id: object.id,
        title: object.medium.title,
        subtitle: object.medium.subtitle,
        thumbnail: `https://cdn-images-1.medium.com/max/1024/${
          object.medium.thumbnail.metadata.id
        }`,
        link: object.link,
        date: object.created_at
      },
      {
        profileImage: object.user.user_detail.profil.foto,
        name: object.user.user_detail.profil.nama_lengkap
      }
    ];
  }

  produceGeneralCards = (objects, storyType) =>
    objects.map((object, key) => {
      const splitObject = this.splitObjects(object);

      return (
        <StoryProfileTabCard
          key={key}
          entry={splitObject[0]}
          profile={splitObject[1]}
          isModifiable={storyType === STORY_TAB_PROFILE}
          push={this.props.push}
        />
      );
    });

  produceWidgetCards = objects =>
    objects.map((object, key) => {
      const splitObject = this.splitObjects(object);

      return (
        <StoryWidgetCard
          key={key}
          entry={splitObject[0]}
          profile={splitObject[1]}
        />
      );
    });

  produceCards = (objects, storyType) =>
    storyType === STORY_WIDGET
      ? this.produceWidgetCards(objects)
      : this.produceGeneralCards(objects, storyType);

  render() {
    const stories = this.props.stories;
    const { isLoading, push, storyType } = this.props;

    return isLoading ? (
      <Loading />
    ) : (
      <GlobalStoryContainer>
        {stories.length !== 0 ? (
          this.produceCards(stories.slice(0, this.state.cardsToShow), storyType)
        ) : (
          <EmptyCard text="There are no stories yet! Come back later :D" />
        )}
        {stories.length > 2 && !this.state.expanded && (
          <div className="loadMoreSection">
            {storyType === STORY_WIDGET ? (
              <p onClick={() => push(`/stories`)}>more stories</p>
            ) : (
              <p onClick={() => this.loadMore(stories.length)}>Load more...</p>
            )}
          </div>
        )}
      </GlobalStoryContainer>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    isLoading: state[ownProps.storyType].get("isLoading"),
    stories: state[ownProps.storyType].get("stories")
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchRecentStories: () => dispatch(fetchRecentStories()),
    fetchMyStories: () => dispatch(fetchMyStories())
  };
}

GlobalStory.propTypes = {
  storyType: PropTypes.string,
  push: PropTypes.func,
  fetchRecentStories: PropTypes.func.isRequired,
  fetchMyStories: PropTypes.func.isRequired,
  stories: PropTypes.array,
  isLoading: PropTypes.bool
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GlobalStory);

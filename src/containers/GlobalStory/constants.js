export const FETCH_STORIES = "src/GlobalStory/FETCH_STORIES";

export const FETCH_STORIES_SUCCESS = "src/GlobalStory/FETCH_STORIES_SUCCESS";

export const FETCH_STORIES_FAILED = "src/GlobalStory/FETCH_RECENT_FAILED";

export const DELETE_STORY = "src/GlobalStory/DELETE_STORY";

export const DELETE_STORY_SUCCESS = "src/GlobalStory/DELETE_STORY_SUCCESS";

export const DELETE_STORY_FAILED = "src/GlobalStory/DELETE_STORY_FAILED";

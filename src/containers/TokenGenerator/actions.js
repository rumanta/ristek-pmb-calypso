import axios from "axios";
import swal from "sweetalert";
import {
  GENERATE_TOKEN,
  GENERATE_TOKEN_SUCCESS,
  GENERATE_TOKEN_FAILED
} from "./constants";
import { generateTokenApi, autoGenerateTokenApi } from "api";

const TOKEN_NAME_LOCAL_STORAGE = "generated_token_pmb_2019";

export function generateToken(limit) {
  return dispatch => {
    dispatch({ type: GENERATE_TOKEN });
    axios
      .get(generateTokenApi(limit))
      .then(response => {
        dispatch({
          payload: response.data,
          type: GENERATE_TOKEN_SUCCESS
        });
        insertTokenToLocalStorage(response.data);
        swal(
          "Success",
          `You can share your generated token up to ${limit} new friends!`,
          "success"
        );
      })
      .catch(error => {
        dispatch({
          payload: error,
          type: GENERATE_TOKEN_FAILED
        });
        swal(
          "Error",
          "Oops! There is something wrong. Please try again later",
          "error"
        );
      });
  };
}

export function autoGenerateToken() {
  return dispatch => {
    dispatch({ type: GENERATE_TOKEN });
    axios
      .get(autoGenerateTokenApi)
      .then(response => {
        dispatch({
          payload: response.data,
          type: GENERATE_TOKEN_SUCCESS
        });
        insertTokenToLocalStorage(response.data);
      })
      .catch(error => {
        dispatch({
          payload: error,
          type: GENERATE_TOKEN_FAILED
        });
      });
  };
}

const insertTokenToLocalStorage = token => {
  localStorage[TOKEN_NAME_LOCAL_STORAGE] = JSON.stringify(token);
};

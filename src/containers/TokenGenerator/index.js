/* eslint-disable no-shadow */
import React, { Component } from "react";
import { TokenGeneratorContainer } from "./style";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import moment from "moment-timezone";
import getLocalTime from "utils/localTime";
import { generateToken, autoGenerateToken } from "./actions";
import { getToken, isTokenLoaded, isTokenLoading } from "selectors/tokens";
import LoadingFullscreen from "components/LoadingFullscreen";

import TokenGeneratedDefault from "components/TokenComponents/TokenGeneratedDefault";
import TokenGeneratorField from "components/TokenComponents/TokenGeneratorField";

const TOKEN_NAME_LOCAL_STORAGE = "generated_token_pmb_2019";

class TokenGenerator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      countDown: 0,
      time: getLocalTime(),
      token: "------",
      isTokenLoaded: false
    };
    this.timer = {};
  }

  componentDidMount() {
    this.serverTime = setInterval(() => this.tick(), 1000);

    // Check existing token in localstorage
    const token = this.retrieveTokenFromLocalStorage();

    if (!token) {
      this.props.autoGenerateToken();
    }

    // Start countdown for the following token if it exists
    if (
      token &&
      !this.isTokenExpired(token.end_time) &&
      !this.state.isTokenLoaded
    ) {
      this.setState({
        token: token.token,
        isTokenLoaded: true
      });
      this.startTimer(token.end_time);
    } else {
      this.removeTokenFromLocalStorage();
    }
  }

  componentWillUnmount() {
    clearInterval(this.serverTime);
    clearInterval(this.timer);
  }

  // static getDerivedStateFromProps(nextProps, prevState) {
  //   const { token, isTokenLoaded } = nextProps;
  //   if (token !== prevState.token && !isEmpty(token)) {
  //     TokenGenerator.startTimer(token.end_time);

  //     return {
  //       token: token.token,
  //       isTokenLoaded
  //     };
  //   }

  //   return null;
  // }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (!this.props.token && nextProps.token) {
      this.startTimer(nextProps.token.end_time);
      this.setState({
        token: nextProps.token.token,
        isTokenLoaded: true
      });
    } else if (
      this.props.token &&
      nextProps.token &&
      this.state.countDown === 0 &&
      this.props.token.end_time !== nextProps.token.end_time
    ) {
      this.setState({
        token: nextProps.token.token,
        isTokenLoaded: true
      });
      this.startTimer(nextProps.token.end_time);
    }
  }

  tick = () => {
    this.setState({
      time: getLocalTime()
    });
  };

  isTokenExpired = expiredTime => {
    const expired = new Date(expiredTime);
    const now = new Date(this.state.time);
    // const now = this.props.serverTime || new Date();

    if (expired - now <= 0) {
      return true;
    }

    return false;
  };

  removeTokenFromLocalStorage = () => {
    localStorage.removeItem(TOKEN_NAME_LOCAL_STORAGE);
  };

  retrieveTokenFromLocalStorage = () => {
    const token = localStorage[TOKEN_NAME_LOCAL_STORAGE];
    if (token) {
      return JSON.parse(localStorage[TOKEN_NAME_LOCAL_STORAGE]);
    }

    return null;
  };

  calculateTimeDifference = end => {
    // const now = moment(this.props.serverTime);
    const now = moment(this.state.serverTime);
    const expired = moment(new Date(end));

    return expired.diff(now, "seconds");
  };

  startTimer = expiredTime => {
    this.timer = setInterval(() => {
      const countDown = this.calculateTimeDifference(expiredTime);
      if (countDown === 0) {
        clearInterval(this.timer);
        this.setState({ isTokenLoaded: false });
        this.removeTokenFromLocalStorage();
      }
      this.setState({ countDown });
    }, 1000);
  };

  handleGenerateToken = limit => {
    this.props.generateToken(limit);
  };

  render() {
    const { countDown, token, isTokenLoaded } = this.state;
    const currentPercentage = this.state.countDown / 3;

    if (this.props.isTokenLoading) {
      return <LoadingFullscreen />;
    }

    return (
      <TokenGeneratorContainer>
        {isTokenLoaded ? (
          <TokenGeneratedDefault
            time={countDown}
            token={token}
            percentage={currentPercentage}
          />
        ) : (
          <TokenGeneratorField onGenerate={this.handleGenerateToken} />
        )}
      </TokenGeneratorContainer>
    );
  }
}

TokenGenerator.propTypes = {
  isMaba: PropTypes.bool,
  token: PropTypes.object,
  generateToken: PropTypes.func.isRequired,
  isTokenLoading: PropTypes.bool,
  isTokenLoaded: PropTypes.bool,
  autoGenerateToken: PropTypes.func
};

const mapStateToProps = state => ({
  token: getToken(state),
  isTokenLoading: isTokenLoading(state),
  isTokenLoaded: isTokenLoaded(state)
});

const mapDispatchToProps = dispatch => ({
  generateToken: limit => dispatch(generateToken(limit)),
  autoGenerateToken: () => dispatch(autoGenerateToken())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TokenGenerator);

export const GENERATE_TOKEN = "src/TokenGenerator/GENERATE_TOKEN";
export const GENERATE_TOKEN_SUCCESS =
  "src/TokenGenerator/GENERATE_TOKEN_SUCCESS";
export const GENERATE_TOKEN_FAILED = "src/TokenGenerator/GENERATE_TOKEN_FAILED";

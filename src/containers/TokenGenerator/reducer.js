/* eslint-disable no-case-declarations */
/* eslint-disable array-callback-return */
import { fromJS } from "immutable";
import {
  GENERATE_TOKEN,
  GENERATE_TOKEN_SUCCESS,
  GENERATE_TOKEN_FAILED
} from "./constants";

const initialState = fromJS({
  token: {},
  error: null,
  isLoaded: false,
  isLoading: false
});

function tokenGeneratorReducer(state = initialState, action) {
  switch (action.type) {
    case GENERATE_TOKEN:
      return state.set("isLoading", true);
    case GENERATE_TOKEN_SUCCESS:
      return state
        .set("token", action.payload)
        .set("isLoading", false)
        .set("isLoaded", true);
    case GENERATE_TOKEN_FAILED:
      return state
        .set("error", action.payload)
        .set("isLoading", false)
        .set("isLoaded", false);
    default:
      return state;
  }
}

export default tokenGeneratorReducer;

import { fromJS } from "immutable";
import {
  FETCH_USER_INTEREST,
  FETCH_USER_INTEREST_FAILED,
  FETCH_USER_INTEREST_SUCCESS,
  POST_USER_INTEREST,
  POST_USER_INTEREST_FAILED,
  POST_USER_INTEREST_SUCCESS,
  DELETE_USER_INTEREST,
  DELETE_USER_INTEREST_FAILED,
  DELETE_USER_INTEREST_SUCCESS,
  PATCH_USER_INTEREST,
  PATCH_USER_INTEREST_FAILED,
  PATCH_USER_INTEREST_SUCCESS
} from "./constants";

const initialState = fromJS({
  error: null,
  isTechLoading: false,
  isTechLoaded: false,
  interestList: [],
  postInterestResponse: "",
  deleteInterestResponse: "",
  patchInterestResponse: ""
});

export default function editProfileInterestsReducer(
  state = initialState,
  action
) {
  switch (action.type) {
    case FETCH_USER_INTEREST:
      return state.set("isLoading", true);
    case FETCH_USER_INTEREST_SUCCESS:
      return state
        .set("isLoading", false)
        .set("isLoaded", true)
        .set("interestList", action.payload);
    case FETCH_USER_INTEREST_FAILED:
      return state
        .set("isLoading", false)
        .set("isLoaded", false)
        .set("error", action.payload);
    case POST_USER_INTEREST:
      return state;
    case POST_USER_INTEREST_SUCCESS:
      return state.set("postInterestResponse", action.payload);
    case POST_USER_INTEREST_FAILED:
      return state;
    case DELETE_USER_INTEREST:
      return state.set("isLoading", true);
    case DELETE_USER_INTEREST_SUCCESS:
      return state
        .set("isLoading", false)
        .set("isLoaded", true)
        .set("deleteInterestResponse", action.payload);
    case DELETE_USER_INTEREST_FAILED:
      return state
        .set("isLoading", false)
        .set("isLoaded", false)
        .set("error", true);
    case PATCH_USER_INTEREST:
      return state.set("isLoading", true);
    case PATCH_USER_INTEREST_SUCCESS:
      return state
        .set("isLoading", false)
        .set("isLoaded", true)
        .set("patchInterestResponse", action.payload);
    case PATCH_USER_INTEREST_FAILED:
      return state
        .set("isLoading", false)
        .set("isLoaded", false)
        .set("error", true);
    default:
      return state;
  }
}

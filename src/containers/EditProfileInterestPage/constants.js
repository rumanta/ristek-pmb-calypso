export const FETCH_USER_INTEREST =
  "src/EditProfileInterestPage/FETCH_USER_INTEREST";

export const FETCH_USER_INTEREST_SUCCESS =
  "src/EditProfileInterestPage/FETCH_USER_INTEREST_SUCCESS";

export const FETCH_USER_INTEREST_FAILED =
  "src/EditProfileInterestPage/FETCH_USER_INTEREST_FAILED";

export const POST_USER_INTEREST =
  "src/EditProfileInterestPage/POST_USER_INTEREST";

export const POST_USER_INTEREST_SUCCESS =
  "src/EditProfileInterestPage/POST_USER_INTEREST_SUCCESS";

export const POST_USER_INTEREST_FAILED =
  "src/EditProfileInterestPage/POST_USER_INTEREST_FAILED";

export const DELETE_USER_INTEREST =
  "src/EditProfileInterestPage/DELETE_USER_INTEREST";

export const DELETE_USER_INTEREST_SUCCESS =
  "src/EditProfileInterestPage/DELETE_USER_INTEREST_SUCCESS";

export const DELETE_USER_INTEREST_FAILED =
  "src/EditProfileInterestPage/DELETE_USER_INTEREST_FAILED";

export const PATCH_USER_INTEREST =
  "src/EditProfileInterestPage/PATCH_USER_INTEREST";

export const PATCH_USER_INTEREST_SUCCESS =
  "src/EditProfileInterestPage/PATCH_USER_INTEREST_SUCCESS";

export const PATCH_USER_INTEREST_FAILED =
  "src/EditProfileInterestPage/PATCH_USER_INTEREST_FAILED";

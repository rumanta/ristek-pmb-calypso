import styled from "styled-components";

export const EditProfileInterestPageContainer = styled.div`
  .technology-card-wrap {
    .Card {
      z-index: 5;
    }
  }

  .non-tech-card-wrap {
    .Card {
      z-index: 4;
    }
  }
  .content {
    position: relative;
  }

  .search-container {
    background: ${props => props.theme.colors.fieldGray};
    margin-top: 0.5rem;
    width: 100%;
    border: 1px solid ${props => props.theme.colors.lightGray};
    border-radius: 5px;
    display: flex;
    flex-direction: row;
    align-items: center;

    img {
      padding: 0.5rem; // this must be the same with input padding
    }

    input {
      width: 100%;
      border: none;
      padding: 0.5rem;
    }

    input:focus {
      outline: none;
    }
  }

  .search-options {
    .search-result {
      background: ${props => props.theme.colors.fieldGray};
      position: absolute;
      margin: 0;
      padding: 0.5rem;
      left: 0;
      right: 0;
      border: 1px solid ${props => props.theme.colors.lightGray};
      border-radius: 5px;
      overflow-y: auto;
      max-height: 150px;
      cursor: pointer;
      z-index: 100;

      .search-result-item {
        padding: 0.5rem 0;
        background: ${props => props.theme.colors.fieldGray};
        width: 100%;
        list-style-type: none;
        border-bottom: 1px solid ${props => props.theme.colors.lightGray};
        z-index: 100;
      }

      .search-result-item:last-child {
        border-bottom: none;
        padding-top: 0.5rem;
      }

      .search-result-item:hover {
        background: ${props => props.theme.colors.gray};
      }
    }
  }

  .interest-list {
    margin: 0.5rem 0;

    .interest-list-item {
      padding: 0.5rem;
      display: flex;
      flex-direction: row;
      justify-content: space-between;
      align-items: center;

      .delete-icon-wrap {
        cursor: pointer;
        margin-right: 1rem;
      }

      .interest-name {
        width: 100%;
        font-size: 16px;
      }

      .interest-top-icon > img {
        cursor: pointer;
        height: 30px;
      }

      .interest-top-icon {
        position: relative;
        display: flex;
        align-items: center;
      }

      .interest-top-icon .tooltip-text {
        visibility: hidden;
        width: 120px;
        background-color: black;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;
        position: absolute;
        z-index: 1;
        bottom: 150%;
        left: 50%;
        font-size: 12px;
        margin-left: -60px;
      }

      .interest-top-icon .tooltip-text::after {
        content: "";
        position: absolute;
        top: 100%;
        left: 50%;
        margin-left: -5px;
        border-width: 5px;
        border-style: solid;
        border-color: black transparent transparent transparent;
      }

      .interest-top-icon:hover .tooltip-text {
        visibility: visible;
      }
    }
  }

  .like-img {
    width: 1.2rem;
  }

  .top-interest-icon {
    width: 1.2rem;
  }

  .interest-list-item:first-child .interest-top-icon > .top-interest-icon {
    cursor: default;
  }

  .interest-list-item:nth-child(1) {
    background-color: ${props => props.theme.colors.boneWhite};
    border-radius: 5px;
  }

  .empty-interest {
    padding: 0.5rem 0;
    font-size: 16px;
    color: ${props => props.theme.colors.lightGray};
  }

  .back-button-wrap {
    margin-top: 1rem;
    display: flex;
    justify-content: flex-end;
  }

  @media (max-width: 882px) {
    p {
      font-size: 13px;
    }
    input {
      font-size: 14px;
    }
  }

  @media (max-width: 460px) {
    .interest-name {
      p {
        font-size: 12px;
      }
    }
    .top-interest-icon {
      width: 1rem;
    }
    input {
      font-size: 12px;
    }
    .like-img {
      width: 1rem;
    }
  }
`;

/* eslint-disable array-callback-return */
/* eslint-disable consistent-return */
import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import Card from "components/Card";
import Section from "../../components/Section";
import { Buttons } from "../../components/Buttons";
import { push } from "connected-react-router";

import { EditProfileInterestPageContainer } from "./style";
import Spinner from "components/Loading";

import SearchIcon from "assets/icons/search.svg";
import rubbishBin from "assets/icons/rubbish-bin.svg";
import likeIcon from "assets/icons/like.svg";
import techTopIcon from "assets/icons/tech-top-icon.svg";
import nonTechIcon from "assets/icons/non-tech-icon.svg";
import entertainmentIcon from "assets/icons/entertainment-icon.svg";

import {
  getInterestList,
  isEditInterestLoading,
  isEditInterestLoaded,
  getPostInterestResponse,
  getDeleteInterestResponse,
  getPatchInterestResponse
} from "selectors/editInterests";
import { getUser } from "selectors/user";

import {
  fetchUserInterest,
  postUserInterest,
  deleteUserInterest,
  patchUserInterest
} from "./actions";
import swal from "sweetalert";

class EditProfileInterestPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      techSearchString: "",
      nonTechSearchString: "",
      entertainmentSearchString: "",
      isEditInterestLoading: this.props.isEditInterestLoading,
      isTechFocus: false,
      isNonTechFocus: false,
      isEntertainmentFocus: false,
      userInterestList: this.props.userInterestList,
      techInterestData: [
        "Game Development",
        "Competitive Programming",
        "Network Security and Operating System",
        "Data Science",
        "Business Intelligence",
        "Web Development",
        "Backend Development",
        "Frontend Development",
        "Mobile Development",
        "UI/UX",
        "Embedded System",
        "Artificial Intelligence",
        "Project Management",
        "Internet of Things"
      ],
      nonTechInterestData: [
        "Business IT",
        "Leadership",
        "Social Act",
        "Travelling",
        "Fashion",
        "Automotive",
        "Debate",
        "Cooking",
        "Gardening",
        "Puzzle",
        "History",
        "Astronomy",
        "Music",
        "Band",
        "Vocal",
        "Dance",
        "Theater",
        "Drawing",
        "Painting",
        "Crafting",
        "Graphic Design",
        "Literature",
        "Writing",
        "Photography",
        "Videography",
        "Football",
        "Basketball",
        "Volleyball",
        "Softball",
        "Cricket",
        "Floorball",
        "Golf",
        "Bowling",
        "Badminton",
        "Tennis",
        "Table Tennis",
        "Martial Art",
        "Pencak Silat",
        "Taekwondo",
        "Boxing",
        "Karate",
        "Javelin",
        "Archery",
        "Darts",
        "Chess",
        "Bridge",
        "Athletic",
        "Gym",
        "Swimming",
        "Running",
        "Cycling",
        "Equestrian",
        "Ice Skating",
        "Hiking",
        "Diving",
        "Surfing",
        "Jogging",
        "Yoga"
      ],
      entertainmentInterestData: [
        "Rock",
        "Jazz",
        "Classic",
        "Pop",
        "R&B",
        "Hip hop",
        "Electric",
        "Alternative",
        "K-Pop",
        "J-Pop",
        "Indie",
        "Netflix",
        "Anime",
        "Thriller",
        "Action",
        "Science Fiction",
        "Comedy",
        "Romance",
        "Superheroes",
        "Star Wars",
        "Lord of The Rings",
        "Game of Thrones",
        "Harry Potter",
        "DotA 2",
        "Pokemon",
        "Counter Strike",
        "League of Legends",
        "Osu",
        "GTA",
        "Minecraft",
        "Visual Novel",
        "FPS Game",
        "Rhytm Game",
        "PUBG",
        "Mobile Legend",
        "Clash Royale",
        "Clash of Clans",
        "Brawl Stars",
        "Marvel",
        "DC Comics",
        "Novel",
        "Comics"
      ],
      techSearchData: "",
      nonTechSearchData: "",
      entertainmentSearchData: ""
    };

    this.handleSearch = this.handleSearch.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.renderSearchList = this.renderSearchList.bind(this);
    this.handleMoveTop = this.handleMoveTop.bind(this);
  }

  componentDidMount() {
    const userId = this.props.user.user_id;

    this.props.fetchUserInterest(userId);
  }

  componentDidUpdate(prevProps) {
    if (prevProps !== this.props) {
      if (prevProps.userInterestList !== this.props.userInterestList) {
        this.setState({ userInterestList: this.props.userInterestList });
      }
      if (
        prevProps.isEditInterestLoading !== this.props.isEditInterestLoading
      ) {
        this.setState({
          isEditInterestLoading: this.props.isEditInterestLoading
        });
      }
      if (prevProps.postInterestResponse !== this.props.postInterestResponse) {
        this.props.fetchUserInterest(this.props.user.user_id);
      }
      if (
        prevProps.isEditInterestLoading !== this.props.isEditInterestLoading
      ) {
        this.setState({
          isEditInterestLoading: this.props.isEditInterestLoading
        });
      }
      if (
        prevProps.deleteInterestResponse !== this.props.deleteInterestResponse
      ) {
        this.props.fetchUserInterest(this.props.user.user_id);
      }
      if (
        prevProps.patchInterestResponse !== this.props.patchInterestResponse
      ) {
        this.props.fetchUserInterest(this.props.user.user_id);
      }
    }
  }

  handleSearch(event) {
    const whichCategory = event.currentTarget.id;
    const searchedString = event.target.value;
    let filteredData = "";

    switch (whichCategory.toLowerCase()) {
      case "technology":
        filteredData = this.filterArrayData(searchedString, "technology");

        this.renderSearchList(filteredData, "Technology");
        this.setState({ techSearchString: searchedString });

        break;
      case "non-tech":
        filteredData = this.filterArrayData(searchedString, "non-tech");

        this.renderSearchList(filteredData, "Non-Tech");
        this.setState({ nonTechSearchString: searchedString });

        break;
      case "entertainment":
        filteredData = this.filterArrayData(searchedString, "entertainment");

        this.renderSearchList(filteredData, "Entertainment");
        this.setState({ entertainmentSearchString: searchedString });

        break;
      default:
        console.log("search switch case err", whichCategory);
    }
  }

  filterArrayData(searchedString, whichCategory) {
    let filteredData = "";
    let techInterestData = "";
    let nonTechInterestData = "";
    let entertainmentInterestData = "";

    switch (whichCategory.toLowerCase()) {
      case "technology":
        techInterestData = this.state.techInterestData;
        filteredData = techInterestData.filter(data => {
          if (data.toLowerCase().match(new RegExp(searchedString, "gi"))) {
            return data;
          }
        });

        // add custom filtered data
        if (searchedString !== "") {
          filteredData.push(searchedString);
        }

        return filteredData;
      case "non-tech":
        nonTechInterestData = this.state.nonTechInterestData;
        filteredData = nonTechInterestData.filter(data => {
          if (data.toLowerCase().match(new RegExp(searchedString, "gi"))) {
            return data;
          }
        });

        // add custom filtered data
        if (searchedString !== "") {
          filteredData.push(searchedString);
        }

        return filteredData;
      case "entertainment":
        entertainmentInterestData = this.state.entertainmentInterestData;
        filteredData = entertainmentInterestData.filter(data => {
          if (data.toLowerCase().match(new RegExp(searchedString, "gi"))) {
            return data;
          }
        });

        // add custom filtered data
        if (searchedString !== "") {
          filteredData.push(searchedString);
        }

        return filteredData;
      default:
        console.log("error filter array data", whichCategory);

        break;
    }

    return null;
  }

  handleClick(event) {
    let userInterestList = this.state.userInterestList;

    let techInterestCount = 0;
    let nonTechInterestCount = 0;
    let entertainmentInterestCount = 0;

    const selectedInterest = event.currentTarget.id;
    const selectedCategory = event.currentTarget.className.split(" ")[1];

    userInterestList.map(interest => {
      const whichCategory = interest.interest_category;
      if (
        interest.interest_name.toLowerCase() === selectedInterest.toLowerCase()
      ) {
        userInterestList = null;

        return userInterestList;
      }

      switch (whichCategory) {
        case "Technology":
          return (techInterestCount += 1);

        case "Non-Tech":
          return (nonTechInterestCount += 1);

        case "Entertainment":
          return (entertainmentInterestCount += 1);

        default:
          console.log("error count interest", whichCategory);
      }
    });

    switch (selectedCategory) {
      case "Technology":
        if (techInterestCount >= 5) {
          return swal(
            "Error",
            "Cannot add more than 5 interests per category",
            "error"
          );
        }
        break;
      case "Non-Tech":
        if (nonTechInterestCount >= 5) {
          return swal(
            "Error",
            "Cannot add more than 5 interests per category",
            "error"
          );
        }
        break;
      case "Entertainment":
        if (entertainmentInterestCount >= 5) {
          return swal(
            "Error",
            "Cannot add more than 5 interests per category",
            "error"
          );
        }
        break;
      default:
        console.log("error count interest", selectedCategory);
    }

    if (userInterestList === null) {
      swal("You already have the same interest", "", "error");

      return null;
    }

    const payload = {
      interest_name: this.titleCase(selectedInterest),
      interest_category: selectedCategory
    };

    this.props.postUserInterest(payload);

    return null;
  }

  handleFocus(event) {
    // do something
    const whichInterest = event.currentTarget.id;
    let searchedString = "";
    let filteredData = [];
    switch (whichInterest.toLowerCase()) {
      case "technology":
        searchedString = this.state.techSearchString;
        filteredData = this.filterArrayData(searchedString, "technology");
        this.renderSearchList(filteredData, "Technology");

        this.setState({
          isTechFocus: true
        });
        break;
      case "non-tech":
        searchedString = this.state.nonTechSearchString;
        filteredData = this.filterArrayData(searchedString, "non-tech");
        this.renderSearchList(filteredData, "Non-Tech");

        this.setState({
          isNonTechFocus: true
        });
        break;
      case "entertainment":
        searchedString = this.state.entertainmentSearchString;
        filteredData = this.filterArrayData(searchedString, "entertainment");
        this.renderSearchList(filteredData, "Entertainment");

        this.setState({
          isEntertainmentFocus: true
        });
        break;
      default:
        console.log("current target error->", whichInterest);
    }
  }

  handleBlur() {
    // timeout so onClick registered first
    if (this.state.isTechFocus) {
      setTimeout(() => {
        this.setState({
          isTechFocus: false
        });
      }, 150);
    } else if (this.state.isNonTechFocus) {
      setTimeout(() => {
        this.setState({
          isNonTechFocus: false
        });
      }, 150);
    } else if (this.state.isEntertainmentFocus) {
      setTimeout(() => {
        this.setState({
          isEntertainmentFocus: false
        });
      }, 150);
    }
  }

  handleDelete(event) {
    const interestId = event.currentTarget.id;
    this.props.deleteUserInterest(interestId);
  }

  renderSearchList(searchArr, whichCategory) {
    const techSearchList = [];
    const nonTechSearchList = [];
    const entertainmentSearchList = [];
    const { userInterestList } = this.state;

    searchArr.map(data => {
      let isValid = true;

      // filter uwhichSectionser chosen interest from interest list
      userInterestList.map(interest => {
        if (interest.interest_name === data) {
          // redundant
          isValid = false;
        }
      });
      if (isValid) {
        switch (whichCategory) {
          case "Technology":
            techSearchList.push(
              <li
                key={`${whichCategory};${data}`}
                className={`search-result-item ${whichCategory}`}
                id={data}
                onClick={this.handleClick}
              >
                {data}
              </li>
            );
            break;
          case "Non-Tech":
            nonTechSearchList.push(
              <li
                key={`${whichCategory};${data}`}
                className={`search-result-item ${whichCategory}`}
                id={data}
                onClick={this.handleClick}
              >
                {data}
              </li>
            );
            break;
          case "Entertainment":
            entertainmentSearchList.push(
              <li
                key={`${whichCategory};${data}`}
                className={`search-result-item ${whichCategory}`}
                id={data}
                onClick={this.handleClick}
              >
                {data}
              </li>
            );
            break;
          default:
            console.log("switch case error", whichCategory);
        }
      }
    });

    this.setState({
      techSearchData: techSearchList,
      nonTechSearchData: nonTechSearchList,
      entertainmentSearchData: entertainmentSearchList
    });
  }

  handleMoveTop(event) {
    const selectedId = event.currentTarget.id;
    this.props.patchUserInterest(selectedId);
  }

  titleCase(str) {
    return str
      .toLowerCase()
      .split(" ")
      .map(word => word.replace(word[0], word[0].toUpperCase()))
      .join(" ");
  }

  render() {
    const {
      userInterestList,
      isTechFocus,
      techSearchData,
      // eslint-disable-next-line no-shadow
      isEditInterestLoading,
      isNonTechFocus,
      nonTechSearchData,
      isEntertainmentFocus,
      entertainmentSearchData
    } = this.state;

    const renderEmptyList = (
      <div className="empty-interest">
        <p>You Have No Interest in This Category</p>
      </div>
    );

    const renderUserTechInterestList = [];
    const renderUserNonTechInterestList = [];
    const renderUserEntertainmentInterestList = [];

    if (userInterestList) {
      userInterestList.map(data => {
        const isFirst = data.is_top_interest;
        if (data.interest_category === "Technology") {
          if (isFirst) {
            renderUserTechInterestList.unshift(
              <div
                className="interest-list-item"
                key={`${data.id};${data.interest_name}`}
                id={data.id}
              >
                <div
                  className="delete-icon-wrap"
                  onClick={this.handleDelete}
                  id={data.id}
                >
                  <img src={rubbishBin} alt="delete-icon" />
                </div>
                <div className="interest-name">
                  <p>{data.interest_name}</p>
                </div>
                <div className="interest-top-icon" id={data.id}>
                  <img
                    className="top-interest-icon"
                    src={techTopIcon}
                    alt="top tech icon"
                  />
                </div>
              </div>
            );
          } else {
            renderUserTechInterestList.push(
              <div
                className="interest-list-item"
                key={`${data.id};${data.interest_name}`}
                id={data.id}
              >
                <div
                  className="delete-icon-wrap"
                  onClick={this.handleDelete}
                  id={data.id}
                >
                  <img src={rubbishBin} alt="delete-icon" />
                </div>
                <div className="interest-name">
                  <p>{data.interest_name}</p>
                </div>
                <div
                  className="interest-top-icon"
                  onClick={this.handleMoveTop}
                  id={data.id}
                >
                  <img
                    className="like-img"
                    src={likeIcon}
                    alt="move item icon"
                  />
                  <span className="tooltip-text">
                    Click to set as your top interest!
                  </span>
                </div>
              </div>
            );
          }
        } else if (data.interest_category === "Non-Tech") {
          if (isFirst) {
            renderUserNonTechInterestList.unshift(
              <div
                className="interest-list-item"
                key={`${data.id};${data.interest_name}`}
                id={data.id}
              >
                <div
                  className="delete-icon-wrap"
                  onClick={this.handleDelete}
                  id={data.id}
                >
                  <img src={rubbishBin} alt="delete-icon" />
                </div>
                <div className="interest-name">
                  <p>{data.interest_name}</p>
                </div>
                <div className="interest-top-icon" id={data.id}>
                  <img
                    className="top-interest-icon"
                    src={nonTechIcon}
                    alt="top non tech icon"
                  />
                </div>
              </div>
            );
          } else {
            renderUserNonTechInterestList.push(
              <div
                className="interest-list-item"
                key={`${data.id};${data.interest_name}`}
                id={data.id}
              >
                <div
                  className="delete-icon-wrap"
                  onClick={this.handleDelete}
                  id={data.id}
                >
                  <img src={rubbishBin} alt="delete-icon" />
                </div>
                <div className="interest-name">
                  <p>{data.interest_name}</p>
                </div>
                <div
                  className="interest-top-icon"
                  onClick={this.handleMoveTop}
                  id={data.id}
                >
                  <img
                    className="like-img"
                    src={likeIcon}
                    alt="move item icon"
                  />
                  <span className="tooltip-text">
                    Click to set as your top interest!
                  </span>
                </div>
              </div>
            );
          }
        } else if (data.interest_category === "Entertainment") {
          if (isFirst) {
            renderUserEntertainmentInterestList.unshift(
              <div
                className="interest-list-item"
                key={`${data.id};${data.interest_name}`}
                id={data.id}
              >
                <div
                  className="delete-icon-wrap"
                  onClick={this.handleDelete}
                  id={data.id}
                >
                  <img src={rubbishBin} alt="delete-icon" />
                </div>
                <div className="interest-name">
                  <p>{data.interest_name}</p>
                </div>
                <div className="interest-top-icon" id={data.id}>
                  <img
                    className="top-interest-icon"
                    src={entertainmentIcon}
                    alt="top entertainment icon"
                  />
                </div>
              </div>
            );
          } else {
            renderUserEntertainmentInterestList.push(
              <div
                className="interest-list-item"
                key={`${data.id};${data.interest_name}`}
                id={data.id}
              >
                <div
                  className="delete-icon-wrap"
                  onClick={this.handleDelete}
                  id={data.id}
                >
                  <img src={rubbishBin} alt="delete-icon" />
                </div>
                <div className="interest-name">
                  <p>{data.interest_name}</p>
                </div>
                <div
                  className="interest-top-icon"
                  onClick={this.handleMoveTop}
                  id={data.id}
                >
                  <img
                    className="like-img"
                    src={likeIcon}
                    alt="move item icon"
                  />
                  <span className="tooltip-text">
                    Click to set as your top interest!
                  </span>
                </div>
              </div>
            );
          }
        }

        return null;
      });
    }

    return (
      <EditProfileInterestPageContainer>
        <Section title="Edit Interest">
          <div className="technology-card-wrap">
            <Card>
              <h2>Technology</h2>
              <div className="content">
                <div className="search-container">
                  <img src={SearchIcon} alt="search-icon.svg" />
                  <input
                    type="text"
                    id="technology"
                    className="searchbar"
                    onChange={this.handleSearch}
                    value={this.state.techSearchString}
                    placeholder="Input your interest (ex: Web Development)"
                    onFocus={this.handleFocus}
                    onBlur={this.handleBlur}
                  />
                </div>
                <div className="tech-interest-data">
                  <div className="search-options">
                    {isTechFocus && (
                      <ul className="search-result">{techSearchData}</ul>
                    )}
                  </div>
                  <div className="interest-list">
                    {isEditInterestLoading ? (
                      <Spinner />
                    ) : renderUserTechInterestList.length === 0 ? (
                      renderEmptyList
                    ) : (
                      renderUserTechInterestList
                    )}
                  </div>
                </div>
              </div>
            </Card>
          </div>

          <div className="non-tech-card-wrap">
            <Card>
              <h2>Non-Technology</h2>
              <div className="content">
                <div className="search-container">
                  <img src={SearchIcon} alt="search-icon.svg" />
                  <input
                    type="text"
                    id="non-tech"
                    className="searchbar"
                    onChange={this.handleSearch}
                    value={this.state.nonTechSearchString}
                    placeholder="Input your interest (ex: Photography)"
                    onFocus={this.handleFocus}
                    onBlur={this.handleBlur}
                  />
                </div>
                <div className="non-tech-interest-data">
                  <div className="search-options">
                    {isNonTechFocus && (
                      <ul className="search-result">{nonTechSearchData}</ul>
                    )}
                  </div>
                  <div className="interest-list">
                    {isEditInterestLoading ? (
                      <Spinner />
                    ) : renderUserNonTechInterestList.length === 0 ? (
                      renderEmptyList
                    ) : (
                      renderUserNonTechInterestList
                    )}
                  </div>
                </div>
              </div>
            </Card>
          </div>

          <div className="entertainment-card-wrap">
            <Card>
              <h2>Entertainment</h2>
              <div className="content">
                <div className="search-container">
                  <img src={SearchIcon} alt="search-icon.svg" />
                  <input
                    type="text"
                    id="entertainment"
                    className="searchbar"
                    onChange={this.handleSearch}
                    value={this.state.entertainmentSearchString}
                    placeholder="Input your interest (ex: Pokemon)"
                    onFocus={this.handleFocus}
                    onBlur={this.handleBlur}
                  />
                </div>
                <div className="entertainment-interest-data">
                  <div className="search-options">
                    {isEntertainmentFocus && (
                      <ul className="search-result">
                        {entertainmentSearchData}
                      </ul>
                    )}
                  </div>
                  <div className="interest-list">
                    {isEditInterestLoading ? (
                      <Spinner />
                    ) : renderUserEntertainmentInterestList.length === 0 ? (
                      renderEmptyList
                    ) : (
                      renderUserEntertainmentInterestList
                    )}
                  </div>
                </div>
              </div>
            </Card>
          </div>

          <div
            className="back-button-wrap"
            onClick={() => this.props.push("/profile")}
          >
            <Buttons name="OK" wide="6rem" />
          </div>
        </Section>
      </EditProfileInterestPageContainer>
    );
  }
}

EditProfileInterestPage.propTypes = {
  push: PropTypes.func.isRequired,
  isEditInterestLoading: PropTypes.bool.isRequired,
  isEditInterestLoaded: PropTypes.bool.isRequired,
  userInterestList: PropTypes.shape().isRequired,
  fetchUserInterest: PropTypes.func.isRequired,
  user: PropTypes.func.isRequired,
  postUserInterest: PropTypes.func.isRequired,
  deleteUserInterest: PropTypes.func.isRequired,
  patchUserInterest: PropTypes.func.isRequired,
  postInterestResponse: PropTypes.object.isRequired,
  deleteInterestResponse: PropTypes.object.isRequired,
  patchInterestResponse: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    isEditInterestLoading: isEditInterestLoading(state),
    isEditInterestLoaded: isEditInterestLoaded(state),
    userInterestList: getInterestList(state),
    user: getUser(state),
    postInterestResponse: getPostInterestResponse(state),
    deleteInterestResponse: getDeleteInterestResponse(state),
    patchInterestResponse: getPatchInterestResponse(state)
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchUserInterest: id => dispatch(fetchUserInterest(id)),
    postUserInterest: payload => dispatch(postUserInterest(payload)),
    deleteUserInterest: id => dispatch(deleteUserInterest(id)),
    push: url => dispatch(push(url)),
    patchUserInterest: id => dispatch(patchUserInterest(id))
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditProfileInterestPage);

import React, { Component } from "react";
import { TokenInputContainer } from "./style";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import Button from "components/Button";
import Card from "components/Card";
import LoadingFullscreen from "components/LoadingFullscreen";
import {
  getKenalan,
  isKenalanLoaded,
  isKenalanLoading
} from "selectors/tokens";

import { postKenalan } from "./actions";

class TokenInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: ""
    };
  }

  handleChangeInput = evt => {
    this.setState({
      token: evt.target.value
    });
  };

  clearInput = () => {
    this.setState({
      token: ""
    });
  };

  submitToken = () => {
    const tokenBody = {
      token: this.state.token
    };

    this.props.postKenalan(tokenBody, this.clearInput);
  };

  render() {
    return (
      <TokenInputContainer>
        {this.props.isKenalanLoading && <LoadingFullscreen />}
        <Card>
          <p className="desc-text">Token:</p>
          <input
            className="input-token-field"
            value={this.state.token}
            onChange={evt => this.handleChangeInput(evt)}
          />
          <div className="btn-cont">
            <Button text="Submit" onClick={this.submitToken} />
          </div>
        </Card>
      </TokenInputContainer>
    );
  }
}

TokenInput.propTypes = {
  kenalan: PropTypes.object,
  isKenalanLoaded: PropTypes.bool,
  isKenalanLoading: PropTypes.bool,
  postKenalan: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  kenalan: getKenalan(state),
  isKenalanLoading: isKenalanLoading(state),
  isKenalanLoaded: isKenalanLoaded(state)
});

const mapDispatchToProps = dispatch => ({
  postKenalan: (token, clearInput) => dispatch(postKenalan(token, clearInput))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TokenInput);

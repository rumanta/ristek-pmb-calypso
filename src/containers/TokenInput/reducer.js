/* eslint-disable no-case-declarations */
/* eslint-disable array-callback-return */
import { fromJS } from "immutable";
import {
  POST_KENALAN,
  POST_KENALAN_SUCCESS,
  POST_KENALAN_FAILED
} from "./constants";

const initialState = fromJS({
  kenalan: {},
  error: null,
  isLoaded: false,
  isLoading: false
});

function tokenInputReducer(state = initialState, action) {
  switch (action.type) {
    case POST_KENALAN:
      return state.set("isLoading", true);
    case POST_KENALAN_SUCCESS:
      return state
        .set("kenalan", action.payload)
        .set("isLoading", false)
        .set("isLoaded", true);
    case POST_KENALAN_FAILED:
      return state
        .set("error", action.payload)
        .set("isLoading", false)
        .set("isLoaded", false);
    default:
      return state;
  }
}

export default tokenInputReducer;

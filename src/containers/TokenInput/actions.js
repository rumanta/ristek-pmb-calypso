import axios from "axios";
import swal from "sweetalert";
import { push } from "connected-react-router";
import {
  POST_KENALAN,
  POST_KENALAN_SUCCESS,
  POST_KENALAN_FAILED
} from "./constants";
import { postKenalanApi } from "api";

export function postKenalan(reqBody, clearInput) {
  return dispatch => {
    dispatch({ type: POST_KENALAN });
    axios
      .post(postKenalanApi, reqBody)
      .then(response => {
        dispatch({
          payload: response.data,
          type: POST_KENALAN_SUCCESS
        });
        clearInput();
        swal("Success", `Token is successfully input`, "success");
        dispatch(push(`/formfriend/${response.data.id}`));
      })
      .catch(error => {
        dispatch({
          payload: error,
          type: POST_KENALAN_FAILED
        });
        swal("Error", "Oops! The token is either invalid or expired", "error");
      });
  };
}

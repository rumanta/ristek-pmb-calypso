import styled from "styled-components";

export const FeedbackPageStyle = styled.div`
  /* media for Iphone 5  or smaller phones*/
  .googleFormContainer {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }

  .googleForm {
    border: none;
    padding: 1rem;
    box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.5);
    border-radius: 10px;
    z-index: 1;
    position: relative;
    background: white;
  }

  .googleFormContainer p {
    text-align: left;
    font-size: 24px;
    padding: 0 1em 0 1em;
    margin-bottom: 1.5rem;
  }

  /* Media for average smartphones*/
  @media (min-height: 600px) {
  .googleFormContainer {
    padding: 2rem;
  }
    .googleFormContainer p {
      font-size: 30px;
    }
  }

  /* Media for smaller screen laptops (touchscreen etc) */
  @media (min-width: 64em) {
    .googleFormContainer p {
      font-size: 36px;
    }

  /* Media for above 1600 in width */
  @media (min-width: 1601px) and (min-height: 700px) {
  }
`;

/* eslint-disable array-callback-return */
/* eslint-disable no-shadow */
import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { push } from "connected-react-router";

import TaskListCard from "../../components/TaskListCard";
import { TaskPageContainer } from "./style";
import PaginationContainer from "components/PaginationContainer";

import { fetchTasksList } from "./actions";
import { isLoading, isLoaded, getTasks } from "selectors/taskPage";

class TaskPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      taskList: this.props.tasks,
      isLoading: this.props.isLoading
    };
  }

  componentDidMount() {
    if (!this.props.isLoaded) {
      this.props.fetchTasksList();
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps !== this.props) {
      if (prevProps.tasks !== this.props.tasks) {
        this.setState({ taskList: this.props.tasks });
      }
      if (prevProps.isLoading !== this.props.isLoading) {
        this.setState({ isLoading: this.props.isLoading });
      }
    }
  }

  renderOngoingTasks = () => {
    const now = new Date();
    const ongoingTasks = [];
    let finishedTasks = [];

    if (this.props.tasks) {
      this.props.tasks.map((task, index) => {
        const taskDate = new Date(task.end_date);
        const taskCard = (
          <TaskListCard
            key={`${task.title};${index};${task.id}`}
            title={task.title}
            date={task.end_date}
            isSubmissionRequired={task.can_have_submission}
            isSubmitted={task.is_submitted}
            onClick={() => this.props.push(`/tasks/${task.id}`)}
            isMaba={this.props.isMaba}
          />
        );
        if (taskDate < now) {
          finishedTasks.push(taskCard);
        } else {
          ongoingTasks.push(taskCard);
        }
      });
    }

    finishedTasks =
      finishedTasks &&
      finishedTasks.sort(
        (taskA, taskB) =>
          new Date(taskB.props.date) - new Date(taskA.props.date)
      );

    return {
      ongoingTasks,
      finishedTasks
    };
  };

  render() {
    const { ongoingTasks, finishedTasks } = this.renderOngoingTasks();

    return (
      <TaskPageContainer>
        <PaginationContainer
          header={<a className="title">Ongoing Tasks</a>}
          isLoading={this.props.isLoading}
          emptyText={"There isn't any Ongoing Task"}
        >
          {ongoingTasks}
        </PaginationContainer>
        <PaginationContainer
          header={<a className="title">Finished Tasks</a>}
          isLoading={this.props.isLoading}
          emptyText={"There isn't any Finished Task"}
        >
          {finishedTasks}
        </PaginationContainer>
      </TaskPageContainer>
    );
  }
}

TaskPage.propTypes = {
  push: PropTypes.func.isRequired,
  tasks: PropTypes.array.isRequired,
  isLoading: PropTypes.bool.isRequired,
  isLoaded: PropTypes.bool.isRequired,
  fetchTasksList: PropTypes.func.isRequired,
  isMaba: PropTypes.bool.isRequired
};

function mapStateToProps(state) {
  return {
    isLoaded: isLoaded(state),
    isLoading: isLoading(state),
    tasks: getTasks(state),
    isMaba: state.profileReducer.get("isUserMaba")
  };
}

function mapDispatchToProps(dispatch) {
  return {
    push: url => dispatch(push(url)),
    fetchTasksList: () => dispatch(fetchTasksList())
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TaskPage);

import axios from "axios";

import {
  FETCH_FEEDBACK,
  FETCH_FEEDBACK_SUCCESS,
  FETCH_FEEDBACK_FAILED
} from "./constants";

import { getFeedbacks } from "api";

export function fetchFeedbacks() {
  return dispatch => {
    dispatch({
      type: FETCH_FEEDBACK
    });
    axios
      .get(getFeedbacks)
      .then(response => {
        dispatch({
          payload: response.data,
          type: FETCH_FEEDBACK_SUCCESS
        });
      })
      .catch(error => {
        dispatch({
          payload: error,
          type: FETCH_FEEDBACK_FAILED
        });
      });
  };
}

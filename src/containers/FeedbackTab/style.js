import styled from "styled-components";

export const FeedbackTabContainer = styled.div`
  display: flex;
  flex-direction: column;

  .loadMoreSection {
    margin: 1rem 0;
    z-index: 1;
  }

  .loadMoreSection p {
    color: ${props => props.theme.colors.darkBlue};
    cursor: pointer;
    margin-bottom: 3rem;
  }

  @media (max-width: 760px) {
    .loadMoreSection p {
      margin-bottom: 5rem;
    }
  }
`;

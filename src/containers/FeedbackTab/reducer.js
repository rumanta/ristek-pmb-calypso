import { fromJS } from "immutable";
import {
  FETCH_FEEDBACK,
  FETCH_FEEDBACK_SUCCESS,
  FETCH_FEEDBACK_FAILED
} from "./constants";

const initialState = fromJS({
  error: null,
  isLoading: false,
  feedbacks: []
});

function feedbackTabReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_FEEDBACK:
      return state.set("isLoading", true);
    case FETCH_FEEDBACK_SUCCESS:
      return state.set("feedbacks", action.payload).set("isLoading", false);
    case FETCH_FEEDBACK_FAILED:
      return state.set("error", action.payload).set("isLoading", false);
    default:
      return state;
  }
}

export default feedbackTabReducer;

export const FETCH_FEEDBACK = "src/FeedbackTab/FETCH_FEEDBACKS";

export const FETCH_FEEDBACK_SUCCESS = "src/FeedbackTab/FETCH_FEEDBACKS_SUCCESS";

export const FETCH_FEEDBACK_FAILED = "src/FeedbackTab/FETCH_FEEDBACKS_FAILED";

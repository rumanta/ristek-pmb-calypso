import axios from "axios";
import swal from "sweetalert";
import {
  FETCH_EVENT_DETAIL,
  FETCH_EVENT_DETAIL_SUCCESS,
  FETCH_EVENT_DETAIL_FAILED,
  POST_QNA_COMMENT,
  POST_QNA_COMMENT_SUCCESS,
  POST_QNA_COMMENT_FAILED,
  CLEAR_EVENT_DETAIL
} from "./constants";

import { fetchEventDetailApi, postCommentApi } from "api";

export function fetchEventDetail(id) {
  return dispatch => {
    dispatch({ type: FETCH_EVENT_DETAIL });
    axios
      .get(fetchEventDetailApi(id))
      .then(response => {
        dispatch({
          payload: response.data,
          type: FETCH_EVENT_DETAIL_SUCCESS
        });
      })
      .catch(error => {
        dispatch({
          payload: error,
          type: FETCH_EVENT_DETAIL_FAILED
        });
      });
  };
}

export function postEventComment(comment) {
  return dispatch => {
    dispatch({ type: POST_QNA_COMMENT });
    axios
      .post(postCommentApi, comment)
      .then(() => {
        dispatch({ type: POST_QNA_COMMENT_SUCCESS });
        dispatch(fetchEventDetail(comment.post_id));
        swal("Comment has been added", "", "success");
      })
      .catch(error => {
        dispatch({
          payload: error,
          type: POST_QNA_COMMENT_FAILED
        });
        swal(
          "Error",
          "Failed to add a comment. Please try again later",
          "error"
        );
      });
  };
}

export function clearEvent() {
  return dispatch => {
    dispatch({ type: CLEAR_EVENT_DETAIL });
  };
}

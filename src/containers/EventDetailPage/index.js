import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { push } from "connected-react-router";

import DetailCard from "components/DetailCard";
import CommentSection from "components/CommentSection";
import Spinner from "components/Loading";
import NotFoundPage from "components/NotFoundPage";

import { fetchEventDetail, postEventComment, clearEvent } from "./actions";
import {
  getEvent,
  isEventLoaded,
  isEventLoading,
  getEventError
} from "selectors/events";

class EventDetailPage extends Component {
  componentDidMount() {
    const param = this.getParam();
    this.props.fetchEventDetail(param);
  }

  componentWillUnmount() {
    this.props.clearEvent();
  }

  getParam = () => {
    const params = this.props.location.pathname.split("/")[2];

    return params;
  };

  renderQnA = () => {
    const { event } = this.props;
    if (event) {
      return (
        <CommentSection
          comments={event.qnas}
          onSubmitComment={this.addQnAComment}
        />
      );
    }

    return null;
  };

  addQnAComment = comment => {
    const requestBodyComment = {
      post_type: "event",
      post_id: this.getParam(),
      comment
    };

    this.props.postEventComment(requestBodyComment);
  };

  render() {
    const { event, eventError, isLoadedEvent } = this.props;

    return (
      <>
        {isLoadedEvent ? (
          [
            <DetailCard
              key={event.title}
              type="event"
              title={event.title}
              date={event.date}
              coverImage={event.cover_image_link}
              location={event.place}
              organizer={event.organizer}
              organizerImage={event.organizer_image_link}
              description={event.description}
              attachmentLink={event.attachment_link}
              onClick={() => this.props.push("/events")}
              isLoading={this.props.isLoadingEvent}
            />,
            this.renderQnA()
          ]
        ) : eventError &&
          eventError.response &&
          eventError.response.data &&
          eventError.response.data.detail === "Not found." ? (
          <NotFoundPage />
        ) : (
          <Spinner />
        )}
      </>
    );
  }
}

EventDetailPage.propTypes = {
  event: PropTypes.shape().isRequired,
  isLoadedEvent: PropTypes.bool.isRequired,
  isLoadingEvent: PropTypes.bool.isRequired,
  fetchEventDetail: PropTypes.func.isRequired,
  location: PropTypes.shape().isRequired,
  push: PropTypes.func.isRequired,
  postEventComment: PropTypes.func.isRequired,
  eventError: PropTypes.object,
  clearEvent: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  event: getEvent(state),
  isLoadedEvent: isEventLoaded(state),
  isLoadingEvent: isEventLoading(state),
  eventError: getEventError(state)
});

const mapDispatchToProps = dispatch => ({
  fetchEventDetail: id => dispatch(fetchEventDetail(id)),
  push: url => dispatch(push(url)),
  postEventComment: comment => dispatch(postEventComment(comment)),
  clearEvent: () => dispatch(clearEvent())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EventDetailPage);

import React from "react";
import PropTypes from "prop-types";
import { ProfileTabContainer } from "./style";
import ProfileCard from "../../components/ProfileCard";
import InterestCard from "../../components/InterestCard";

export default class ProfileTab extends React.Component {
  render() {
    const { interests, profile, push } = this.props;

    return (
      <ProfileTabContainer>
        <ProfileCard profile={profile} push={push} />
        <InterestCard interests={interests} push={push} />
      </ProfileTabContainer>
    );
  }
}

ProfileTab.propTypes = {
  profile: PropTypes.node,
  push: PropTypes.func,
  interests: PropTypes.node
};

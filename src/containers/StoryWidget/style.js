import styled from "styled-components";

export const StoryWidgetContainer = styled.div`
  .loadMoreSection {
    display: flex;
    justify-content: flex-end;
    margin: 1rem 0;
  }

  .loadMoreSection p {
    cursor: pointer;
  }

  .see-more {
    font-size: 12px;
    text-decoration: none;
    color: ${props => props.theme.colors.blue};
  }

  .see-more:hover {
    text-decoration: underline;
  }
`;

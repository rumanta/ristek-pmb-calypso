export const FETCH_STORIES = "src/StoryWidget/FETCH_STORIES";

export const FETCH_STORIES_SUCCESS = "src/StoryWidget/FETCH_STORIES_SUCCESS";

export const FETCH_STORIES_FAILED = "src/StoryWidget/FETCH_RECENT_FAILED";

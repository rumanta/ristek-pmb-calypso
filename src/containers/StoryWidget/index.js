import React from "react";
import { StoryWidgetContainer } from "./style";
import { connect } from "react-redux";
import StoryWidgetCard from "components/StoryWidgetCard";
import EmptyCard from "components/EmptyCard";
import PropTypes from "prop-types";
import { fetchRecentStories } from "./actions";
import Loading from "components/Loading";
import { push } from "connected-react-router";

class StoryWidget extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      cardsToShow: 2,
      expanded: false,
      stories: []
    };
    this.loadMore = this.loadMore.bind(this);
  }

  loadMore(itemLength) {
    if (this.state.cardsToShow === 2) {
      this.setState({
        cardsToShow: itemLength,
        expanded: true
      });
    } else {
      this.setState({
        cardsToShow: 2,
        expanded: false
      });
    }
  }

  componentDidMount() {
    this.props.fetchRecentStories();
  }

  componentDidUpdate() {
    if (this.props.isDeleteSuccess) {
      this.props.fetchRecentStories();
    }
  }

  splitObjects(object) {
    return [
      {
        id: object.id,
        title: object.medium.title,
        subtitle: object.medium.subtitle,
        thumbnail: object.medium.thumbnail
          ? `https://cdn-images-1.medium.com/max/1024/${
              object.medium.thumbnail.metadata.id
            }`
          : null,
        link: object.link,
        date: object.created_at
      },
      {
        profileImage: object.user.user_detail.profil.foto,
        name: object.user.user_detail.profil.nama_lengkap
      }
    ];
  }

  produceWidgetCards = objects =>
    objects.map((object, key) => {
      const splitObject = this.splitObjects(object);

      return (
        <StoryWidgetCard
          key={key}
          entry={splitObject[0]}
          profile={splitObject[1]}
        />
      );
    });

  render() {
    const stories = this.props.stories.filter(
      object => object.medium.title || object.medium.subtitle
    );
    const { isLoading } = this.props;

    return isLoading ? (
      <Loading />
    ) : (
      <StoryWidgetContainer>
        {stories.length !== 0 ? (
          this.produceWidgetCards(stories.slice(0, this.state.cardsToShow))
        ) : (
          <EmptyCard text="There are no stories yet! Come back later :D" />
        )}
        {stories.length > 2 && !this.state.expanded && (
          <div className="loadMoreSection">
            <a className="see-more" href="/stories">
              more stories...
            </a>
          </div>
        )}
      </StoryWidgetContainer>
    );
  }
}

function mapStateToProps(state) {
  return {
    isLoading: state.storyWidgetReducer.get("isLoading"),
    stories: state.storyWidgetReducer.get("stories"),
    isDeleteSuccess: state.storyTabReducer.get("isDeleteSuccess")
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchRecentStories: () => dispatch(fetchRecentStories()),
    push: url => dispatch(push(url))
  };
}

StoryWidget.propTypes = {
  push: PropTypes.func,
  fetchRecentStories: PropTypes.func.isRequired,
  stories: PropTypes.array,
  isLoading: PropTypes.bool,
  isDeleteSuccess: PropTypes.bool,
  resetState: PropTypes.func.isRequired
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StoryWidget);

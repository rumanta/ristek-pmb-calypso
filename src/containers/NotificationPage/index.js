import React, { Component } from "react";
import { NotificationPageContainer } from "./style";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import SwipeToDelete from "react-swipe-to-delete-component";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";
import PaginationContainer from "components/PaginationContainer";
import PlaceholderPicture from "assets/placeholder_profile.png";
import CloseIcon from "assets/icons/close.svg";

import { parseDataToMessageAndPicture } from "./util";

import { fetchNotificationsList, removeNotification } from "./actions";
import {
  getNotifications,
  isNotificationsLoading,
  isNotificationsLoaded,
  isRemoveNotificationLoading
} from "selectors/notifications";
import { isMaba } from "selectors/user";

class NotificationPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screenWidth: window.innerWidth
    };
  }

  componentDidMount() {
    window.addEventListener("resize", this.updateScreenWidth);
    this.props.fetchNotificationsList();
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateScreenWidth);
  }

  updateScreenWidth = () => {
    this.setState({ screenWidth: window.innerWidth });
  };

  handleDelete = id => this.props.removeNotification(id);

  renderDesktopNotifications = () =>
    this.props.notifications &&
    this.props.notifications
      .filter(item => item.related_object)
      .map(item => {
        const data = parseDataToMessageAndPicture(item, this.props.isMaba);

        return (
          <ReactCSSTransitionGroup
            key={`notification${item.id}`}
            transitionName="course-item"
            transitionLeave={true}
            transitionAppear={true}
            transitionAppearTimeout={2500}
            transitionEnterTimeout={1700}
            transitionLeaveTimeout={1000}
          >
            <li className="notification-card">
              <div className="card-wrapper">
                <div
                  className={
                    data.isFriend ? "picture-profile-cont" : "picture-cont"
                  }
                  style={{
                    backgroundImage: `url(${
                      data.photoUrl ? data.photoUrl : PlaceholderPicture
                    })`
                  }}
                />
                <div className="message-cont">{data.message}</div>
              </div>
              <div className="close-btn-cont">
                <img
                  src={CloseIcon}
                  className="close-icon"
                  onClick={() => this.handleDelete(item.id)}
                />
              </div>
            </li>
          </ReactCSSTransitionGroup>
        );
      });

  renderMobileNotifications = () =>
    this.props.notifications &&
    this.props.notifications
      .filter(item => item.related_object)
      .map(item => {
        const data = parseDataToMessageAndPicture(item, this.props.isMaba);

        return (
          <SwipeToDelete
            key={item.id}
            tag="li"
            classNameTag="tw feed"
            onDelete={() => this.handleDelete(item.id)}
          >
            <li className="notification-card">
              <div
                className={
                  data.isFriend ? "picture-profile-cont" : "picture-cont"
                }
                style={{
                  backgroundImage: `url(${
                    data.photoUrl ? data.photoUrl : PlaceholderPicture
                  })`
                }}
              />
              <div className="message-cont">{data.message}</div>
            </li>
          </SwipeToDelete>
        );
      });

  renderNotifications = () => {
    let result = this.renderMobileNotifications();

    if (this.state.screenWidth > 1024) {
      result = this.renderDesktopNotifications();
    }

    return result || [];
  };

  render() {
    return (
      <NotificationPageContainer>
        <PaginationContainer
          header={<p className="title">Notifications</p>}
          isLoading={this.props.isNotificationsLoading}
          emptyText={"There isn't any new Notifications yet"}
          isNotification={true}
          itemsPerPage={8}
        >
          {this.renderNotifications()}
        </PaginationContainer>
      </NotificationPageContainer>
    );
  }
}

NotificationPage.propTypes = {
  notifications: PropTypes.arrayOf(PropTypes.any),
  isNotificationsLoading: PropTypes.bool,
  isNotificationsLoaded: PropTypes.bool,
  isRemoveNotificationLoading: PropTypes.bool,
  fetchNotificationsList: PropTypes.func,
  removeNotification: PropTypes.func,
  isMaba: PropTypes.bool
};

const mapStateToProps = state => ({
  notifications: getNotifications(state),
  isNotificationsLoading: isNotificationsLoading(state),
  isNotificationsLoaded: isNotificationsLoaded(state),
  isRemoveNotificationLoading: isRemoveNotificationLoading(state),
  isMaba: isMaba(state)
});

const mapDispatchToProps = dispatch => ({
  fetchNotificationsList: () => dispatch(fetchNotificationsList()),
  removeNotification: id => dispatch(removeNotification(id))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NotificationPage);

import styled from "styled-components";

export const NotificationPageContainer = styled.div`

  .notification-card > div {
    padding: 0.25rem;
  }

  .notification-card {
    position: relative;
    padding: 0.8rem;
    margin-bottom: 0.5rem;
    box-shadow: ${props => props.theme.smallBoxShadow};
    background-color: ${props => props.theme.colors.white};
    border-radius: 7px;
    z-index: 3;
    display: flex;
    justify-content: space-between;

    p, a {
      font-size: 14px;
      color: ${props => props.theme.colors.primaryBlack};
    }

    a {
    text-decoration: none; /* no underline */
  }

    a:hover {
      text-decoration: underline;
    }
  }

  .card-wrapper {
    display: flex;
    align-items: center;
  }

  .picture-cont {
    min-width: 30px;
    max-width: 30px;
    height: 30px;
    background-position: center;
    background-size: cover;
  }

  .picture-profile-cont {
    min-width: 30px;
    max-width: 30px;
    height: 30px;
    border-radius: 50%;
    background-position: center;
    background-size: cover;
    border-width: 1px;
    border-color: ${props => props.theme.colors.blue};
    border-style: solid;
  }

  .close-btn-cont {
    display: flex;
    flex-direction: flex-end;
    cursor: pointer;
  }

  .close-icon {
    width: 12px;
  }

  .message-cont {
    margin-left: 0.5rem;
  }

  .title {
      font-size: 20px;
    }
  }

  @media (max-width: 1024px) {
    .notification-card {
      justify-content: flex-start;
      align-items: center;
    }
  }

  @media (max-width: 600px) {
    .title {
      font-size: 16px;
    }
  }

  @media (max-width: 500px) {
    .notification-card {
      p, a {
        font-size: 12px;
      }
    }

    .picture-cont {
      min-width: 20px;
      max-width: 20px;
      height: 20px;
    }
  }
`;

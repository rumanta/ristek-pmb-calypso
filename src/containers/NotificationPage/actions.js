import axios from "axios";
import {
  FETCH_NOTIFICATIONS_LIST,
  FETCH_NOTIFICATIONS_LIST_SUCCESS,
  FETCH_NOTIFICATIONS_LIST_FAILED,
  REMOVE_NOTIFICATION,
  REMOVE_NOTIFICATION_SUCCESS,
  REMOVE_NOTIFICATION_FAILED
} from "./constants";

import { fetchNotificationsApi, setNotificationAsViewedApi } from "api";

export function fetchNotificationsList() {
  return dispatch => {
    dispatch({ type: FETCH_NOTIFICATIONS_LIST });
    axios
      .get(fetchNotificationsApi)
      .then(response => {
        dispatch({
          payload: response.data,
          type: FETCH_NOTIFICATIONS_LIST_SUCCESS
        });
      })
      .catch(error => {
        dispatch({
          payload: error,
          type: FETCH_NOTIFICATIONS_LIST_FAILED
        });
      });
  };
}

export function removeNotification(id) {
  return dispatch => {
    dispatch({ type: REMOVE_NOTIFICATION });
    axios
      .patch(setNotificationAsViewedApi(id))
      .then(response => {
        dispatch({
          payload: response.data,
          objectId: id,
          type: REMOVE_NOTIFICATION_SUCCESS
        });
      })
      .catch(error => {
        dispatch({
          payload: error,
          objectId: id,
          type: REMOVE_NOTIFICATION_FAILED
        });
      });
  };
}

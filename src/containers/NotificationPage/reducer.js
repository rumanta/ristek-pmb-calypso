/* eslint-disable no-case-declarations */
import { fromJS } from "immutable";
import {
  FETCH_NOTIFICATIONS_LIST,
  FETCH_NOTIFICATIONS_LIST_SUCCESS,
  FETCH_NOTIFICATIONS_LIST_FAILED,
  REMOVE_NOTIFICATION,
  REMOVE_NOTIFICATION_SUCCESS,
  REMOVE_NOTIFICATION_FAILED
} from "./constants";

const initialState = fromJS({
  error: null,
  isLoaded: false,
  isLoading: false,
  isRemoveLoading: false,
  notifications: []
});

function notificationPageReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_NOTIFICATIONS_LIST:
      return state.set("isLoading", true);
    case FETCH_NOTIFICATIONS_LIST_SUCCESS:
      return state
        .set("notifications", action.payload)
        .set("isLoading", false)
        .set("isLoaded", true);
    case FETCH_NOTIFICATIONS_LIST_FAILED:
      return state
        .set("error", action.payload)
        .set("isLoaded", false)
        .set("isLoading", false);
    case REMOVE_NOTIFICATION:
      return state.set("isRemoveLoading", true);
    case REMOVE_NOTIFICATION_SUCCESS:
      const viewedNotifId = action.objectId;
      const oldNotifs = state.get("notifications");
      const newNotifs = oldNotifs.filter(notif => notif.id !== viewedNotifId);

      return state
        .set("isRemoveLoading", false)
        .set("notifications", newNotifs);
    case REMOVE_NOTIFICATION_FAILED:
      return state.set("error", action.payload).set("isRemoveLoading", false);
    default:
      return state;
  }
}

export default notificationPageReducer;

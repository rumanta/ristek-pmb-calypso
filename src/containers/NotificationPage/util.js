/* eslint-disable react/no-unescaped-entities */
import React from "react";
import EventIcon from "assets/icons/calendar.svg";
import AnnouncementIcon from "assets/icons/promotion.svg";
import TaskIcon from "assets/icons/paper.svg";
import FeedbackIcon from "assets/icons/love.svg";

const KENALAN_ACCEPTED = "KENALAN_ACCEPTED";
const KENALAN_REJECTED = "KENALAN_REJECTED";
const KENALAN_PENDING = "KENALAN_PENDING";
const NEW_STORY = "NEW_STORY";
const NEW_FEEDBACK = "NEW_FEEDBACK";
const NEW_EVENT = "NEW_EVENT";
const NEW_TASK = "NEW_TASK";
const NEW_ANNOUNCEMENT = "NEW_ANNOUNCEMENT";

const kenalanUrl = id => `formfriend/${id}`;
const announcementUrl = id => `announcements/${id}`;
const eventUrl = id => `events/${id}`;
const taskUrl = id => `tasks/${id}`;
const feedbackUrl = () => `profile?=feedback`;

export const parseDataToMessageAndPicture = (data, isMaba) => {
  if (!data) {
    return {};
  }

  let message = null;
  let isFriend = false;
  let photoUrl = null;

  if (data.related_object_type.model === "kenalan") {
    if (isMaba) {
      photoUrl = data.related_object.user_elemen.foto;
    } else {
      photoUrl = data.related_object.user_maba.foto;
    }

    isFriend = true;

    switch (data.notification_type) {
      case KENALAN_ACCEPTED:
        message = (
          <a href={kenalanUrl(data.related_object_id)}>
            <b>{data.related_object.user_elemen.profil.nama_lengkap}</b>
            &thinsp;accepted your friends request. Congrats!
          </a>
        );
        break;
      case KENALAN_PENDING:
        message = (
          <a href={kenalanUrl(data.related_object_id)}>
            <b>{data.related_object.user_maba.profil.nama_lengkap}</b>&thinsp;
            has sent you a friend request.
          </a>
        );
        break;
      case KENALAN_REJECTED:
        message = (
          <a href={kenalanUrl(data.related_object_id)}>
            <b>{data.related_object.user_elemen.profil.nama_lengkap}</b>
            &thinsp;rejected your friend request. Let's see why!
          </a>
        );
        break;
      default:
        message = "";
    }
  } else if (data.notification_type === NEW_ANNOUNCEMENT) {
    photoUrl = AnnouncementIcon;
    message = (
      <a href={announcementUrl(data.related_object_id)}>
        A new announcement has announced:&thinsp;
        <b>{data.related_object.title}</b>
      </a>
    );
  } else if (data.notification_type === NEW_EVENT) {
    photoUrl = EventIcon;
    message = (
      <a href={eventUrl(data.related_object_id)}>
        A new event has announced:&thinsp;
        <b>{data.related_object.title}</b>
      </a>
    );
  } else if (data.notification_type === NEW_FEEDBACK) {
    photoUrl = FeedbackIcon;
    message = (
      <a href={feedbackUrl()}>
        <b>{data.related_object.initial}</b>&thinsp;has given you a feedback for
        your task, feelin' nervous?
      </a>
    );
  } else if (data.notification_type === NEW_TASK) {
    photoUrl = TaskIcon;
    message = (
      <a href={taskUrl(data.related_object_id)}>
        A new task has released:&thinsp;
        <b>{data.related_object.title}</b>
      </a>
    );
  } else if (data.notification_type === NEW_STORY) {
    if (data.related_object) {
      photoUrl = data.related_object.user.user_detail.profil.foto;
      isFriend = true;
      message = (
        <a
          href={data.related_object.link}
          target="_blank"
          rel="noopener noreferrer"
        >
          {data.related_object.user.user_detail.profil.nama_lengkap}
          &thinsp;posted a new story!&thinsp;Eager to find out?
        </a>
      );
    }
  }

  return {
    message,
    isFriend,
    photoUrl
  };
};

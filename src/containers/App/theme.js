export const theme = {
  boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
  smallBoxShadow: "0px 2px 2px rgba(0, 0, 0, 0.25)",

  colors: {
    black: "#000000",
    primaryBlack: "#333333",
    secondaryBlack: "#343542",
    green: "#32C238",
    lightBlue: "#1E89E2",
    blue: "#4461A7",
    darkBlue: "#1D4266",
    gray: "#999999",
    darkGray: "#777777",
    orange: "#FFA400",
    lightRed: "#F76B6B",
    red: "#CF5061",
    redBorder: "#F44336",
    white: "#FFFFFF",
    boneWhite: "#F7F8F9",
    deleteRed: "#CC4040",
    lightGray: "#BBB2C4",
    fieldGray: "#FCFCFC",
    semiGray: "#C2C2C2"
  },

  sizes: {
    tablet: "769px",
    desktop: "1024px",
    widescreen: "1216px",
    fullhd: "1408px"
  },

  weight: {
    regular: 400,
    bold: 500
  }
};

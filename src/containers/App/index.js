/* eslint-disable */
import React from "react";
import { Switch, Route, withRouter, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { ThemeProvider } from "styled-components";

import { theme } from "./theme";
import { routes } from "./routes";
import { AppContainer } from "./style";

import auth from "auth";
import NavBar from "components/NavBar";
import ContentLayout from "../ContentLayout";
import Footer from "components/Footer";
import { history } from "store";

import { ConnectedRouter } from "connected-react-router";
import { getServerTime, setServerTime } from "globalActions";

import { fetchProfile } from "containers/ProfilePage/actions";
import { isEmpty } from "lodash";

const ProtectedRoute = ({
  component: Component,
  path,
  profile,
  isUserMaba,
  kelompok,
  ...rest
}) => (
  <Route
    {...rest}
    render={props => {
      if (auth.loggedIn()) {
        if (
          path !== "/editprofile" &&
          !profile.isEmpty() &&
          (!profile.get("tempat_lahir") ||
            !profile.get("tanggal_lahir") ||
            !profile.get("asal_sekolah") ||
            !profile.get("line_id") ||
            (isUserMaba && isEmpty(kelompok)))
        ) {
          return <Redirect to="/editprofile" />;
        }
        return <Component {...props} />;
      }
      return <Redirect to={{ pathname: "/login" }} />;
    }}
  />
);

export class App extends React.Component {
  // componentDidMount() {
  //   if (auth.loggedIn() && !this.props.hasLoaded) {
  //     this.props.getServerTime();
  //   }
  // }

  // componentDidUpdate(prevProps, prevState) {
  //   if (prevProps.hasLoaded !== this.props.hasLoaded) {
  //     setInterval(() => {
  //       this.props.setServerTime(this.incrementSeconds(this.props.serverTime));
  //     }, 1000);
  //   }
  // }

  // incrementSeconds(time) {
  //   return moment(time)
  //     .add(1, "seconds")
  //     .toString();
  // }

  render() {
    if (auth.loggedIn() && this.props.profil.isEmpty()) {
      this.props.fetchProfile();
      this.props.getServerTime();
    }

    const pages = routes.map((route, i) => {
      if (route.protected) {
        return (
          <ProtectedRoute
            component={route.component}
            exact={route.exact}
            path={route.path}
            key={i}
            profile={this.props.profil}
            isUserMaba={this.props.isUserMaba}
            kelompok={this.props.kelompok}
          />
        );
      }
      return (
        <Route
          component={route.component}
          exact={route.exact}
          path={route.path}
          key={i}
        />
      );
    });
    return (
      <ConnectedRouter history={history}>
        <ThemeProvider theme={theme}>
          <AppContainer>
            <>{window.location.pathname === "/login" ? null : <NavBar />}</>
            <ContentLayout location={this.props.location}>
              <Switch>{pages}</Switch>
            </ContentLayout>
            <>{window.location.pathname === "/login" ? null : <Footer />}</>
          </AppContainer>
        </ThemeProvider>
      </ConnectedRouter>
    );
  }
}

function mapStateToProps(state) {
  return {
    profil: state.profileReducer.get("profil"),
    isUserMaba: state.profileReducer.get("isUserMaba"),
    kelompok: state.profileReducer.get("kelompok"),
    serverTime: state.global.get("serverTime"),
    hasLoaded: state.global.get("hasLoaded")
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchProfile: () => dispatch(fetchProfile()),
    getServerTime: () => dispatch(getServerTime()),
    setServerTime: time => dispatch(setServerTime(time))
  };
}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(App)
);

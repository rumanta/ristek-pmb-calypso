/* eslint-disable object-property-newline */
import HomePage from "../../containers/HomePage";
import NotFoundPage from "../../components/NotFoundPage";
import ProfilePage from "../../containers/ProfilePage";
import EditProfilePage from "../../containers/EditProfilePage";
import LandingPage from "../../containers/LandingPage";
import AboutPage from "containers/AboutPage";
// import Announcement from "../Announcement";
// import EventPage from "containers/EventPage";
// import DetailPage from "containers/DetailPage";
import FriendPage from "containers/FriendPage";
import AnnouncementListPage from "containers/AnnouncementListPage";
import EventListPage from "containers/EventListPage";
// import GalleryPage from "../../containers/GalleryPage";
import NeedHelpPage from "containers/NeedHelpPage";
import EventDetailPage from "containers/EventDetailPage";
import AnnouncementDetailPage from "../AnnouncementDetailPage";
import Task from "../TaskPage";
import TaskDetailPage from "containers/TaskDetailPage";
import EditProfileInterestPage from "containers/EditProfileInterestPage";
import FeedbackTab from "../FeedbackTab";
import FormFriendPage from "containers/FormFriendPage";
import NewStoryPage from "containers/NewStoryPage";
import StoryPage from "containers/StoryPage";
import EditStoryPage from "containers/EditStoryPage";
import NotificationPage from "containers/NotificationPage";

export const routes = [
  {
    component: HomePage,
    exact: true,
    path: "/",
    protected: true
  },
  {
    component: LandingPage,
    exact: true,
    path: "/login"
  },
  {
    component: Task,
    exact: true,
    path: "/tasks",
    protected: true
  },
  {
    component: AboutPage,
    exact: true,
    path: "/about",
    protected: true
  },
  {
    component: AnnouncementListPage,
    exact: true,
    path: "/announcements",
    protected: true
  },
  {
    component: AnnouncementDetailPage,
    exact: true,
    path: "/announcements/:slug",
    protected: true
  },
  // {
  //   component: GalleryPage,
  //   exact: true,
  //   path: "/gallery"
  // },
  {
    component: ProfilePage,
    exact: true,
    path: "/profile",
    protected: true
  },
  {
    component: FriendPage,
    exact: true,
    path: "/friends",
    protected: true
  },
  {
    // for developing purposes
    component: EditProfilePage,
    exact: true,
    path: "/editprofile",
    protected: true
  },
  {
    component: NeedHelpPage,
    exact: true,
    path: "/need-help",
    protected: true
  },
  {
    component: EventListPage,
    exact: true,
    path: "/events",
    protected: true
  },
  {
    component: EventDetailPage,
    exact: true,
    path: "/events/:slug",
    protected: true
  },
  {
    component: TaskDetailPage,
    exact: true,
    path: "/tasks/:id",
    protected: true
  },
  {
    // protected: true
    component: FeedbackTab,
    exact: true,
    path: "/feedback",
    protected: true
  },
  {
    component: FormFriendPage,
    exact: true,
    path: "/formfriend/:id",
    protected: true
  },
  {
    component: EditProfileInterestPage,
    exact: true,
    path: "/edit-interest",
    protected: true
  },
  {
    component: NewStoryPage,
    exact: true,
    path: "/new-story",
    protected: true
  },
  {
    component: EditStoryPage,
    exact: true,
    path: "/stories/edit/:id",
    protected: true
  },
  {
    component: StoryPage,
    exact: true,
    path: "/stories",
    protected: true
  },
  {
    component: NotificationPage,
    exact: true,
    path: "/notifications",
    protected: true
  },
  { component: NotFoundPage, protected: true }
];

import { fromJS } from "immutable";
import {
  FETCH_GROUP,
  FETCH_GROUP_SUCCESS,
  FETCH_GROUP_FAILED,
  PATCH_EDIT_PROFILE,
  PATCH_EDIT_PROFILE_SUCCESS,
  PATCH_EDIT_PROFILE_FAILED,
  UPLOAD_PROFILE_PICTURE,
  UPLOAD_PROFILE_PICTURE_SUCCESS,
  UPLOAD_PROFILE_PICTURE_FAILED,
  RESET_STATE
} from "./constants";

const initialState = fromJS({
  error: null,
  isLoading: false,
  isSuccess: false,
  kelompoks: [],
  isFetchGroupSuccess: false
});

export default function editProfileReducer(state = initialState, action) {
  switch (action.type) {
    case UPLOAD_PROFILE_PICTURE:
      return state.set("isLoading", true);
    case PATCH_EDIT_PROFILE:
      return state.set("isLoading", true);
    case UPLOAD_PROFILE_PICTURE_SUCCESS:
      return state.set("isLoading", true);
    case UPLOAD_PROFILE_PICTURE_FAILED:
      return state.set("isLoading", false).set("error", action.payload);
    case PATCH_EDIT_PROFILE_SUCCESS:
      return state.set("isLoading", false).set("isSuccess", true);
    case PATCH_EDIT_PROFILE_FAILED:
      return state.set("error", action.payload).set("isLoading", false);
    case RESET_STATE:
      return state
        .set("error", null)
        .set("isLoading", false)
        .set("isSuccess", false);
    case FETCH_GROUP:
      return state.set("isLoading", true);
    case FETCH_GROUP_SUCCESS:
      return state
        .set("kelompoks", action.payload)
        .set("isLoading", false)
        .set("isFetchGroupSuccess", true);
    case FETCH_GROUP_FAILED:
      return state.set("error", action.payload).set("isLoading", false);
    default:
      return state;
  }
}

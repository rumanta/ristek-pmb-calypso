export const FETCH_GROUP = "src/EditProfilePage/FETCH_GROUP";

export const FETCH_GROUP_SUCCESS = "src/EditProfilePage/FETCH_GROUP_SUCCESS";

export const FETCH_GROUP_FAILED = "src/EditProfilePage/FETCH_GROUP_FAILED";

export const PATCH_EDIT_PROFILE = "src/EditProfilePage/PATCH_EDIT_PROFILE";

export const PATCH_EDIT_PROFILE_SUCCESS =
  "src/EditProfilePage/PATCH_EDIT_PROFILE_SUCCESS";

export const PATCH_EDIT_PROFILE_FAILED =
  "src/EditProfilePage/PATCH_EDIT_PROFILE_FAILED";

export const UPLOAD_PROFILE_PICTURE =
  "src/EditProfilePage/UPLOAD_PROFILE_PICTURE";

export const UPLOAD_PROFILE_PICTURE_SUCCESS =
  "src/EditProfilePage/UPLOAD_PROFILE_PICTURE_SUCCESS";

export const UPLOAD_PROFILE_PICTURE_FAILED =
  "src/EditProfilePage/UPLOAD_PROFILE_PICTURE_FAILED";

export const RESET_STATE = "src/EditProfilePage/RESET_STATE";

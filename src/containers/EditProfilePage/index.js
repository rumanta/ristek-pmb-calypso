/* eslint-disable react/no-unescaped-entities */
import React from "react";
import { connect } from "react-redux";
import { EditProfilePageContainer } from "./style";
import Card from "../../components/Card";
import { Buttons } from "../../components/Buttons";
import { ButtonInput } from "../../components/ButtonInput";
import PropTypes from "prop-types";
import { fetchProfile } from "../ProfilePage/actions";
import { fetchGroup } from "../EditProfilePage/actions";
import { editProfile, resetState } from "./actions";
import Loading from "components/Loading";
import ProfilePicture from "components/ProfilePicture";
import { push } from "connected-react-router";
import { isEmpty } from "lodash";

import swal from "sweetalert";

/*
export const FormError = ({ formErrors }) => (
  <div className="formErrors">
    {Object.keys(formErrors).map((fieldName, id) => {
      if (formErrors[fieldName] === false) {
        return <small key={id}>{fieldName} is invalid</small>
      }

      return "";
    })}
  </div>
);
*/

class EditProfilePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      profilId: null,
      name: null,
      position: null,
      major: null,
      highSchool: null,
      placeOfBirth: null,
      dateOfBirth: null,
      email: null,
      line: null,
      linkedin: null,
      isPrivate: false,
      profileImage: null,
      image: null,
      about: null,
      no_tlp: null,
      nameValid: true,
      emailValid: true,
      formValid: true,
      formError: { email: true }
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.validateField = this.validateField.bind(this);
    this.onChange = this.onChange.bind(this);
    this.deletePhoto = this.deletePhoto.bind(this);
    this.shouldUpload = this.shouldUpload.bind(this);
    this.formatInput = this.formatInput.bind(this);
    this.generateFields = this.generateFields.bind(this);
  }

  // Tribute, thanks to andre for this getDerivedStateFromProps life cycle instead of willReceiveProps
  static getDerivedStateFromProps(nextProps, prevState) {
    const profil = nextProps.profil;
    const title = nextProps.title;
    const kelompok = nextProps.kelompok;
    const isUserMaba = nextProps.isUserMaba;
    const angkatan = nextProps.angkatan;
    if (nextProps.profilId !== prevState.profilId) {
      return {
        profilId: nextProps.profilId,
        profileImage: profil.get("foto"),
        name: profil.get("nama_lengkap"),
        major: `Fasilkom ${angkatan.get("tahun")}`,
        position: isUserMaba ? kelompok.id : title,
        highSchool: profil.get("asal_sekolah"),
        placeOfBirth: profil.get("tempat_lahir"),
        dateOfBirth: profil.get("tanggal_lahir"),
        email: profil.get("email"),
        line: profil.get("line_id"),
        linkedin: profil.get("linkedin"),
        isPrivate: profil.get("is_private")
      };
    }
    if (nextProps.kelompoks[0]) {
      if (isUserMaba && (kelompok.size === 0 || isEmpty(kelompok))) {
        return {
          profilId: nextProps.profilId,
          profileImage: profil.get("foto"),
          name: profil.get("nama_lengkap"),
          major: `Fasilkom ${angkatan.get("tahun")}`,
          highSchool: profil.get("asal_sekolah"),
          placeOfBirth: profil.get("tempat_lahir"),
          dateOfBirth: profil.get("tanggal_lahir"),
          email: profil.get("email"),
          line: profil.get("line_id"),
          linkedin: profil.get("linkedin"),
          position: nextProps.kelompoks[0].id,
          isPrivate: profil.get("is_private")
        };
      }
    }

    return null;
  }

  shouldUpload() {
    return {
      delete: !this.state.profileImage && !this.state.image,
      upload: this.state.image
    };
  }

  handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    this.setState({ [name]: value }, () => {
      this.validateField(name, value);
    });
  }

  componentDidMount() {
    this.props.fetchGroup();
    this.props.fetchProfile();
  }

  componentDidUpdate() {
    if (this.props.isEditSuccess) {
      this.props.resetState();
      this.props.push("/profile");
    }
  }

  formatInput(event) {
    const attribute = event.target.getAttribute("name");
    this.setState({ [attribute]: event.target.value.trim() });
  }

  validateField(name, value) {
    let emailValid = this.state.emailValid;
    const formErrors = this.state.formError;

    switch (name) {
      case "email":
        emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
        formErrors.email = Boolean(emailValid);
        break;
      default:
    }

    this.setState({
      formError: formErrors,
      emailValid
    });
  }

  deletePhoto() {
    this.setState({
      profileImage: null,
      image: null
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    const errors = this.state.formError;
    Object.keys(errors).forEach(err => {
      if (errors[err]) {
        this.setState({ formValid: false });
      }
    });

    const formData = new FormData();
    formData.append("file", this.state.image);

    const obj = {
      id: this.state.profilId,
      profil: {
        about: this.state.about,
        no_telepon: this.state.no_tlp,
        tempat_lahir: this.state.placeOfBirth,
        tanggal_lahir: this.state.dateOfBirth,
        asal_sekolah: this.state.highSchool,
        line_id: this.state.line,
        linkedin: this.state.linkedin,
        is_private: this.state.isPrivate
      },
      title: this.props.isUserMaba ? null : this.state.position,
      kelompok: this.props.isUserMaba ? { id: this.state.position } : null
    };
    this.props.editProfile(formData, obj, this.shouldUpload());
  }

  onChange(err) {
    const imageMaxSize = 1000000;
    if (err.target.files[0].size > imageMaxSize) {
      swal(
        "Error",
        `Image maximum size is ${imageMaxSize / 1000000} MB`,
        "error"
      );
    } else {
      this.setState({
        profileImage: URL.createObjectURL(err.target.files[0]),
        image: err.target.files[0]
      });
    }
  }

  generateFields(property, index, classname, val) {
    if (property === "email") {
      return (
        <input
          disabled
          className={classname}
          name={property}
          value={val[index]}
        />
      );
    } else if (
      property === "position" &&
      this.props.isUserMaba &&
      this.props.isFetchGroupSuccess
    ) {
      return (
        <select name={property} onChange={this.handleChange}>
          {this.props.kelompoks.map((kelompok, ind) => {
            if (this.state.position && this.state.position === kelompok.id) {
              return (
                <option key={ind} selected value={kelompok.id}>{`${
                  kelompok.kelompok_besar.nama
                } - ${kelompok.kelompok_kecil.nama}`}</option>
              );
            }

            return (
              <option key={ind} value={kelompok.id}>{`${
                kelompok.kelompok_besar.nama
              } - ${kelompok.kelompok_kecil.nama}`}</option>
            );
          })}
        </select>
      );
    } else if (property === "dateOfBirth") {
      return (
        <input
          type="date"
          className={classname}
          min="1899-01-01"
          max="2019-01-01"
          name={property}
          value={val[index]}
          onChange={this.handleChange}
          onBlur={this.formatInput}
          required={property !== "linkedin" && property !== "position"}
        />
      );
    }

    return (
      <input
        type="text"
        className={classname}
        name={property}
        value={val[index]}
        onChange={this.handleChange}
        onBlur={this.formatInput}
        required={property !== "linkedin" && property !== "position"}
      />
    );
  }

  render() {
    const isLoadingProfile = this.props.isLoadingProfile;
    const isLoadingEdit = this.props.isLoadingEdit;
    const isUserMaba = this.props.isUserMaba;

    const profile = {
      profileImage: this.state.profileImage,
      name: this.state.name,
      major: this.state.major,
      position: this.state.position,
      highSchool: this.state.highSchool,
      placeOfBirth: this.state.placeOfBirth,
      dateOfBirth: this.state.dateOfBirth,
      email: this.state.email,
      line: this.state.line,
      linkedin: this.state.linkedin
    };

    const mapLibrary = {
      position: "Title",
      highSchool: "High School",
      placeOfBirth: "Birth Place",
      dateOfBirth: "Birth Date",
      email: "Email",
      line: "LINE ID",
      linkedin: "Website"
    };

    const renderFields = (properties, classname, val) =>
      properties.map((property, index) => (
        <label key={index}>
          <p>
            {property === "position" && isUserMaba
              ? "Kelompok PMB"
              : mapLibrary[property]}
          </p>
          {this.generateFields(property, index, classname, val)}
        </label>
      ));

    return isLoadingEdit || isLoadingProfile ? (
      <Loading />
    ) : (
      <EditProfilePageContainer>
        <h1>Edit Profile</h1>
        <Card>
          <div className="cardContainer">
            <form onSubmit={this.handleSubmit} action="">
              <div className="topSection">
                <div className="profileImageSection">
                  <ProfilePicture src={profile.profileImage} />
                  <ButtonInput
                    name="Change Photo Profile"
                    wide="12rem"
                    onChange={this.onChange}
                    accept="image/*"
                  />
                  <Buttons
                    name="Delete Photo Profile"
                    type="button"
                    delete={true}
                    wide="12rem"
                    onPressed={this.deletePhoto}
                  />
                </div>
                <div className="titleSection">
                  <h1>{profile.name}</h1>
                  <h3>{profile.major}</h3>
                </div>
              </div>
              <div className="profileSection">
                <div className="formSection">
                  <div className="titleForm">
                    {renderFields(
                      Object.keys(profile).slice(3, 4),
                      "titleField",
                      Object.values(profile).slice(3, 4)
                    )}
                  </div>
                  <div className="lowerForm">
                    <div className="leftLowerForm">
                      {renderFields(
                        Object.keys(profile).slice(4, 7),
                        "leftField",
                        Object.values(profile).slice(4, 7)
                      )}
                    </div>
                    <div className="rightLowerForm">
                      {renderFields(
                        Object.keys(profile).slice(7),
                        "rightField",
                        Object.values(profile).slice(7)
                      )}
                    </div>
                  </div>
                  <div className="footerForm">
                    <input
                      className="styled-checkbox"
                      id="styled-checkbox-1"
                      type="checkbox"
                      checked={this.state.isPrivate}
                      onChange={() =>
                        this.setState({ isPrivate: !this.state.isPrivate })
                      }
                    />
                    <label htmlFor="styled-checkbox-1">Set Private Mode</label>
                    <span className="tooltip-text">
                      Check this box if you don't want people to find you in
                      Find Friends Page
                    </span>
                  </div>
                  <br />
                  <div className="submitSection">
                    <Buttons name="Save" type="submit" wide="8rem" />
                  </div>
                </div>
              </div>
            </form>
          </div>
        </Card>
      </EditProfilePageContainer>
    );
  }
}

EditProfilePage.propTypes = {
  fetchProfile: PropTypes.func.isRequired,
  editProfile: PropTypes.func.isRequired,
  fetchGroup: PropTypes.func.isRequired,
  profilId: PropTypes.number,
  isLoadingProfile: PropTypes.bool,
  isLoadingEdit: PropTypes.bool,
  isUserMaba: PropTypes.bool,
  profil: PropTypes.object.isRequired,
  title: PropTypes.string,
  kelompok: PropTypes.string,
  angkatan: PropTypes.object.isRequired,
  isEditSuccess: PropTypes.bool,
  isFetchGroupSuccess: PropTypes.bool,
  kelompoks: PropTypes.array,
  push: PropTypes.func.isRequired,
  resetState: PropTypes.func.isRequired
};

function mapStateToProps(state) {
  return {
    profilId: state.profileReducer.get("profilId"),
    isLoadingProfile: state.profileReducer.get("isLoading"),
    isUserMaba: state.profileReducer.get("isUserMaba"),
    profil: state.profileReducer.get("profil"),
    title: state.profileReducer.get("title"),
    kelompok: state.profileReducer.get("kelompok"),
    angkatan: state.profileReducer.get("angkatan"),
    isLoadingEdit: state.editProfileReducer.get("isLoading"),
    isEditSuccess: state.editProfileReducer.get("isSuccess"),
    isFetchGroupSuccess: state.editProfileReducer.get("isFetchGroupSuccess"),
    kelompoks: state.editProfileReducer.get("kelompoks")
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchProfile: () => dispatch(fetchProfile()),
    editProfile: (formData, object, shouldUpload) =>
      dispatch(editProfile(formData, object, shouldUpload)),
    resetState: () => dispatch(resetState()),
    fetchGroup: () => dispatch(fetchGroup()),
    push: url => dispatch(push(url))
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditProfilePage);

import styled from "styled-components";

export const EditProfilePageContainer = styled.div`
  @import url("https://fonts.googleapis.com/css?family=Rubik:400,500,700&display=swap");
  font-family: Rubik;

  .cardContainer {
    display: flex;
    flex-direction: column;
    justify-content: center;
    padding: 1rem;
  }

  .topSection {
    display: flex;
    flex-direction: row;
  }

  .profileImageSection {
    display: flex;
    flex-grow: 2;
    justify-content: center;
    align-items: center;
    flex-direction: column;
  }

  .profileImageSection div {
    margin-bottom: 0.5rem;
  }

  h1 {
    font-size: 1.5rem;
    font-weight: ${props => props.theme.weight.regular};
  }

  .titleSection {
    width: 100%;
    margin: 0 0 1rem 1rem;
  }

  .titleSection h1,
  h3 {
    margin: 0.5rem;
  }

  .titleSection h1 {
    font-weight: ${props => props.theme.weight.bold};
    font-size: 2rem;
    color: ${props => props.theme.colors.primaryBlack};
  }

  .titleSection h3 {
    font-weight: ${props => props.theme.weight.regular};
    color: ${props => props.theme.colors.gray};
    font-size: 1.25rem;
  }

  .profileSection {
    margin: 1rem 0 1rem 0;
  }

  .titleForm {
    width: 100%;
    margin-bottom: 1rem;
  }

  .titleField,
  .leftField,
  .rightField {
  }

  .lowerForm {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
  }

  .lowerForm div {
    width: 48%;
  }

  .submitSection {
    display: flex;
    justify-content: flex-end;
  }

  label {
    width: 100%;
    margin-bottom: 1rem;
  }

  label p {
    margin-bottom: 0.4rem;
  }

  label.uploadLabel {
    margin-bottom: 0;
  }

  .titleField,
  .leftField,
  .rightField {
    border: 1px solid ${props => props.theme.colors.lightGray};
    border-radius: 5px;
    height: 1.25rem;
    background-color: ${props => props.theme.colors.fieldGray};
    outline: none;
    padding: 5px;
    margin: 0 0 1rem 0;
    width: 100%;
  }

  .titleField,
  .leftField,
  .rightField:focus {
    background-color: ${props => props.theme.colors.white};
  }

  .checkBox {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    height: 0;
    width: 0;
  }

  .styled-checkbox {
    position: absolute;
    opacity: 0; // hide it

    & + label {
      position: relative;
      cursor: pointer;
      padding: 0;
    }

    & + label:before {
      content: "";
      margin-right: 10px;
      display: inline-block;
      vertical-align: text-top;
      width: 20px;
      height: 20px;
      background-color: ${props => props.theme.colors.boneWhite};
      border: 1px solid ${props => props.theme.colors.lightGray};
      border-radius: 4px;
    }

    &:focus + label:before {
      box-shadow: 0 0 0 3px rgba(0, 0, 0, 0.12);
    }

    &:checked + label:before {
      background-color: ${props => props.theme.colors.blue};
    }

    &:disabled + label {
      color: #b8b8b8;
      cursor: auto;
    }

    &:disabled + label:before {
      box-shadow: none;
      background: #ddd;
    }

    &:checked + label:after {
      content: "";
      position: absolute;
      left: 6px;
      top: 10px;
      background: white;
      width: 2px;
      height: 2px;
      box-shadow: 2px 0 0 white, 4px 0 0 white, 4px -2px 0 white,
        4px -4px 0 white, 4px -6px 0 white, 4px -8px 0 white;
      transform: rotate(45deg);
    }
  }

  .footerForm {
    display: flex;
    justify-content: flex-start;
    align-items: center;
    position: relative;
    width: 200px;
  }

  .tooltip-text {
    visibility: hidden;
    width: 120px;
    background-color: black;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 10px;
    position: absolute;
    z-index: 3;
    bottom: 150%;
    left: 50%;
    font-size: 12px;
    margin-left: -60px;
  }

  .footerForm .tooltip-text::after {
    content: "";
    position: absolute;
    top: 100%;
    left: 10%;
    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: black transparent transparent transparent;
  }

  .footerForm:hover .tooltip-text {
    visibility: visible;
  }

  @media (max-width: 1024px) {
    .titleSection {
      margin: 0;
    }

    .titleSection {
      display: flex;
      flex-direction: column;
      align-items: center;
      text-align: center;
    }

    .checkBox {
      background: white;
    }
  }

  @media (max-width: 580px) {
    .topSection {
      flex-direction: column;
    }

    .lowerForm {
      flex-direction: column;
    }

    .lowerForm div {
      width: 100%;
    }
  }

  @media (max-width: 480px) {
    .imageSection img {
      width: 7.5rem;
    }

    .titleSection h1 {
      font-size: 1.25rem;
    }

    .titleSection h3 {
      font-size: 0.75rem;
      margin-top: 0;
    }

    p {
      font-size: 0.8rem;
    }
  }
`;

import styled from "styled-components";

export const GalleryPageStyle = styled.div`
  @import url("https://fonts.googleapis.com/css?family=Rubik:400,500,700&display=swap");
`;

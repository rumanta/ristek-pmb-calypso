import React, { Component } from "react";
import { GalleryPageStyle } from "./style";
import GalleryCard from "../../components/GalleryCard";
import GalleryCarousel from "../../components/GalleryCarousel";
import Section from "../../components/Section";

class GalleryPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      carouselIsOpen: false,
      // indicates which album for carousel
      albumIndex: 0,
      // indicates which image when carousel open
      imageIndex: 0
    };

    this.openCarousel = this.openCarousel.bind(this);
    this.closeCarousel = this.closeCarousel.bind(this);
  }

  openCarousel(event) {
    this.setState({
      carouselIsOpen: true,
      albumIndex: parseInt(event.target.dataset.albumIndex, 10),
      imageIndex: parseInt(event.target.dataset.imageIndex, 10)
    });
  }

  closeCarousel() {
    this.setState({ carouselIsOpen: false });
  }

  render() {
    const carouselIsOpen = this.state.carouselIsOpen;

    // sample albums for carousel
    const imageUrlsArray = [
      [
        "https://media.dayoftheshirt.com/images/shirts/bEnHm/dbh_jellyspace_1467529895.full.png",
        "https://images.pexels.com/photos/355956/pexels-photo-355956.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260",
        "https://i.imgur.com/HMBCeFR.png",
        "https://i.imgur.com/IxAvpt8.png",
        "https://i.imgur.com/FlhxCjz.png"
      ],
      [
        "https://www.dike.lib.ia.us/images/sample-1.jpg/image",
        "https://4.img-dpreview.com/files/p/E~TS590x0~articles/3925134721/0266554465.jpeg",
        "https://www.easyhdr.com/examples/notredame/notredame.jpg",
        "http://apresentation.com/wp-content/uploads/2017/11/BLG_Andrew-G.-River-Sample_09.13.12.png",
        "https://imgsv.imaging.nikon.com/lineup/lens/f-mount/zoom/normalzoom/af-s_dx_18-140mmf_35-56g_ed_vr/img/sample/sample1_l.jpg"
      ]
    ];

    return (
      <GalleryPageStyle>
        <div className="Gallery">
          <Section title="Gallery">
            {imageUrlsArray.map((imageUrls, index) => (
              <GalleryCard
                key={index}
                albumIndex={index}
                imageArray={imageUrls}
                onClickImage={this.openCarousel}
              />
            ))}
          </Section>
          <GalleryCarousel
            isOpen={carouselIsOpen}
            onCloseCarousel={this.closeCarousel}
            imageUrls={imageUrlsArray[this.state.albumIndex]}
            imageIndex={this.state.imageIndex}
          />
        </div>
      </GalleryPageStyle>
    );
  }
}

export default GalleryPage;

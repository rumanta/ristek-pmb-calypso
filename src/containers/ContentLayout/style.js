import styled from "styled-components";

export const ContentLayoutStyle = styled.div`
  position: relative;
  z-index: 1;
  flex-grow: 1;
  display: flex;
  flex-direction: column;

  .ContentLayout {
    display: flex;
    flex-direction: column;
    align-items: center;
    background-color: #f7f8f9;
    flex-grow: 1;
  }

  .ContentLayout__container {
    display: flex;
    flex-direction: column;
    justify-content: center;
    box-sizing: border-box;
    width: 100%;
    padding: 0 0.5rem;
    padding-top: 1rem;
    flex-grow: 1;

    @media (max-width: 500px) {
      padding-bottom: 0;
      padding: 1rem 0;
    }

    @media (min-width: ${props => props.theme.sizes.desktop}) {
      max-width: 960px;
      padding-top: 0;
    }
    @media screen and (min-width: ${props => props.theme.sizes.widescreen}) {
      max-width: 1152px;
    }
    @media screen and (min-width: ${props => props.theme.sizes.fullhd}) {
      max-width: 1344px;
    }
  }

  .ContentLayout__row {
    display: flex;
    flex-direction: column;
    flex-grow: 1;

    @media (min-width: ${props => props.theme.sizes.desktop}) {
      flex-direction: row;
      display: flex;
      justify-content: center;
    }
  }

  .notification-card:last-child {
    border-style: none;
  }

  .see-more {
    font-size: 12px;
    text-decoration: none;
    color: ${props => props.theme.colors.blue};
  }

  .see-more:hover {
    text-decoration: underline;
  }

  .patch-notes-cont {
    padding: 0 1rem;
    margin: 0.8rem 0 0 0;

    li {
      font-size: 12px;
      margin-bottom: 12px;
    }
  }

  .patch-li {
    font-size: 12px;
  }

  .notification-card-home {
    display: flex;
    padding: 1rem 0.5rem;
    background: transparent;
    border-bottom: 1px solid ${props => props.theme.colors.primaryBlack};
  }

  .notification-card-home {
    p,
    a {
      font-size: 14px;
      color: ${props => props.theme.colors.primaryBlack};
    }

    a {
      text-decoration: none; /* no underline */
    }

    a:hover {
      text-decoration: underline;
    }
  }

  .message-cont {
    margin-left: 0.5rem;
  }

  .picture-cont {
    min-width: 32px;
    max-width: 32px;
    height: 32px;
    background-position: center;
    background-size: cover;
  }

  .picture-profile-cont {
    min-width: 32px;
    max-width: 32px;
    height: 32px;
    border-radius: 50%;
    background-position: center;
    background-size: cover;
    border-width: 1px;
    border-color: ${props => props.theme.colors.blue};
    border-style: solid;
  }

  @media (max-width: 1216px) {
    .notification-card {
      a {
        font-size: 13px;
      }
    }

    .picture-cont {
      min-width: 25px;
      max-width: 25px;
      height: 25px;
    }

    .picture-profile-cont {
      min-width: 25px;
      max-width: 25px;
      height: 25px;
    }
  }

  @media (max-width: 500px) {
    .notification-card {
      p,
      a {
        font-size: 12px;
      }
    }

    .picture-cont {
      min-width: 20px;
      max-width: 20px;
      height: 20px;
    }
  }

  .ContentLayout__row > * {
    display: flex;
    flex-direction: column;
    box-sizing: border-box;
    padding: 1rem;
    width: 100%;

    @media (max-width: ${props => props.theme.sizes.desktop}) {
      padding-top: 0;
      padding-bottom: 0;
      flex-grow: 1;
    }
  }

  @media (min-width: ${props => props.theme.sizes.desktop}) {
    .ContentLayout__col-2 {
      width: calc(2 / 12 * 100%);
    }
    .ContentLayout__col-3 {
      width: calc(3 / 12 * 100%);
    }
    .ContentLayout__col-7 {
      width: calc(7 / 12 * 100%);
    }
    .ContentLayout__col-9 {
      width: calc(9 / 12 * 100%);
    }
    .ContentLayout__col-10 {
      width: calc(10 / 12 * 100%);
    }
  }
`;

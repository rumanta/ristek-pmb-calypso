/* eslint-disable no-shadow */
/* eslint-disable no-unused-vars */
/* eslint-disable guard-for-in */
import React, { Component } from "react";
import { connect } from "react-redux";
import { isEqual } from "lodash";
import PropTypes from "prop-types";
import moment from "moment-timezone";
import Spinner from "components/Loading";
import PlaceholderPicture from "assets/placeholder_profile.png";
import Card from "components/Card";

import TokenGenerator from "containers/TokenGenerator";
import TokenInput from "containers/TokenInput";
import CalendarCard from "../../components/CalendarCard";
// import Card from "../../components/Card";
import DisplayModifier from "../../components/DisplayModifier";
import ServerTimeCard from "../../components/ServerTimeCard";
import StoryWidget from "containers/StoryWidget";
import Section from "../../components/Section";
import { isMaba } from "selectors/user";
// import StoryListCard from "../../components/StoryListCard";
import { ContentLayoutStyle } from "./style";
import { getTasks } from "selectors/taskPage";
import { getAllEvents } from "selectors/events";
import { fetchEvents } from "containers/EventListPage/actions";
import { fetchTasksList } from "containers/TaskPage/actions";
import { push } from "connected-react-router";

import { parseDataToMessageAndPicture } from "containers/NotificationPage/util";
import { fetchNotificationsList } from "containers/NotificationPage/actions";
import {
  getNotifications,
  isNotificationsLoading,
  isNotificationsLoaded
} from "selectors/notifications";

class ContentLayout extends Component {
  constructor(props) {
    super(props);
    this.layoutDict = {
      bypass: ["/about", "/login"],
      threeColumns: [
        "/",
        "/profile",
        "/events",
        "/announcements",
        "/tasks",
        "/friends",
        "/stories"
      ],
      // twoColumns: ["/gallery"],
      twoColumns: [],
      oneNarrowColumn: [],
      oneWideColumn: [
        "/editprofile",
        "/edit-interest",
        /\/events\/[0-9]+/g,
        /\/tasks\/[0-9]+/g,
        /\/announcements\/[0-9]+/g,
        /\/formfriend\/[0-9]+/g,
        // not found page
        /\/.*/g
      ]
    };
    this.getLayout = this.getLayout.bind(this);
    this.getEventDictionaryForCalendar = this.getEventDictionaryForCalendar.bind(
      this
    );
  }

  static defaultProps = {
    events: [],
    tasks: []
  };

  static layoutsWithCalendar = ["threeColumns", "twoColumns"];

  // eslint-disable-next-line consistent-return
  shouldComponentUpdate(prevProps) {
    if (
      !isEqual(this.props.events, prevProps.events) ||
      !isEqual(this.props.tasks, prevProps.tasks) ||
      !isEqual(this.props.notifications, prevProps.notifications) ||
      isEqual(this.props.notifications, prevProps.notifications)
    ) {
      return true;
    }

    if (
      prevProps.location.pathname !== undefined &&
      this.props.location.pathname !== undefined
    ) {
      return prevProps.location.pathname !== this.props.location.pathname;
    }
  }

  componentDidMount() {
    if (ContentLayout.layoutsWithCalendar.includes(this.getLayout())) {
      this.props.fetchEvents();
      this.props.fetchTasksList();
      this.props.fetchNotificationsList();
    }
  }

  getLayout() {
    // returns layout used as specified in layoutDict
    // returns bypass if not found
    /* eslint-disable array-callback-return */
    // eslint-disable-next-line no-unused-vars
    for (const key in this.layoutDict) {
      const urlPath = this.props.location.pathname;
      let res = null;

      this.layoutDict[key].map(path => {
        const match = urlPath.match(path);
        const isMatched = match && urlPath === match[0];

        if (path === urlPath || isMatched) {
          res = key;
        }
      });
      if (res) {
        return res;
      }
    }

    return "bypass";
  }

  /*
    builds dictionary to pass to CalendarCard
    key: unix timestamp seconds
    see https://momentjs.com/docs/#/parsing/unix-timestamp-milliseconds/
    value: dictionary containing event info
  */

  getEventDictionaryForCalendar = () => {
    const result = {};
    const tasks = this.props.tasks || [];
    const events = this.props.events || [];

    const getEventDetail = (item, dateStringGetter, linkPrefix) => ({
      title: item.title,
      date: dateStringGetter(item),
      link: `${linkPrefix}/${item.id}`
    });
    const getEventKey = dateString =>
      moment(dateString)
        .startOf("day")
        .unix();

    if ([tasks.length, events.length].includes(undefined)) {
      return result;
    }
    tasks.map(task => {
      const getDate = item => item.end_date;
      const key = getEventKey(getDate(task));
      const eventDetail = getEventDetail(task, getDate, "/tasks");
      // eslint-disable-next-line no-unused-expressions
      result[key] === undefined
        ? (result[key] = [eventDetail])
        : result[key].push(eventDetail);
    });
    events.map(event => {
      const getDate = item => item.date;
      const key = getEventKey(getDate(event));
      const eventDetail = getEventDetail(event, getDate, "/events");
      // eslint-disable-next-line no-unused-expressions
      result[key] === undefined
        ? (result[key] = [eventDetail])
        : result[key].push(eventDetail);
    });

    return result;
  };

  renderNotificationCards = () => {
    const {
      isNotificationsLoaded,
      isNotificationsLoading,
      notifications,
      isMaba
    } = this.props;
    const N_LIMIT = 4;

    if (isNotificationsLoading && !isNotificationsLoaded) {
      return <Spinner />;
    }

    return notifications
      .filter(item => item.related_object)
      .slice(0, N_LIMIT)
      .map(data => {
        const { photoUrl, isFriend, message } = parseDataToMessageAndPicture(
          data,
          isMaba
        );

        return (
          <div
            className="notification-card-home"
            key={`notification-card-${data.id}`}
          >
            <div
              className={isFriend ? "picture-profile-cont" : "picture-cont"}
              style={{
                backgroundImage: `url(${
                  photoUrl ? photoUrl : PlaceholderPicture
                })`
              }}
            />
            <div className="message-cont">{message}</div>
          </div>
        );
      });
  };

  render() {
    // const stories = this.state.stories;
    const layoutUsed = this.getLayout();
    // only fetch events on pages with calendar
    const calendarEvents = ContentLayout.layoutsWithCalendar.includes(
      layoutUsed
    )
      ? this.getEventDictionaryForCalendar()
      : {};

    return layoutUsed === "bypass" ? (
      <>{this.props.children}</>
    ) : (
      <ContentLayoutStyle>
        <div className="ContentLayout">
          <div className="ContentLayout__container">
            <div className="ContentLayout__row">
              {/* hide notifications */}
              {layoutUsed === "threeColumns" && (
                <div className="ContentLayout__col-2">
                  <DisplayModifier showOn="desktop">
                    <Section title="notifications">
                      {this.renderNotificationCards()}
                    </Section>
                    <a className="see-more" href="notifications">
                      See More...
                    </a>
                  </DisplayModifier>
                </div>
              )}
              <div
                className={
                  (["threeColumns", "oneNarrowColumn"].includes(layoutUsed) &&
                    "ContentLayout__col-7") ||
                  (layoutUsed === "twoColumns" && "ContentLayout__col-9") ||
                  (layoutUsed === "oneWideColumn" && "ContentLayout__col-10")
                }
              >
                {this.props.children}
              </div>
              {["threeColumns", "twoColumns"].includes(layoutUsed) && (
                <div className="ContentLayout__col-3">
                  <DisplayModifier showOn="desktop">
                    {/* hide token and section title */}
                    <Section title="&nbsp;">
                      {this.props.isMaba ? <TokenInput /> : <TokenGenerator />}
                      <ServerTimeCard />
                      <CalendarCard events={calendarEvents} />
                      <Section title="Featured Stories">
                        <StoryWidget push={push} />
                      </Section>
                    </Section>
                  </DisplayModifier>
                </div>
              )}
            </div>
          </div>
        </div>
      </ContentLayoutStyle>
    );
  }
}

ContentLayout.propTypes = {
  children: PropTypes.node.isRequired,
  location: PropTypes.shape({
    pathname: PropTypes.string
  }),
  isMaba: PropTypes.bool.isRequired,
  fetchEvents: PropTypes.func.isRequired,
  fetchTasksList: PropTypes.func.isRequired,
  events: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  tasks: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  fetchNotificationsList: PropTypes.func,
  notifications: PropTypes.arrayOf(PropTypes.object),
  isNotificationsLoading: PropTypes.bool,
  isNotificationsLoaded: PropTypes.bool
};

const mapStateToProps = state => ({
  isMaba: isMaba(state),
  events: getAllEvents(state),
  tasks: getTasks(state),
  notifications: getNotifications(state),
  isNotificationsLoading: isNotificationsLoading(state),
  isNotificationsLoaded: isNotificationsLoaded(state)
});

function mapDispatchToProps(dispatch) {
  return {
    fetchEvents: () => dispatch(fetchEvents()),
    fetchTasksList: () => dispatch(fetchTasksList()),
    fetchNotificationsList: () => dispatch(fetchNotificationsList())
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ContentLayout);

import axios from "axios";

import {
  FETCH_STORIES,
  FETCH_STORIES_SUCCESS,
  FETCH_STORIES_FAILED,
  DELETE_STORY,
  DELETE_STORY_SUCCESS,
  DELETE_STORY_FAILED,
  RESET_STATE
} from "./constants";

import { getMyStories, deleteStoryApi } from "api";
import swal from "sweetalert";

export function resetState() {
  return dispatch =>
    dispatch({
      type: RESET_STATE
    });
}

export function fetchMyStories() {
  return dispatch => {
    dispatch({
      type: FETCH_STORIES
    });
    axios
      .get(getMyStories)
      .then(response => {
        dispatch({
          payload: response.data,
          type: FETCH_STORIES_SUCCESS
        });
      })
      .catch(error => {
        dispatch({
          payload: error,
          type: FETCH_STORIES_FAILED
        });
      });
  };
}

export function deleteMyStory(storyId) {
  return dispatch => {
    dispatch({
      type: DELETE_STORY
    });
    axios
      .delete(deleteStoryApi(storyId))
      .then(response => {
        dispatch({
          payload: response.data,
          type: DELETE_STORY_SUCCESS
        });
        swal("Successful", "Your story has been deleted!", "success");
      })
      .catch(error => {
        dispatch({
          payload: error,
          type: DELETE_STORY_FAILED
        });
        swal(
          "Error",
          error.message === "Request failed with status code 406"
            ? `Failed to edit your story, URL is not a valid Medium Post URL`
            : error.message,
          "error"
        );
      });
  };
}

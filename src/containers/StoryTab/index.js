import React from "react";
import { StoryTabContainer } from "./style";
import { connect } from "react-redux";
import StoryProfileTabCard from "components/StoryProfileTabCard";
import EmptyCard from "components/EmptyCard";
import PropTypes from "prop-types";
import { fetchMyStories, deleteMyStory, resetState } from "./actions";
import Loading from "components/Loading";
import Buttons from "components/Buttons";

class StoryTab extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      cardsToShow: 2,
      expanded: false,
      stories: [],
      id: null
    };
    this.loadMore = this.loadMore.bind(this);
  }

  loadMore(itemLength) {
    if (this.state.cardsToShow === 2) {
      this.setState({
        cardsToShow: itemLength,
        expanded: true
      });
    } else {
      this.setState({
        cardsToShow: 2,
        expanded: false
      });
    }
  }

  componentDidMount() {
    this.props.fetchMyStories();
  }

  componentDidUpdate() {
    if (this.props.isDeleteSuccess) {
      this.props.resetState();
      this.props.fetchMyStories();
    }
  }

  splitObjects(object) {
    return [
      {
        id: object.id,
        title: object.medium.title,
        subtitle: object.medium.subtitle,
        thumbnail: `https://cdn-images-1.medium.com/max/1024/${
          object.medium.thumbnail.metadata.id
        }`,
        link: object.link,
        date: object.created_at
      },
      {
        profileImage: object.user.user_detail.profil.foto,
        name: object.user.user_detail.profil.nama_lengkap
      }
    ];
  }

  produceGeneralCards = objects =>
    objects.map((object, key) => {
      const splitObject = this.splitObjects(object);

      return (
        <StoryProfileTabCard
          key={key}
          entry={splitObject[0]}
          profile={splitObject[1]}
          isModifiable={true}
          push={this.props.push}
          delete={this.props.deleteMyStory}
        />
      );
    });

  render() {
    const stories = this.props.stories;
    const { isLoading } = this.props;

    return isLoading ? (
      <Loading />
    ) : (
      <StoryTabContainer>
        <div className="addSection">
          <h1>My Stories</h1>
          <Buttons
            name="Add Story"
            onPressed={() => this.props.push("/new-story")}
            wide="6rem"
            type="button"
          />
        </div>
        {stories.length !== 0 ? (
          this.produceGeneralCards(stories.slice(0, this.state.cardsToShow))
        ) : (
          <EmptyCard text="You have no stories posted. Write one :D" />
        )}
        {stories.length > 2 && !this.state.expanded && (
          <div className="loadMoreSection">
            <p onClick={() => this.loadMore(stories.length)}>Load more...</p>
          </div>
        )}
      </StoryTabContainer>
    );
  }
}

function mapStateToProps(state) {
  return {
    isLoading: state.storyTabReducer.get("isLoading"),
    stories: state.storyTabReducer.get("stories"),
    isDeleteSuccess: state.storyTabReducer.get("isDeleteSuccess")
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchMyStories: () => dispatch(fetchMyStories()),
    deleteMyStory: storyid => dispatch(deleteMyStory(storyid)),
    resetState: () => dispatch(resetState())
  };
}

StoryTab.propTypes = {
  push: PropTypes.func,
  fetchMyStories: PropTypes.func.isRequired,
  stories: PropTypes.array,
  isLoading: PropTypes.bool,
  deleteMyStory: PropTypes.func.isRequired,
  isDeleteSuccess: PropTypes.bool,
  resetState: PropTypes.func.isRequired
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StoryTab);

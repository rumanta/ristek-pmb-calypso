import styled from "styled-components";

export const StoryTabContainer = styled.div`
  .addSection {
    display: flex;
    justify-content: space-between;
    align-items: center;

    margin: 1.2rem 0;

    h1 {
      margin: 0;
    }
  }

  .addSection img {
    width: 3rem;
    cursor: pointer;
  }

  .loadMoreSection {
    display: flex;
    justify-content: center;
    margin: 2rem 0;
  }

  .loadMoreSection p {
    color: ${props => props.theme.colors.darkBlue};
    cursor: pointer;
  }

  @media (max-width: 480px) {
    .loadMoreSection p {
      font-size: 15px;
    }
  }
`;

import React from "react";
import { StoryPageContainer } from "./style";
import { connect } from "react-redux";
import StoryProfileTabCard from "components/StoryProfileTabCard";
import EmptyCard from "components/EmptyCard";
import PropTypes from "prop-types";
import { fetchRecentStories } from "./actions";
import Loading from "components/Loading";

class StoryPage extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      cardsToShow: 2,
      expanded: false,
      stories: []
    };
    this.loadMore = this.loadMore.bind(this);
  }

  loadMore(itemLength) {
    if (this.state.cardsToShow === 2) {
      this.setState({
        cardsToShow: itemLength,
        expanded: true
      });
    } else {
      this.setState({
        cardsToShow: 2,
        expanded: false
      });
    }
  }

  componentDidMount() {
    this.props.fetchRecentStories();
  }

  splitObjects(object) {
    return [
      {
        // Mandatory
        id: object.id,
        // Mandatory
        title: object.medium.title,
        // Mandatory
        subtitle: object.medium.subtitle,
        // Not Mandatory
        thumbnail: object.medium.thumbnail
          ? `https://cdn-images-1.medium.com/max/1024/${
              object.medium.thumbnail.metadata.id
            }`
          : null,
        link: object.link,
        date: object.created_at
      },
      {
        profileImage: object.user.user_detail.profil.foto,
        name: object.user.user_detail.profil.nama_lengkap
      }
    ];
  }

  produceGeneralCards = objects =>
    objects.map((object, key) => {
      const splitObject = this.splitObjects(object);

      return (
        <StoryProfileTabCard
          key={key}
          entry={splitObject[0]}
          profile={splitObject[1]}
          isModifiable={false}
        />
      );
    });

  render() {
    const stories = this.props.stories.filter(
      object => object.medium.title || object.medium.subtitle
    );
    const { isLoading } = this.props;

    return isLoading ? (
      <Loading />
    ) : (
      <StoryPageContainer>
        <h1>Stories</h1>
        {stories.length !== 0 ? (
          this.produceGeneralCards(stories.slice(0, this.state.cardsToShow))
        ) : (
          <EmptyCard text="There are no stories yet! Come back later :D" />
        )}
        {stories.length > 2 && !this.state.expanded && (
          <div className="loadMoreSection">
            <p onClick={() => this.loadMore(stories.length)}>Load more...</p>
          </div>
        )}
      </StoryPageContainer>
    );
  }
}

function mapStateToProps(state) {
  return {
    isLoading: state.storyPageReducer.get("isLoading"),
    stories: state.storyPageReducer.get("stories")
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchRecentStories: () => dispatch(fetchRecentStories())
  };
}

StoryPage.propTypes = {
  push: PropTypes.func,
  fetchRecentStories: PropTypes.func.isRequired,
  stories: PropTypes.array,
  isLoading: PropTypes.bool
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StoryPage);

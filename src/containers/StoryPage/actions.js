import axios from "axios";

import {
  FETCH_STORIES,
  FETCH_STORIES_SUCCESS,
  FETCH_STORIES_FAILED
} from "./constants";

import { getRecentStories } from "api";

export function fetchRecentStories() {
  return dispatch => {
    dispatch({
      type: FETCH_STORIES
    });
    axios
      .get(getRecentStories)
      .then(response => {
        dispatch({
          payload: response.data,
          type: FETCH_STORIES_SUCCESS
        });
      })
      .catch(error => {
        dispatch({
          payload: error,
          type: FETCH_STORIES_FAILED
        });
      });
  };
}

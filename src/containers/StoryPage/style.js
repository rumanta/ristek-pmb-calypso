import styled from "styled-components";

export const StoryPageContainer = styled.div`
  @import url("https://fonts.googleapis.com/css?family=Rubik:400,500,700&display=swap");
  padding: 0.25rem;

  .loadMoreSection {
    display: flex;
    justify-content: center;
    margin: 2rem 0;
  }

  .loadMoreSection p {
    color: ${props => props.theme.colors.darkBlue};
    cursor: pointer;
  }

  @media (max-width: 480px) {
    .loadMoreSection p {
      font-size: 15px;
    }
  }
`;

import { fromJS } from "immutable";
import {
  FETCH_STORIES,
  FETCH_STORIES_SUCCESS,
  FETCH_STORIES_FAILED
} from "./constants";

const initialState = fromJS({
  error: null,
  isLoading: false,
  stories: []
});

function storyPageReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_STORIES:
      return state.set("isLoading", true);
    case FETCH_STORIES_SUCCESS:
      return state.set("stories", action.payload).set("isLoading", false);
    case FETCH_STORIES_FAILED:
      return state.set("error", action.payload).set("isLoading", false);
    default:
      return state;
  }
}

export default storyPageReducer;

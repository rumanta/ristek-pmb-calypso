export const FETCH_STORIES = "src/StoryPage/FETCH_STORIES";

export const FETCH_STORIES_SUCCESS = "src/StoryPage/FETCH_STORIES_SUCCESS";

export const FETCH_STORIES_FAILED = "src/StoryPage/FETCH_RECENT_FAILED";

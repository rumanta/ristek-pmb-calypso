import axios from "axios";

import {
  FETCH_PROFILE,
  FETCH_PROFILE_SUCCESS,
  FETCH_PROFILE_FAILED,
  FETCH_INTEREST,
  FETCH_INTEREST_SUCCESS,
  FETCH_INTEREST_FAILED
} from "./constants";

import { fetchProfileApi, getUserInterestApi } from "api";

export function fetchProfile() {
  return dispatch => {
    dispatch({
      type: FETCH_PROFILE
    });
    axios
      .get(fetchProfileApi)
      .then(response => {
        dispatch({
          payload: response.data,
          type: FETCH_PROFILE_SUCCESS
        });
      })
      .catch(error => {
        dispatch({
          payload: error,
          type: FETCH_PROFILE_FAILED
        });
      });
  };
}

export function fetchInterest(id) {
  return dispatch => {
    dispatch({
      type: FETCH_INTEREST
    });
    axios
      .get(getUserInterestApi(id))
      .then(response => {
        dispatch({
          payload: response.data,
          type: FETCH_INTEREST_SUCCESS
        });
      })
      .catch(error => {
        dispatch({
          payload: error,
          type: FETCH_INTEREST_FAILED
        });
      });
  };
}

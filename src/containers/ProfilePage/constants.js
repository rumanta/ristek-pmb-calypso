export const FETCH_PROFILE = "src/ProfilePage/FETCH_PROFILE";

export const FETCH_PROFILE_SUCCESS = "src/ProfilePage/FETCH_PROFILE_SUCCESS";

export const FETCH_PROFILE_FAILED = "src/ProfilePage/FETCH_PROFILE_FAILED";

export const UPDATE_PROFILE = "src/ProfilePage/UPDATE_PROFILE";

export const FETCH_INTEREST = "src/ProfilePage/FETCH_INTEREST";

export const FETCH_INTEREST_SUCCESS = "src/ProfilePage/FETCH_INTEREST_SUCCESS";

export const FETCH_INTEREST_FAILED = "src/ProfilePage/FETCH_INTEREST_FAILED";

import styled from "styled-components";

export const ProfilePageContainer = styled.div`
  @import url("https://fonts.googleapis.com/css?family=Rubik:400,500,700&display=swap");
  padding: 0.25rem;
/*
  .nav-tabs {
    border-bottom: 2px solid ${props => props.theme.colors.darkBlue};
    margin-bottom: 1.5rem;
  }

  .nav-tabs .nav-link {
    border: 0px;
    border-top-left-radius: 0rem;
    border-top-right-radius: 0rem;
    border-right: 1px solid ${props => props.theme.colors.darkBlue};
    width: 10rem;
    justify-content: center;
  }

  .tab-content {
    padding: 1.5rem;
  }

  a {
    color: ${props => props.theme.colors.darkBlue};
    font-family: Rubik;
    font-style: normal;
    font-weight: ${props => props.theme.weight.bold};
    font-size: 1.75vh;
    text-align: center;
  }

  .nav-tabs .nav-item.show .nav-link,
  .nav-tabs .nav-link.active {
    color: ${props => props.theme.colors.white};
    background-color: ${props => props.theme.colors.darkBlue};
  }
*/
  .event-tab-container {
    display: flex;
    flex-direction: row;
    margin-bottom: 1rem;
    border-bottom: 2px solid ${props => props.theme.colors.darkBlue};

    .tab {
      padding: 0.4rem 1.5rem;
      color: ${props => props.theme.colors.darkBlue};
      font-weight: ${props => props.theme.weight.bold};
      cursor: pointer;
    }

    .tab:hover {
      background: ${props => props.theme.colors.darkBlue};
      color: ${props => props.theme.colors.white};
    }

    .active {
      color: ${props => props.theme.colors.white};
      background-color: ${props => props.theme.colors.darkBlue};
    }
  }

  @media (max-width: 1024px) {
    .tab {
      width: 33.33%;
      padding: 0.2rem;
    }
  }



`;

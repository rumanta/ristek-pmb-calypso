export const FETCH_TASK = "src/TaskDetailPage/FETCH_TASK";

export const FETCH_TASK_SUCCESS = "src/TaskDetailPage/FETCH_TASK_SUCCESS";

export const FETCH_TASK_FAILED = "src/TaskDetailPage/FETCH_TASK_FAILED";

export const FETCH_TASK_SUBMISSIONS =
  "src/TaskDetailPage/FETCH_TASK_SUBMISSIONS";

export const FETCH_TASK_SUBMISSIONS_SUCCESS =
  "src/TaskDetailPage/FETCH_TASK_SUBMISSIONS_SUCCESS";

export const FETCH_TASK_SUBMISSIONS_FAILED =
  "src/TaskDetailPage/FETCH_TASK_SUBMISSIONS_FAILED";

export const POST_SUBMISSION = "src/TaskDetailPage/POST_SUBMISSION";

export const POST_SUBMISSION_SUCCESS =
  "src/TaskDetailPage/POST_SUBMISSION_SUCCESS";

export const POST_SUBMISSION_FAILED =
  "src/TaskDetailPage/POST_SUBMISSION_FAILED";

export const POST_QNA_COMMENT = "src/TaskDetailPage/POST_QNA_COMMENT";
export const POST_QNA_COMMENT_SUCCESS =
  "src/TaskDetailPage/POST_QNA_COMMENT_SUCCESS";
export const POST_QNA_COMMENT_FAILED =
  "src/TaskDetailPage/POST_QNA_COMMENT_FAILED";

export const CLEAR_TASK_DETAIL = "src/TaskDetailPage/CLEAR_TASK_DETAIL";

import axios from "axios";
import swal from "sweetalert";
import {
  FETCH_TASK,
  FETCH_TASK_SUCCESS,
  FETCH_TASK_FAILED,
  FETCH_TASK_SUBMISSIONS,
  FETCH_TASK_SUBMISSIONS_SUCCESS,
  FETCH_TASK_SUBMISSIONS_FAILED,
  POST_SUBMISSION,
  POST_SUBMISSION_FAILED,
  POST_SUBMISSION_SUCCESS,
  POST_QNA_COMMENT,
  POST_QNA_COMMENT_SUCCESS,
  POST_QNA_COMMENT_FAILED,
  CLEAR_TASK_DETAIL
} from "./constants";

import { getTaskApi, taskSubmissionsApi, uploadApi, postCommentApi } from "api";

export function fetchTask(id) {
  return dispatch => {
    dispatch({ type: FETCH_TASK });
    axios
      .get(getTaskApi(id))
      .then(response => {
        dispatch({
          payload: response.data,
          type: FETCH_TASK_SUCCESS
        });
      })
      .catch(error => {
        dispatch({
          payload: error,
          type: FETCH_TASK_FAILED
        });
      });
  };
}

export function fetchTaskSubmissions(id) {
  return dispatch => {
    dispatch({ type: FETCH_TASK_SUBMISSIONS });
    axios
      .get(taskSubmissionsApi(id))
      .then(response => {
        dispatch({
          payload: response.data,
          type: FETCH_TASK_SUBMISSIONS_SUCCESS
        });
      })
      .catch(error => {
        dispatch({
          payload: error,
          type: FETCH_TASK_SUBMISSIONS_FAILED
        });
      });
  };
}

export function postTaskSubmissions(submission) {
  const { file, taskId, time } = submission;
  const formData = new FormData();
  formData.append("file", file);

  return dispatch => {
    dispatch({ type: POST_SUBMISSION });
    axios
      .post(uploadApi, formData, {
        headers: {
          "Content-Type": "multipart/form-data"
        }
      })
      .then(responseUrl => {
        const payload = {
          file_link: responseUrl.data.file,
          submit_time: time
        };

        axios
          .post(taskSubmissionsApi(taskId), payload, {
            headers: {
              "Content-Type": "application/json"
            }
          })
          .then(responseSubmission => {
            dispatch({
              payload: responseSubmission.data,
              type: POST_SUBMISSION_SUCCESS
            });
            swal("Submission Success", "", "success");
          })
          .catch(error => {
            dispatch({
              payload: error,
              type: POST_SUBMISSION_FAILED
            });
          });
        swal("Submission Failed", "Please try again later", "error");
      })
      .catch(error => {
        dispatch({
          payload: error,
          type: POST_SUBMISSION_FAILED
        });
        swal(
          "Submission Failed",
          "Failed to upload to server, Please try again later",
          "error"
        );
      });
  };
}

export function postTaskComment(comment) {
  return dispatch => {
    dispatch({ type: POST_QNA_COMMENT });
    axios
      .post(postCommentApi, comment)
      .then(() => {
        dispatch({ type: POST_QNA_COMMENT_SUCCESS });
        dispatch(fetchTask(comment.post_id));
        swal("Comment has been added", "", "success");
      })
      .catch(error => {
        dispatch({
          payload: error,
          type: POST_QNA_COMMENT_FAILED
        });
        swal(
          "Error",
          "Failed to add a comment. Please try again later",
          "error"
        );
      });
  };
}

export function clearTask() {
  return dispatch => {
    dispatch({ type: CLEAR_TASK_DETAIL });
  };
}

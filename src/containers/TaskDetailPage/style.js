import styled from "styled-components";

export const TaskDetailPageContainer = styled.div`
  padding-top: 2em;
  background-color: ${props => props.theme.boneWhite}
  display: flex;
  width: 100%;
  margin: auto;

  .Card {
    padding: 1rem;
  }

  .button-wrap {
    display: flex;
    justify-content: flex-end;
  }

  .dropzone-header {
    line-height: 1.5;
    overflow-wrap: break-word;
  }

  .dropzone {
    margin: 1rem 0;
    min-height: 100px;
    background: ${props => props.theme.colors.lightGray};
    border-radius: 5px;
    border: dashed 2px ${props => props.theme.colors.darkGray};
    display: flex;
    text-align: center;
    align-items: center;
    cursor: pointer;
    color: ${props => props.theme.colors.darkGray};

    .dropzone-content{
      width: 100%;
      font-size: 14px;
    }

    .file-display-wrap {
      z-index: 500;
      margin: 1rem;
      width: 100px;
      height: 100px;
      overflow: hidden;

      .file-name {
        overflow-wrap: break-word;
      }
    }
  }

  .files-dropzone-active {
    border: dashed 2px ${props => props.theme.colors.blue};
    color: ${props => props.theme.colors.blue};
  }

  .green {
    color: ${props => props.theme.colors.green};
  }

  .red {
    color: ${props => props.theme.colors.red};
  }
`;

import { fromJS } from "immutable";
import {
  FETCH_TASK,
  FETCH_TASK_SUCCESS,
  FETCH_TASK_FAILED,
  FETCH_TASK_SUBMISSIONS,
  FETCH_TASK_SUBMISSIONS_SUCCESS,
  FETCH_TASK_SUBMISSIONS_FAILED,
  POST_SUBMISSION,
  POST_SUBMISSION_FAILED,
  POST_SUBMISSION_SUCCESS,
  CLEAR_TASK_DETAIL
} from "./constants";

const initialState = fromJS({
  error: null,
  isTaskLoaded: false,
  isTaskLoading: false,
  task: null,
  isSubmissionLoaded: false,
  isSubmissionLoading: false,
  submissions: [],
  submissionResponse: null
});

function taskDetailPageReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_TASK:
      return state.set("isTaskLoading", true);
    case FETCH_TASK_SUCCESS:
      return state
        .set("task", action.payload)
        .set("isTaskLoading", false)
        .set("isTaskLoaded", true);
    case FETCH_TASK_FAILED:
      return state
        .set("error", action.payload)
        .set("isTaskLoaded", false)
        .set("isTaskLoading", false);
    case FETCH_TASK_SUBMISSIONS:
      return state.set("isTaskLoading", true);
    case FETCH_TASK_SUBMISSIONS_SUCCESS:
      return state
        .set("submissions", action.payload)
        .set("isTaskLoading", false)
        .set("isTaskLoaded", true);
    case FETCH_TASK_SUBMISSIONS_FAILED:
      return state
        .set("error", action.payload)
        .set("isTaskLoaded", false)
        .set("isTaskLoading", false);
    case POST_SUBMISSION:
      return state.set("isSubmissionLoading", true);
    case POST_SUBMISSION_SUCCESS:
      return state
        .set("submissionResponse", action.payload)
        .set("isSubmissionLoading", false)
        .set("isSubmissionLoaded", true);
    case POST_SUBMISSION_FAILED:
      return state
        .set("error", action.payload)
        .set("isSubmissionLoaded", false)
        .set("isSubmissionLoading", false);
    case CLEAR_TASK_DETAIL:
      return state.set("error", null).set("isSubmissionLoaded", false);

    default:
      return state;
  }
}

export default taskDetailPageReducer;

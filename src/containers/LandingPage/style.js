import styled from "styled-components";
import desktop from "./assets/desktopLandingPage.png";
import mobile from "./assets/mobileLandingPage.png";

export const LandingPageElement = styled.div`
  width: 100%;
  height: 100%;
  font-family: "Rubik";

  /* media for Iphone 5  or smaller phones*/
  .container {
    width: 100%;
    height: 100%;
    min-height: 100vh;
    position: fixed;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    align-content: center;
  }

  .logoContainer {
    width: auto;
    height: 2rem;
    top: 1rem;
    left: 0;
    position: absolute;
    padding: 1rem 2rem;

    img {
      max-height: 35px;
      padding-right: 1rem;
    }
  }

  .menu {
    height: 100%;
    width: 100vw;
    top: 27vw;
    left: 1.5rem;
    position: relative;

    h1 {
      margin: 0;
      font-style: normal;
      font-weight: ${props => props.theme.weight.regular};
      font-size: 1rem;
      text-align: left;
    }

    .ssoButton {
      border: 2px solid ${props => props.theme.colors.lightBlue};
      border-radius: 10px;
      width: 9rem;
      padding: 0.25rem 0.1rem;
      position: relative;
      display: block;
      background: none;
      overflow: hidden;
      font-size: 14px;
      font-weight: ${props => props.theme.weight.regular};
      cursor: pointer;
      transition: all 0.3s;

      &:hover {
        color: white;
      }

      &::before,
      &::after {
        content: "";
        position: absolute;
        background: ${props => props.theme.colors.lightBlue};
        z-index: -1;
      }

      &::after {
        width: 0;
        height: 100%;
        left: -35%;
        top: 0;
        transform: skew(60deg);
        transition-duration: 0.5s;
        transform-origin: top left;
        transition: all 0.25s;
      }

      &:hover:after {
        width: 150%;
        height: 100%;
      }
    }
  }

  .ssoButton {
    margin: 1.5rem 0 0 0;
  }

  .homeImage {
    width: 100%;
    height: 100%;
    display: block;
    background: #f7f8f9;
    background-image: url(${mobile});
    background-repeat: no-repeat;
    background-position: 100% 100%;
    background-size: 100%;
    z-index: -9999;
    position: absolute;
  }

  /* Media for average smartphones*/
  @media (min-height: 600px) {
    .menu {
      h1 {
        font-size: 1.5rem;
      }

      .ssoButton {
        width: 11rem;
        padding: 0.5rem 0.3rem;
        font-size: 16px;
      }
    }
  }

  /* Media for smaller screen laptops (touchscreen etc) */
  @media (min-width: 64em) {
    .container {
      flex-direction: row;
      justify-content: flex-start;
      overflow: hidden;
    }

    .logoContainer {
      img {
        max-height: 50px;
      }
    }

    .buttonContainer {
      display: flex;
      justify-content: flex-end;
      flex-direction: column;
      align-items: flex-end;
    }

    .menu {
      height: 100%;
      margin: 0;
      top: 18vh;
      position: relative;
      margin-right: 10rem;

      h1 {
        font-size: 4rem;
        text-align: right;
        font-weight: ${props => props.theme.weight.regular};
      }

      .ssoButton {
        padding: 1rem 0.5rem;
        font-style: normal;
        font-weight: ${props => props.theme.weight.regular};
        font-size: 18px;
        border-radius: 0.8rem;
        width: 15rem;
      }

      .ssoButton {
        margin-top: 2rem;
      }
    }

    .homeImage {
      background-image: url(${desktop});
      z-index: -9999;
      position: absolute;
    }
  }

  /* Media for above 1600 in width */
  @media (min-width: 1601px) and (min-height: 700px) {
    .container {
      flex-direction: row;
      justify-content: flex-start;
      overflow: hidden;
    }

    .logoContainer {
      img {
        max-height: 72px;
      }
    }

    .menu {
      height: 100%;
      top: 5vw;

      h1 {
        font-size: 6rem;
        font-weight: ${props => props.theme.weight.regular};
      }

      .ssoButton {
        width: 15rem;
        padding: 1rem 0.5rem;
        margin-left: 5.75rem;
        font-weight: ${props => props.theme.weight.regular};
        font-size: 18px;
      }

      .ssoButton {
        margin-top: 3.3rem;
      }
    }

    .homeImage {
      background-image: url(${desktop});
    }
  }
`;

import React from "react";
import RistekLogo from "./assets/ristekLogo.png";
import PmbLogo from "./assets/pmbLogo.png";
import { LandingPageElement } from "./style";
import PropTypes from "prop-types";

import { connect } from "react-redux";
import { push } from "connected-react-router";

import { login } from "globalActions";

import { loggingInApi } from "api";
import { isLoggedIn } from "selectors/user";

export class LandingPage extends React.Component {
  componentDidMount() {
    window.addEventListener("message", this.receiveLoginData, false);
    if (this.props.loggedIn) {
      this.props.push("/");
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.loggedIn && !this.props.loggedIn) {
      this.props.push("/login");
    }
  }

  handleLogin = () => {
    const loginWindow = window.open(
      loggingInApi,
      "_blank",
      "width=800,height=800",
      "toolbar=0,location=0,menubar=0"
    );

    const getUserDataInterval = setInterval(() => {
      if (loginWindow.closed) {
        clearInterval(getUserDataInterval);
      }
      loginWindow.postMessage("MadeByWebdev2019", loggingInApi);
    }, 1000);
  };

  receiveLoginData = event => {
    const origin = event.origin || event.originalEvent.origin;
    const user = event.data;

    if (loggingInApi.startsWith(origin)) {
      this.props.login(user);
    }
  };

  render() {
    return (
      <LandingPageElement>
        <div className="container">
          <div className="logoContainer">
            <img src={RistekLogo} alt="ristek" />
            <img src={PmbLogo} alt="pmb" />
          </div>
          <div className="menu">
            <h1>Melangkah Bersama,</h1>
            <h1>Jelajahi Dunia</h1>
            <div className="buttonContainer">
              <button onClick={() => this.handleLogin()} className="ssoButton">
                Log In
              </button>
            </div>
          </div>
          <div className="homeImage" />
        </div>
      </LandingPageElement>
    );
  }
}

LandingPage.propTypes = {
  push: PropTypes.func.isRequired,
  login: PropTypes.func.isRequired,
  loggedIn: PropTypes.bool
};

function mapStateToProps(state) {
  return {
    loggedIn: isLoggedIn(state)
  };
}

function mapDispatchToProps(dispatch) {
  return {
    push: url => dispatch(push(url)),
    login: user => dispatch(login(user))
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LandingPage);

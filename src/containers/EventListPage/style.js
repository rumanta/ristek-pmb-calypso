import styled from "styled-components";

export const EventPageContainer = styled.div`
  width: 100%;

  .event-tab-container {
    display: flex;
    flex-direction: row;
    margin-bottom: 1rem;
    border-bottom: 2px solid ${props => props.theme.colors.darkBlue};

    .tab {
      padding: 0.4rem 2.5rem;
      color: ${props => props.theme.colors.darkBlue};
      font-weight: ${props => props.theme.weight.bold};
      cursor: pointer;
    }

    .tab:hover {
      background: ${props => props.theme.colors.darkBlue};
      color: ${props => props.theme.colors.white};
    }

    .active {
      color: ${props => props.theme.colors.white};
      background-color: ${props => props.theme.colors.darkBlue};
    }
  }

  @media (max-width: ${props => props.theme.sizes.desktop}) {
    .tab {
      width: 50%;
    }
  }
`;

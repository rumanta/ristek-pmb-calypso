import axios from "axios";
import {
  FETCH_EVENT_LIST,
  FETCH_EVENT_LIST_SUCCESS,
  FETCH_EVENT_LIST_FAILED
} from "./constants";

import { fetchEventsApi } from "api";

export function fetchEvents() {
  return dispatch => {
    dispatch({ type: FETCH_EVENT_LIST });
    axios
      .get(fetchEventsApi)
      .then(response => {
        dispatch({
          payload: response.data,
          type: FETCH_EVENT_LIST_SUCCESS
        });
      })
      .catch(error => {
        dispatch({
          payload: error,
          type: FETCH_EVENT_LIST_FAILED
        });
      });
  };
}

import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import { EventPageContainer } from "./style";
import InternalEventSection from "components/InternalEventSection";
import ExternalEventSection from "components/ExternalEventSection";

import { fetchEvents } from "./actions";
import {
  getInternalEvents,
  getExternalEvents,
  isEventsLoaded,
  isEventsLoading
} from "selectors/events";

class EventListPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isInternal: true
    };

    this.handleClick = this.handleClick.bind(this);
  }

  componentDidMount() {
    this.props.fetchEvents();
  }

  handleClick(toggledButton) {
    switch (toggledButton.toLowerCase()) {
      case "internal":
        this.setState({ isInternal: true });
        break;
      case "external":
        this.setState({ isInternal: false });
        break;
      default:
        break;
    }
  }

  render() {
    const { isInternal } = this.state;

    const renderEventContent = isInternal ? (
      <InternalEventSection
        events={this.props.internalEvents}
        isLoading={this.props.isLoadingEvents}
      />
    ) : (
      <ExternalEventSection
        events={this.props.externalEvents}
        isLoading={this.isLoadingEvents}
      />
    );

    return (
      <EventPageContainer>
        <div className="event-tab-container">
          <div
            className={`internal tab ${isInternal ? "active" : ""}`}
            onClick={() => this.handleClick("internal")}
          >
            Internal
          </div>
          <div
            className={`external tab ${isInternal ? "" : "active"}`}
            onClick={() => this.handleClick("external")}
          >
            External
          </div>
        </div>
        {renderEventContent}
      </EventPageContainer>
    );
  }
}

EventListPage.propTypes = {
  internalEvents: PropTypes.array.isRequired,
  externalEvents: PropTypes.array.isRequired,
  fetchEvents: PropTypes.func.isRequired,
  isLoadedEvents: PropTypes.bool.isRequired,
  isLoadingEvents: PropTypes.bool.isRequired
};

function mapStateToProps(state) {
  return {
    internalEvents: getInternalEvents(state),
    externalEvents: getExternalEvents(state),
    isLoadedEvents: isEventsLoaded(state),
    isLoadingEvents: isEventsLoading(state)
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchEvents: () => dispatch(fetchEvents())
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EventListPage);

/* eslint-disable no-case-declarations */
/* eslint-disable array-callback-return */
import { fromJS } from "immutable";
import {
  FETCH_EVENT_LIST,
  FETCH_EVENT_LIST_SUCCESS,
  FETCH_EVENT_LIST_FAILED
} from "./constants";

const initialState = fromJS({
  internalEvents: [],
  externalEvents: [],
  allEvents: [],
  error: null,
  isLoaded: false,
  isLoading: false
});

function eventListPageReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_EVENT_LIST:
      return state.set("isLoading", true);
    case FETCH_EVENT_LIST_SUCCESS:
      const internalEvents = [];
      const externalEvents = [];

      action.payload.map(event => {
        if (event.organizer) {
          externalEvents.push(event);
        } else {
          internalEvents.push(event);
        }
      });

      return state
        .set("internalEvents", internalEvents)
        .set("externalEvents", externalEvents)
        .set("allEvents", action.payload)
        .set("isLoading", false)
        .set("isLoaded", true);
    case FETCH_EVENT_LIST_FAILED:
      return state
        .set("error", action.payload)
        .set("isLoading", false)
        .set("isLoaded", false);
    default:
      return state;
  }
}

export default eventListPageReducer;

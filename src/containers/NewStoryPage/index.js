import React from "react";
import { NewStoryPageContainer } from "./style";
import Card from "../../components/Card";
import { Buttons } from "components/Buttons";
import { postStory, resetState } from "./actions";
import PropTypes from "prop-types";
import { push } from "connected-react-router";
import { connect } from "react-redux";
import Loading from "components/Loading";
import Button from "components/Button";

class NewStoryPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      link: null
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    const value = event.target.value;

    this.setState({ link: value });
  }

  handleSubmit(event) {
    event.preventDefault();
    const obj = {
      link: `https://medium.com/${this.state.link}`
    };
    this.props.postStory(obj);
  }

  componentDidUpdate() {
    if (this.props.isSuccess) {
      this.props.resetState();
      this.props.push({
        pathname: "/profile",
        state: { key: "story" }
      });
    }
  }

  render() {
    return this.props.isLoading ? (
      <Loading />
    ) : (
      <NewStoryPageContainer>
        <div className="back-button-wrap">
          <Button
            text="<<< Back"
            onClick={() => this.props.push("/profile?=story")}
          />
        </div>
        <h1>Lets Share Your Story to Our New Family!</h1>
        <Card>
          <h2>Fill the information below</h2>
          <form onSubmit={this.handleSubmit}>
            <div className="formSection">
              <label>Medium URL</label>
              <div className="urlsection">
                <div className="leftSection">
                  <small>https://medium.com/</small>
                </div>
                <div className="rightSection">
                  <input onChange={this.handleChange} />
                </div>
              </div>
              <div className="submitButton">
                <div className="button">
                  <Buttons name="Save" type="submit" wide="8rem" />
                </div>
              </div>
            </div>
          </form>
        </Card>
      </NewStoryPageContainer>
    );
  }
}

function mapStateToProps(state) {
  return {
    link: state.postStoryReducer.get("link"),
    isLoading: state.postStoryReducer.get("isLoading"),
    isSuccess: state.postStoryReducer.get("isSuccess")
  };
}

function mapDispatchToProps(dispatch) {
  return {
    postStory: obj => dispatch(postStory(obj)),
    resetState: () => dispatch(resetState()),
    push: url => dispatch(push(url))
  };
}

NewStoryPage.propTypes = {
  link: PropTypes.object.isRequired,
  postStory: PropTypes.func.isRequired,
  push: PropTypes.func.isRequired,
  isLoading: PropTypes.bool,
  resetState: PropTypes.func.isRequired,
  isSuccess: PropTypes.bool.isRequired
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewStoryPage);

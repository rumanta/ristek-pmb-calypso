export const FETCH_LINK = "src/NewStoryPage/FETCH_LINK";

export const FETCH_LINK_SUCCESS = "src/NewStoryPage/FETCH_LINK_SUCCESS";

export const FETCH_LINK_FAILED = "src/NewStoryPage/FETCH_LINK_FAILED";

export const RESET_STATE = "src/NewStoryPage/RESET_STATE";

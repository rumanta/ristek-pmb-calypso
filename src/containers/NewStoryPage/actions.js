import axios from "axios";

import {
  FETCH_LINK,
  FETCH_LINK_SUCCESS,
  FETCH_LINK_FAILED,
  RESET_STATE
} from "./constants";

import { fetchStory } from "api";

import swal from "sweetalert";

export function resetState() {
  return dispatch =>
    dispatch({
      type: RESET_STATE
    });
}

export function postStory(obj) {
  return dispatch => {
    dispatch({
      type: FETCH_LINK
    });
    axios
      .post(fetchStory, obj)
      .then(response => {
        dispatch({
          payload: response.data,
          type: FETCH_LINK_SUCCESS
        });
        swal("Successful", "Your story has been shared!", "success");
      })
      .catch(error => {
        dispatch({
          payload: error,
          type: FETCH_LINK_FAILED
        });
        swal(
          "Error",
          error.message === "Request failed with status code 406"
            ? `Failed to edit your story, URL is not a valid Medium Post URL`
            : error.message,
          "error"
        );
      });
  };
}

import {
  FETCH_LINK,
  FETCH_LINK_SUCCESS,
  FETCH_LINK_FAILED,
  RESET_STATE
} from "./constants";
import { fromJS } from "immutable";

const initialState = fromJS({
  error: null,
  isLoading: false,
  link: null,
  isSuccess: false
});

export default function postStoryReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_LINK:
      return state.set("isLoading", true);
    case FETCH_LINK_SUCCESS:
      return state
        .set("isLoading", false)
        .set("link", action.payload)
        .set("isSuccess", true);
    case FETCH_LINK_FAILED:
      return state.set("error", action.payload).set("isLoading", false);
    case RESET_STATE:
      return state
        .set("error", null)
        .set("isLoading", false)
        .set("isSuccess", false);
    default:
      return state;
  }
}

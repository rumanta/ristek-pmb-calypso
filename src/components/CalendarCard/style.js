import styled from "styled-components";

export const CalendarCardStyle = styled.div`
  .CalendarCard__title {
    margin-bottom: 0.5rem;
    color: ${props => props.theme.colors.darkGray};
  }

  .CalendarCard__tile {
    color: ${props => props.theme.colors.darkGray};
  }

  #Calendar {
    .react-calendar {
      border: none;
      font-family: inherit;
    }

    /* navigation */
    .react-calendar__navigation__label {
      font-weight: ${props => props.theme.weight.bold};
      font-size: 1.5rem;

      @media (max-width: ${props => props.theme.sizes.widescreen}) {
        font-size: 1rem;
      }
    }
    .react-calendar__navigation__prev2-button {
      display: none;
    }
    .react-calendar__navigation__next2-button {
      display: none;
    }
    .react-calendar__navigation button[disabled] {
      background-color: inherit;
    }

    /* days of week */
    abbr[title] {
      text-decoration: none;
      font-weight: ${props => props.theme.weight.regular};
    }

    /* calendar day tile style */
    .react-calendar__tile {
      box-sizing: border-box;
      display: flex;
      position: relative;
      align-items: center;
      padding: 0;
      width: 14.2857%;
      height: 0;
      overflow: visible !important;
      /* ensure 1:1 ratio */
      padding-bottom: 14.2857%;
      border-radius: 50%;

      /* centers day vertically and horizontally */
      & > abbr {
        position: absolute;
        width: 100%;
        top: 50%;
        transform: translateY(-50%);
      }
    }

    /* calendar day tile selected */
    .react-calendar__tile--active,
    .react-calendar__tile:hover {
      /* very light blue */
      background-color: #c4dbff;

      & > abbr {
        color: ${props => props.theme.colors.blue};
      }
    }

    /* calendar day tile today */
    .react-calendar__tile.react-calendar__tile--now {
      background-color: ${props => props.theme.colors.lightBlue};

      & > abbr {
        color: ${props => props.theme.colors.white};
      }
    }

    /* calendar day tile with events */
    .CalendarCard__tile__event abbr::after {
      content: "";
      position: absolute;
      left: 50%;
      top: 100%;
      transform: translateX(-50%);
      border-radius: 50%;
      width: 5px;
      height: 5px;
      background-color: ${props => props.theme.colors.blue};
    }

    /* custom tile content for tooltip */
    .Calendar__tileContent {
      position: absolute;
      width: 100%;
      height: 100%;
    }

    @media (max-width: ${props => props.theme.sizes.widescreen}) {
      .react-calendar__month-view {
        font-size: 0.75rem;
      }
    }

    .Calendar__Tooltip__messages {
      display: flex;
      flex-direction: column;
      justify-content: flex-start;

      & > * {
        display: inline-block;
        text-align: left;
      }
    }
  }
`;

export const TooltipStyle = styled.div`
  .Tooltip {
    display: inline-block;
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
  }

  .Tooltip__trigger {
    display: inline-block;
    text-decoration: underline;
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
  }

  .Tooltip__bubble {
    max-width: 270px;
    width: fit-content;
    position: absolute;
    z-index: 10;

    &::after {
      content: "";
      position: absolute;
    }
  }

  .Tooltip__top {
    bottom: 100%;
    left: 50%;
    padding-bottom: 9px;
    transform: translateX(-50%);
  }

  .Tooltip__message {
    background: ${props => props.theme.colors.darkGray};
    color: ${props => props.theme.colors.black};
    opacity: 1;
    border-radius: 3px;
    font-size: 0.75rem;
    line-height: 1.4;
    padding: 0.75em;
    text-align: center;

    a {
      color: inherit;
      text-decoration: none;
      white-space: nowrap;

      &:hover {
        text-decoration: underline;
      }
    }
  }

  .Tooltip__message {
    position: relative;
    max-width: 30em;
    background-color: ${props => props.theme.colors.white};
    padding: 0.75rem;
    font-size: 1.25em;
    border-radius: 1rem;
    box-shadow: ${props => props.theme.boxShadow};
  }

  .Tooltip__message::before {
    content: "";
    position: absolute;
    width: 0;
    height: 0;
    top: 100%;
    left: 50%;
    transform: translateX(-50%) rotate(180deg);
    border: 0.75rem solid transparent;
    border-top: none;
    border-bottom-color: ${props => props.theme.colors.white};
  }
`;

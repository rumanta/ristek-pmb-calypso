import React from "react";
import { ProfileCardContainer } from "./style";
import PropTypes from "prop-types";
import CakeIcon from "../../assets/icons/birthday.svg";
import BookIcon from "../../assets/icons/book.svg";
import SchoolIcon from "../../assets/icons/college.svg";
import EmailIcon from "../../assets/icons/email.svg";
import LineIcon from "../../assets/icons/line.svg";
import WebIcon from "../../assets/icons/web.svg";
import CheckIcon from "assets/icons/check-box.svg";
import UncheckIcon from "assets/icons/check-box-empty.svg";
import { Buttons } from "../Buttons";
import ProfilePicture from "../ProfilePicture";

export default class ProfileCard extends React.Component {
  renderListOfDetails = details =>
    details.map((detail, index) => (
      <div key={index} className="detail">
        <img className="detailImage" src={detail.icon} />
        <p className="detailContent">{detail.content ? detail.content : "-"}</p>
      </div>
    ));

  render() {
    const {
      profileImage,
      name,
      position,
      major,
      highSchool,
      email,
      line,
      linkedin,
      isPrivate
    } = this.props.profile;

    const placeDateOfBirth = `${this.props.profile.placeOfBirth}, ${
      this.props.profile.dateOfBirth
    }`;

    const leftData = [
      {
        icon: BookIcon,
        content: major
      },
      {
        icon: SchoolIcon,
        content: highSchool
      },
      {
        icon: CakeIcon,
        content: placeDateOfBirth
      }
    ];

    const rightData = [
      {
        icon: EmailIcon,
        content: email
      },
      {
        icon: LineIcon,
        content: line
      },
      {
        icon: WebIcon,
        content: linkedin
      }
    ];

    return (
      <ProfileCardContainer>
        <div className="cardContainer">
          <div>
            <ProfilePicture src={profileImage} />
            <div className="checkBoxContainer">
              <img
                className="check-box-icon"
                src={isPrivate ? CheckIcon : UncheckIcon}
              />
              <p className="private-mode-text">Private Mode</p>
            </div>
          </div>
          <div className="profileSection">
            <div className="titleSection">
              <h1>{name}</h1>
              <h3>{position}</h3>
            </div>
            <div className="detailsSection">
              <div className="leftDetails">
                {this.renderListOfDetails(leftData)}
              </div>
              <div className="rightDetails">
                {this.renderListOfDetails(rightData)}
              </div>
            </div>
            <div className="checkBoxContainerMobile">
              <img
                className="check-box-icon"
                src={isPrivate ? CheckIcon : UncheckIcon}
              />
              <p className="private-mode-text">Private Mode</p>
            </div>
            <div className="button">
              <Buttons
                name="Edit"
                wide="8rem"
                type="button"
                onPressed={() => this.props.push("/editprofile")}
              />
            </div>
          </div>
        </div>
      </ProfileCardContainer>
    );
  }
}

ProfileCard.propTypes = {
  profile: PropTypes.node,
  push: PropTypes.func
};

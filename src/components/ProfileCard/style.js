import styled from "styled-components";

export const ProfileCardContainer = styled.div`
  border-radius: 10px;
  background: white;
  box-shadow: ${props => props.theme.boxShadow};
  color: ${props => props.theme.colors.primaryBlack};
  font-family: Rubik;
  width: 100%;
  position: relative;
  z-index: 1;

  .cardContainer {
    display: flex;
    flex-direction: row;
    justify-content: center;
    padding: 1rem;
  }

  .profileSection {
    display: flex;
    flex-direction: column;
    flex-grow: 4;
    padding: 1rem;
  }

  .titleSection {
    width: 100%;
    margin-bottom: 1rem;
  }

  .titleSection h1,
  h3 {
    margin: 0rem;
  }

  .titleSection h1 {
    font-weight: ${props => props.theme.weight.bold};
    font-size: 24px;
  }

  .titleSection h3 {
    font-weight: ${props => props.theme.weight.regular};
    color: ${props => props.theme.colors.gray};
    font-size: 14px;
  }

  .detailsSection {
    display: flex;
    flex-direction: row;
  }

  .detailsSection div {
    align-items: center;
    width: 100%;
    justify-contents: center;
  }

  .detail {
    width: 100%;
    display: flex;
    flex-direction: row;
    margin-bottom: 1.5rem;
  }

  .detail .detailImage {
    margin-right: 0.75rem;
    width: 1.5rem;
  }

  .detailContent {
    margin-bottom: 0;
  }

  .detail p {
    font-size: 14px;
  }

  .checkBoxContainer {
    display: flex;
    align-items: center;
    margin: 1rem 0 0 12px;
  }

  .checkBoxContainerMobile {
    display: none;
    align-items: center;
  }

  .check-box-icon {
    width: 1rem;
    margin-right: 12px;
  }

  .private-mode-text {
    font-size: 14px;
    color: ${props => props.theme.colors.primaryBlack};
  }

  @media (max-width: 1216px) {
    .detail p {
      font-size: 12px;
    }
    .private-mode-text {
      font-size: 12px;
    }

    .detail .detailImage {
      width: 1.25rem;
    }

    .titleSection h1 {
      font-size: 22px;
    }

    .titleSection h3 {
      font-size: 12px;
    }
  }

  @media (max-width: 1024px) {
    .cardContainer {
      flex-direction: column;
    }

    .titleSection {
      display: flex;
      flex-direction: column;
      align-items: center;
      margin-bottom: 1.5rem;
      text-align: center;
    }

    .detailsSection {
      flex-direction: column;
    }

    .detail p {
      font-size: 14px;
    }

    .detail .detailImage {
      width: 1.5rem;
    }

    .checkBoxContainer {
      display: none;
    }
    .checkBoxContainerMobile {
      display: flex;
    }

    .check-box-icon {
      width: 1.2rem;
    }
  }

  @media (max-width: 580px) {
  }

  @media (max-width: 480px) {
    .titleSection h1 {
      font-size: 20px;
    }

    .titleSection h3 {
      font-size: 12px;
    }

    .detail {
      margin-bottom: 0.5rem;
    }

    .detail p {
      font-size: 12px;
    }

    .detail .detailImage {
      width: 1rem;
    }
  }

  .check-box-icon {
    width: 0.8rem;
  }

  .button {
    margin-top: 0.75rem;
    display: flex;
    align-items: flex-end;
    justify-content: flex-end;
  }
`;

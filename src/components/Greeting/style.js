import styled from "styled-components";

export const GreetingStyle = styled.div`
  .Greeting {
    margin-bottom: 1rem;
  }

  .Greeting__title {
    font-size: 1.5rem;
    margin-bottom: 0.5rem;
  }

  .Greeting__subtitle {
  }
`;

import React, { Component } from "react";
import PropTypes from "prop-types";

import { GreetingStyle } from "./style";

class Greeting extends Component {
  render() {
    const name = this.props.name;

    return (
      <GreetingStyle>
        <div className="Greeting">
          <div className="Greeting__title">Welcome, {name}</div>
          <div className="Greeting__subtitle">
            Get to know your new friends now.
          </div>
        </div>
      </GreetingStyle>
    );
  }
}

Greeting.propTypes = {
  name: PropTypes.string.isRequired
};

export default Greeting;

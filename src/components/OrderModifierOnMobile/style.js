import styled from "styled-components";

export const OrderModifierOnMobileStyle = styled.div`
  @media (max-width: ${props => props.theme.sizes.desktop}) {
    order: ${props => props.order};
  }
`;

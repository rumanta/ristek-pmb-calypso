import React, { Component } from "react";
import PropTypes from "prop-types";

import { OrderModifierOnMobileStyle } from "./style";

class OrderModifierOnMobile extends Component {
  render() {
    const order = this.props.order ? this.props.order : 0;

    return (
      <OrderModifierOnMobileStyle order={order}>
        <div className="OrderModifierOnMobile">{this.props.children}</div>
      </OrderModifierOnMobileStyle>
    );
  }
}

OrderModifierOnMobile.propTypes = {
  order: PropTypes.number,
  children: PropTypes.node.isRequired
};

export default OrderModifierOnMobile;

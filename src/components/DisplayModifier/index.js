import React, { Component } from "react";
import PropTypes from "prop-types";

import { DisplayModifierStyle } from "./style";

// class to change css property display to none
// on either mobile only or desktop only specified
// by props showOn
class DisplayModifier extends Component {
  render() {
    const showChildrenOn = this.props.showOn;

    return (
      <DisplayModifierStyle showOn={showChildrenOn}>
        <div className="DisplayModifier">{this.props.children}</div>
      </DisplayModifierStyle>
    );
  }
}

DisplayModifier.propTypes = {
  children: PropTypes.node.isRequired,
  showOn: PropTypes.oneOf(["mobile", "desktop"]).isRequired
};

export default DisplayModifier;

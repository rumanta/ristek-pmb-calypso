import styled from "styled-components";

export const DisplayModifierStyle = styled.div`
  @media (max-width: ${props => props.theme.sizes.desktop}) {
    /* if mobile then display block, if desktop 
    display none on small screen */
    .DisplayModifier {
      display: ${props => (props.showOn === "mobile" ? "block" : "none")};
    }
  }

  @media (min-width: ${props => props.theme.sizes.desktop}) {
    /* if mobile then display none, if desktop 
    display block on large screen */
    .DisplayModifier {
      display: ${props => (props.showOn === "mobile" ? "none" : "block")};
    }
  }
`;

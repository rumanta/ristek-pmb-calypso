import React, { Component } from "react";
import { EmptyCardContainer } from "./style";
import PropTypes from "prop-types";

class EmptyCard extends Component {
  render() {
    let h2Style = {};
    let bodyStyle = {};

    if (this.props.passive) {
      h2Style = {
        fontWeight: 400,
        fontSize: "14px",
        color: "#BBB2C4"
      };
    }

    if (this.props.passive) {
      bodyStyle = {
        borderStyle: "none",
        padding: 0
      };
    }

    return (
      <EmptyCardContainer style={bodyStyle}>
        <div>
          <h2 style={h2Style}>{this.props.text}</h2>
        </div>
      </EmptyCardContainer>
    );
  }
}

EmptyCard.propTypes = {
  text: PropTypes.string.isRequired,
  passive: PropTypes.bool
};

export default EmptyCard;

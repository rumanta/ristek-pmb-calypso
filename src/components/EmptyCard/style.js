import styled from "styled-components";

export const EmptyCardContainer = styled.div`
  position: relative;
  padding: 0 1rem;
  border-radius: 7px;
  border-style: solid;
  z-index: 3;
  border-width: 1px;
  border-color: ${props => props.theme.colors.blue};
  height: 60px;
  display: flex;
  justify-content: center;
  align-items: center;

  h2 {
    font-weight: ${props => props.theme.weight.bold};
    font-size: 18px;
    font-color: ${props => props.theme.colors.primaryBlack};
  }

  @media (max-width: 600px) {
    h2 {
      font-size: 16px;
    }
  }

  @media (max-width: 400px) {
    h2 {
      font-size: 14px;
    }
  }
`;

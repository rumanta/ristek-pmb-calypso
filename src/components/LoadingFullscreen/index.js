import React from "react";

import { LoadingContainer } from "./style";

class LoadingFullscreen extends React.Component {
  render() {
    return (
      <LoadingContainer>
        <div className="overlay">
          <div className="overlay-content">
            <div className="spinner" />
          </div>
        </div>
      </LoadingContainer>
    );
  }
}

export default LoadingFullscreen;

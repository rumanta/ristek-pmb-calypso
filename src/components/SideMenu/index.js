import React, { Component } from "react";
import { SideMenuStyle } from "./style";
import IconPMB from "../../assets/logos/pmb-logo.png";
import PropTypes from "prop-types";

import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { logout } from "globalActions";
import { push } from "connected-react-router";
import { compose } from "redux";

class SideMenu extends Component {
  handleLogout = () => {
    this.props.logout();
    this.props.onLogout();
  };

  // eslint-disable-next-line consistent-return
  currentURLCheck(url) {
    const grey = { color: "grey" };
    if (url === this.props.location.pathname) {
      return grey;
    }
  }

  render() {
    const menuClass = ["sidemenu"];
    if (this.props.isOpened) {
      menuClass.push("open");
    }

    return (
      <SideMenuStyle>
        <div className={menuClass.join(" ")}>
          <div className="pmb-logo-container">
            <div className="pmb__wrapper">
              <img
                onClick={() => this.props.changePage("/")}
                src={IconPMB}
                alt="icon-pmb"
              />
            </div>
          </div>
          <div className="menu-container">
            <div>
              <a
                style={this.currentURLCheck("/profile")}
                onClick={() => this.props.changePage("/profile")}
              >
                Profile
              </a>
            </div>
            <div>
              <a
                style={this.currentURLCheck("/friends")}
                onClick={() => this.props.changePage("/friends")}
              >
                Friends
              </a>
            </div>
            <div>
              <a
                style={this.currentURLCheck("/tasks")}
                onClick={() => this.props.changePage("/tasks")}
              >
                Task
              </a>
            </div>
            <div>
              <a
                style={this.currentURLCheck("/events")}
                onClick={() => this.props.changePage("/events")}
              >
                Event
              </a>
            </div>
            <div>
              <a
                style={this.currentURLCheck("/announcements")}
                onClick={() => this.props.changePage("/announcements")}
              >
                Announcement
              </a>
            </div>
            <div>
              <a
                style={this.currentURLCheck("/stories")}
                onClick={() => this.props.changePage("/stories")}
              >
                Story
              </a>
            </div>
            <div>
              <a
                style={this.currentURLCheck("/elemen")}
                onClick={() => this.props.changePage("/elemen")}
              >
                Apa Kata Elemen
              </a>
            </div>
            <div>
              <a
                style={this.currentURLCheck("/gallery")}
                onClick={() => this.props.changePage("/gallery")}
              >
                Gallery
              </a>
            </div>
            <div>
              <a
                style={this.currentURLCheck("/about")}
                onClick={() => this.props.changePage("/about")}
              >
                About
              </a>
            </div>
            <div>
              <a
                style={this.currentURLCheck("/statistics")}
                onClick={() => this.props.changePage("/statistics")}
              >
                Statistics
              </a>
            </div>
            <div>
              <a
                style={this.currentURLCheck("/need-help")}
                onClick={() => this.props.changePage("/need-help")}
              >
                Need Help
              </a>
            </div>
          </div>
          <div className="button-container">
            <div className="logout button" onClick={() => this.handleLogout()}>
              <a>Logout</a>
            </div>
            <div className="back button" onClick={this.props.closeMenu}>
              <a>Back</a>
            </div>
          </div>
        </div>
      </SideMenuStyle>
    );
  }
}
SideMenu.propTypes = {
  isOpened: PropTypes.bool,
  closeMenu: PropTypes.func,
  onLogout: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired,
  push: PropTypes.func.isRequired,
  changePage: PropTypes.func,
  location: PropTypes.shape({
    pathname: PropTypes.string
  })
};

function mapDispatchToProps(dispatch) {
  return {
    logout: () => dispatch(logout()),
    push: url => dispatch(push(url))
  };
}

export default compose(
  withRouter,
  connect(
    null,
    mapDispatchToProps
  )
)(SideMenu);

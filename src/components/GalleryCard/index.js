import React, { Component } from "react";
import PropTypes from "prop-types";

import { GalleryCardStyle, MiniImage } from "./style";
import Card from "../Card";

class GalleryCard extends Component {
  getImageAlternateString = (albumIndex, imageIndex) =>
    `album${albumIndex}_image${imageIndex}`;

  imageFactory = () => {
    const MiniImageObjects = this.props.imageArray
      .slice(0, 3)
      .map((imageUrl, index) => (
        <MiniImage
          key={index}
          data-image-index={index}
          data-album-index={this.props.albumIndex}
          src={imageUrl}
          onClick={this.props.onClickImage}
          alt={this.getImageAlternateString(this.props.albumIndex, index)}
        />
      ));

    return MiniImageObjects;
  };

  render() {
    const largeImageIndex = 3;

    return (
      <GalleryCardStyle>
        <Card>
          <div className="GalleryCard__card">
            <div className="GalleryCard__column__left">
              {this.imageFactory()}
            </div>
            <div className="GalleryCard__column__right">
              <img
                src={this.props.imageArray[largeImageIndex]}
                data-image-index={largeImageIndex}
                data-album-index={this.props.albumIndex}
                onClick={this.props.onClickImage}
                alt={this.getImageAlternateString(
                  this.props.albumIndex,
                  largeImageIndex
                )}
              />
            </div>
          </div>
        </Card>
      </GalleryCardStyle>
    );
  }
}

GalleryCard.propTypes = {
  imageArray: PropTypes.arrayOf(PropTypes.string).isRequired,
  onClickImage: PropTypes.func.isRequired,
  albumIndex: PropTypes.number.isRequired
};

export default GalleryCard;

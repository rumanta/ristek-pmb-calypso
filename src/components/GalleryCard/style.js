import styled from "styled-components";

export const GalleryCardStyle = styled.div`
  .GalleryCard__card {
    display: flex;
  }

  .GalleryCard__column__left {
    display: flex;
    flex-direction: column;
    flex: 1;
  }

  .GalleryCard__column__right {
    display: flex;
    width: 100%;
    min-height: 100%;
    flex: 3;

    img {
      height: auto;
      width: 100%;
      object-fit: cover;
      object-position: center;
      cursor: pointer;
      max-height: 25rem;
    }
  }

  @media (max-width: ${props => props.theme.sizes.tablet}) {
    .GalleryCard__column__right {
      img {
        max-height: 12.5rem;
      }
    }
  }
`;

export const MiniImage = styled.img`
  max-height: 8rem;
  object-fit: cover;
  object-position: center;
  cursor: pointer;
  margin: 0 0.5rem 0.5rem 0.5rem;

  @media (max-width: ${props => props.theme.sizes.tablet}) {
    max-height: 4rem;
    margin: 0 0.25rem 0.25rem 0.25rem;
  }
`;

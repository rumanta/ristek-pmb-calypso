import React, { Component } from "react";
import PropTypes from "prop-types";

import DisplayModifier from "../DisplayModifier";
import nextButtonImage from "../../assets/gallery-next-button.svg";
import closeButtonImage from "../../assets/gallery-close-button.svg";
import { GalleryCarouselStyle, GalleryCarouselImageStyle } from "./style";

class GalleryCarousel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // default index if not specified is 0
      imageIndex: this.props.imageIndex || 0,
      // indicates opening of carousel,
      // will not play sliding animation when true
      justOpened: false
    };

    this.handleResize = this.handleResize.bind(this);
    this.closeCarousel = this.closeCarousel.bind(this);
    this.handleLeftArrowClick = this.handleLeftArrowClick.bind(this);
    this.handleRightArrowClick = this.handleRightArrowClick.bind(this);
    this.handleMenuImageClick = this.handleMenuImageClick.bind(this);
  }

  UNSAFE_componentWillMount() {
    this.handleResize();
  }

  componentDidMount() {
    window.addEventListener("resize", this.handleResize);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.handleResize);
  }

  componentDidUpdate(prevProps) {
    // when carousel closes
    if (prevProps.isOpen && !this.props.isOpen) {
      this.setAllowBodyScrolling(true);
    }
    // when carousel opens
    if (!prevProps.isOpen && this.props.isOpen) {
      this.setAllowBodyScrolling(false);
      this.setState(
        {
          imageIndex: this.props.imageIndex,
          justOpened: true
        },
        // set justOpened to false without rerender after above setState done
        // eslint-disable-next-line react/no-direct-mutation-state
        () => (this.state.justOpened = false)
      );
    }
  }

  // rerender component on resize to adjust translate css property on sliders
  handleResize() {
    this.setState(this.state);
  }

  closeCarousel() {
    this.props.onCloseCarousel();
  }

  handleLeftArrowClick(event) {
    event.stopPropagation();
    this.setState(state => ({
      imageIndex: this.modulo(state.imageIndex - 1, this.props.imageUrls.length)
    }));
  }

  handleRightArrowClick(event) {
    event.stopPropagation();
    this.setState(state => ({
      imageIndex: this.modulo(
        // ensure addition of numbers, not string concatenation
        Number(state.imageIndex) + Number(1),
        this.props.imageUrls.length
      )
    }));
  }

  handleMenuImageClick(event) {
    event.stopPropagation();
    this.setState({
      imageIndex: event.target.dataset.key
    });
  }

  // get value of translateX css property for the main slider
  getTranslateValue = () => -1 * this.state.imageIndex * this.getSlideWidth();

  // get value of translateX css property for bottom menu slider
  getMenuTranslateValue = () =>
    -1 * this.state.imageIndex * this.getMenuSlideWidth();

  // get distance of one slide for main slider
  getSlideWidth = () => {
    const elements = document.getElementsByClassName(
      "GalleryCarouselImage__carousel-image"
    );

    return elements.length !== 0 ? elements[0].clientWidth : 0;
  };

  // get distance of one slide for bottom menu slider
  getMenuSlideWidth = () => {
    const elements = document.getElementsByClassName(
      "GalleryCarouselMenuImage__menu-image"
    );
    if (elements.length !== 0) {
      const element = elements[0];
      const style = element.currentStyle || window.getComputedStyle(element);

      return element.clientWidth + this.stringPixelToInteger(style.marginRight);
    }

    return 0;
  };

  // converts string "10px" to number 10
  stringPixelToInteger(string) {
    return parseInt(string.slice(0, -2), 10);
  }

  // modulo always return positive number
  modulo = (number, modulo) => ((number % modulo) + modulo) % modulo;

  // prevent page scroll
  setAllowBodyScrolling(isAllowed) {
    if (!isAllowed) {
      document.body.style.overflow = "hidden";
      document.body.addEventListener(
        "touchmove",
        event => event.preventDefault(),
        false
      );
    } else {
      document.body.style.overflow = "initial";
      document.body.removeEventListener(
        "touchmove",
        event => event.preventDefault(),
        false
      );
    }
  }

  render() {
    const carouselIsOpen = this.props.isOpen;
    const imageUrls = this.props.imageUrls;
    const darkenPercentage = 0.7;
    const translateValue = this.getTranslateValue();
    const menuTranslateValue = this.getMenuTranslateValue();
    const isPlayAnimation = !this.state.justOpened;

    return (
      <GalleryCarouselStyle
        darkenPercentage={darkenPercentage}
        nextButtonImage={nextButtonImage}
        closeButtonImage={closeButtonImage}
        translateValue={translateValue}
        menuTranslateValue={menuTranslateValue}
        isPlayAnimation={isPlayAnimation}
      >
        <div
          className={
            // eslint-disable-next-line prefer-template
            "GalleryCarousel " +
            (!carouselIsOpen ? "GalleryCarousel__hide" : "")
          }
        >
          <div
            className="GalleryCarousel__button-close"
            onClick={this.closeCarousel}
          />
          <div
            className="GalleryCarousel__button-left"
            onClick={this.handleLeftArrowClick}
          />
          <div className="GalleryCarousel__content">
            <div className="GalleryCarousel__carousel">
              <div className="GalleryCarousel__carousel__slider">
                <div className="GalleryCarousel__carousel__slider-wrapper">
                  {imageUrls.map((imageUrl, index) => (
                    <GalleryCarouselImage
                      key={index}
                      imageUrl={imageUrl}
                      imageIndex={index}
                    />
                  ))}
                </div>
              </div>
            </div>
            <DisplayModifier showOn="desktop">
              <div className="GalleryCarousel__menu">
                <div className="GalleryCarousel__menu__slider">
                  <div className="GalleryCarousel__menu__slider-wrapper">
                    {imageUrls.map((imageUrl, index) => (
                      <GalleryCarouselMenuImage
                        key={index}
                        imageIndex={index}
                        imageUrl={imageUrl}
                        functionOnClick={this.handleMenuImageClick}
                      />
                    ))}
                  </div>
                </div>
              </div>
            </DisplayModifier>
          </div>
          <div
            className="GalleryCarousel__button-right"
            onClick={this.handleRightArrowClick}
          />
        </div>
      </GalleryCarouselStyle>
    );
  }
}

const GalleryCarouselImage = ({ imageUrl, functionOnClick, imageIndex }) => (
  <GalleryCarouselImageStyle
    className="GalleryCarouselImage__carousel-image"
    image={imageUrl}
    onClick={functionOnClick}
    alt={`carousel_image${imageIndex}`}
  />
);

const GalleryCarouselMenuImage = ({
  imageUrl,
  functionOnClick,
  imageIndex
}) => (
  <GalleryCarouselImageStyle
    className="GalleryCarouselMenuImage__menu-image"
    image={imageUrl}
    onClick={functionOnClick}
    data-key={imageIndex}
    alt={`carousel_menu_image${imageIndex}`}
  />
);

GalleryCarousel.propTypes = {
  imageUrls: PropTypes.arrayOf(PropTypes.string).isRequired,
  onCloseCarousel: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
  imageIndex: PropTypes.number
};

GalleryCarouselImage.propTypes = {
  imageUrl: PropTypes.string,
  functionOnClick: PropTypes.func,
  imageIndex: PropTypes.number
};

GalleryCarouselMenuImage.propTypes = {
  imageUrl: PropTypes.string,
  functionOnClick: PropTypes.func,
  imageIndex: PropTypes.number
};

export default GalleryCarousel;

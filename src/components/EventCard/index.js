/* eslint-disable global-require */
import React, { Component } from "react";
import PropTypes from "prop-types";
// import { push } from "connected-react-router"

import Card from "../Card";
import Button from "../Button";
import { EventCardStyle } from "./style";
import CalendarIcon from "../../assets/calendar_1.svg";
import LocationIcon from "../../assets/icons/location-80.png";

class EventCard extends Component {
  render() {
    const {
      title,
      date,
      location,
      organizer,
      coverImage,
      organizerImage
    } = this.props;

    return (
      <EventCardStyle image={coverImage} logo={organizerImage}>
        <div className="EventCard">
          <Card>
            <div className="EventCard__card">
              {this.props.coverImage ? (
                <div className="EventCard__card__image" />
              ) : (
                ""
              )}
              <div className="EventCard__card__content">
                <div className="row">
                  <h1>{title}</h1>
                  {organizer ? (
                    <div className="desktop logoset-container">
                      <div className="EventCard__card__logo" />
                      <div className="logoset-description">
                        <p>{organizer}</p>
                      </div>
                    </div>
                  ) : (
                    ""
                  )}
                </div>
                <div className="EventCard__card__date">
                  <img
                    src={CalendarIcon}
                    alt="calendar icon"
                    className="EventCard__card__date__icon"
                  />
                  <p>{date}</p>
                </div>
                <div className="EventCard__card__location">
                  <img
                    src={LocationIcon}
                    alt="location icon"
                    className="EventCard__card__location__icon"
                  />
                  <p>{location}</p>
                </div>
                <div className="EventCard__card__footer">
                  <div className="mobile logoset-container">
                    {organizerImage ? (
                      <div className="EventCard__card__logo" />
                    ) : (
                      ""
                    )}
                    {organizer && (
                      <div className="logoset-description">
                        <p>{organizer}</p>
                      </div>
                    )}
                  </div>
                  <Button text="Details" onClick={this.props.onClick} />
                </div>
              </div>
            </div>
          </Card>
        </div>
      </EventCardStyle>
    );
  }
}

EventCard.defaultProps = {
  detailsUrl: "#"
};

EventCard.propTypes = {
  title: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  coverImage: PropTypes.string,
  location: PropTypes.string.isRequired,
  organizer: PropTypes.string,
  organizerImage: PropTypes.string,
  detailsUrl: PropTypes.string,
  onClick: PropTypes.func.isRequired
};

export default EventCard;

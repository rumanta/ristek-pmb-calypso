import styled from "styled-components";

export const EventCardStyle = styled.div`
  p,
  h1 {
    margin: 0;
  }

  .EventCard__card__content > * {
    margin-bottom: 0.5rem;

    &:last-child {
      margin-bottom: 0;
    }
  }
  .EventCard .Card {
    padding: 0;
  }

  .EventCard__card__content {
    padding: 1rem;

    p {
      font-size: 14px;
    }

    h1 {
      font-size: 24px;
      color: ${props => props.theme.colors.blue};
    }

    .row {
      display: flex;
      flex-direction: row;
      justify-content: space-between;
      align-items: center;
    }
  }

  .logoset-container {
    display: flex;
    flex-direction: row;
    background: ${props => props.theme.colors.blue};
    color: ${props => props.theme.colors.white};
    border-radius: 7px;

    .logoset-description {
      font-size: 14px;
      padding: 0 5px;
      display: flex;
      flex-direction: column;
      justify-content: center;
    }
  }

  .EventCard__card__image {
    border-radius: 5px;
    box-shadow: ${props => props.theme.boxShadow};
    /* make div width height ratio 3:1 */
    padding-top: 33.3%;
    background-color: gray;
    background-image: url(${props => props.image});
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center center;
  }

  .EventCard__card__logo {
    height: 30px;
    width: 30px;
    background-color: ${props => props.theme.colors.white};
    background-image: url(${props => props.logo});
    background-repeat: no-repeat;
    background-position: center center;
    background-size: contain;
  }

  .EventCard__card__footer {
    display: flex;
    justify-content: flex-end;
    flex-direction: row;
  }

  .EventCard__card__date {
    display: flex;
    flex-direction: row;
    align-items: center;

    .EventCard__card__date__icon {
      margin-right: 0.5rem;
    }
  }

  .EventCard__card__location {
    display: flex;
    flex-direction: row;
    align-items: center;

    .EventCard__card__location__icon {
      margin-right: 0.5rem;
      height: 24px;
    }
  }

  .mobile {
    display: none;
  }

  @media (max-width: 1225px) {
    .logoset-container {
      .logoset-description {
        p {
          font-size: 10px;
        }
      }
    }
  }

  @media (max-width: ${props => props.theme.sizes.desktop}) {
    .mobile {
      display: flex;
    }

    .EventCard__card__footer {
      justify-content: space-between;
    }

    .desktop {
      display: none;
    }
  }

  @media (max-width: 440px) {
    .row h1 {
      font-size: 20px;
    }
  }

  @media (max-width: 360px) {
    .row h1 {
      font-size: 18px;
    }

    .EventCard p {
      font-size: 12px;
    }
  }
`;

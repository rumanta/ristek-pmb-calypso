import styled from "styled-components";

export const TaskListCardStyle = styled.div`
  .TaskListCard__card__title {
    margin-top: 0;
    margin-bottom: 1rem;
    color: ${props => props.theme.colors.blue};
    font-weight: ${props => props.theme.weight.bold};
  }

  h1 {
    font-size: 24px;
  }

  p {
    font-size: 14px;
  }

  .TaskListCard__card__date {
    display: flex;
    align-items: center;
    margin-bottom: 0.5rem;

    .TaskListCard__card__date__icon {
      width: 24px;
      height: 24px;
    }

    .TaskListCard__card__date__text {
      margin-top: 0;
      margin-bottom: 0;
      line-height: 19px;
      margin-left: 0.5rem;
      display: inline-block;
    }
  }

  .TaskListCard__card__content > p {
    margin-top: 0;
    margin-bottom: 0;
  }

  .TaskListCard__card__footer {
    display: flex;
    justify-content: flex-end;
    margin-top: 1rem;
  }

  .red {
    color: ${props => props.theme.colors.red};
  }

  .green {
    color: ${props => props.theme.colors.green};
  }

  @media (max-width: 440px) {
    h1 {
      font-size: 20px;
    }
  }

  @media (max-width: 360px) {
    h1 {
      font-size: 18px;
    }

    p {
      font-size: 12px;
    }
  }
`;

import React, { Component } from "react";
import PropTypes from "prop-types";
import { TaskListCardStyle } from "./style";
import CalendarIcon from "../../assets/calendar_1.svg";
import { timestampParser } from "../../utils/timestampParser";
import Button from "../Button";
import Card from "../Card";

class TaskListCard extends Component {
  render() {
    const title = this.props.title;
    const date = timestampParser(new Date(this.props.date));
    const isSubmissionRequired = this.props.isSubmissionRequired;
    const submissionStringHtml = this.props.isSubmitted ? (
      <p>
        Submission status: <strong className="green">Submitted</strong>
      </p>
    ) : (
      <p>
        Submission status: <strong className="red">No Submission</strong>
      </p>
    );

    return (
      <TaskListCardStyle>
        <div className="TaskListCard">
          <Card>
            <div className="TaskListCard__card">
              <h1 className="TaskListCard__card__title">{title}</h1>
              <div className="TaskListCard__card__date">
                <img
                  src={CalendarIcon}
                  className="TaskListCard__card__date__icon"
                  alt="calendar icon"
                />
                <p className="TaskListCard__card__date__text">{date}</p>
              </div>
              {this.props.isMaba && isSubmissionRequired && (
                <div className="TaskListCard__card__content">
                  {submissionStringHtml}
                </div>
              )}
              <div className="TaskListCard__card__footer">
                <Button text="Details" onClick={this.props.onClick} />
              </div>
            </div>
          </Card>
        </div>
      </TaskListCardStyle>
    );
  }
}

TaskListCard.propTypes = {
  title: PropTypes.string.isRequired,
  date: PropTypes.instanceOf(Date).isRequired,
  isSubmissionRequired: PropTypes.bool,
  isSubmitted: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
  isMaba: PropTypes.bool
};

export default TaskListCard;

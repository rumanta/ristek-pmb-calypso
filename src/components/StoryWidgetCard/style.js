import styled from "styled-components";

export const StoryWidgetCardContainer = styled.div`
  .feedImage {
    width: 100%;
    height: auto;
    margin: 0;
  }

  .time {
    font-size: 0.8rem;
    margin: 0;
  }

  .tanggal {
    padding-top: 0.5rem;
    padding-bottom: 0.5rem;
  }

  .title {
    font-size: 1.25rem;
    font-style: bold;
    margin: 0 0 0.5rem 0;
  }

  .desc {
    font-size: 0.75rem;
    margin: 0 0 0.5rem 0;
  }

  .blog-link {
    color: ${props => props.theme.colors.gray};
  }
`;

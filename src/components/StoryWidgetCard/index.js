import React from "react";
import { PropTypes } from "prop-types";
import Card from "components/Card";
import Moment from "react-moment";
import { StoryWidgetCardContainer } from "./style";
import NoImage from "assets/no-image.png";

export default class StoryWidgetCard extends React.Component {
  renderContent = details => (
    <div className="content">
      <div className="title">{details.title}</div>
      <div className="desc">{details.contents}</div>
      <div className="urlSection">
        <a
          className="blog-link"
          target="_blank"
          rel="noopener noreferrer"
          href={details.link}
        >
          ... More
        </a>
      </div>
    </div>
  );

  render() {
    const { title, subtitle, thumbnail, link, date } = this.props.entry;

    const contentData = {
      title,
      contents: subtitle,
      link
    };

    return (
      <StoryWidgetCardContainer>
        <Card>
          <div className="storyCard">
            {thumbnail ? (
              <img className="feedImage" src={thumbnail} />
            ) : (
              <img className="feedImage" src={NoImage} />
            )}
            <div className="tanggal">
              <Moment className="time" fromNow>
                {date}
              </Moment>
            </div>
            {this.renderContent(contentData)}
          </div>
        </Card>
      </StoryWidgetCardContainer>
    );
  }
}

StoryWidgetCard.propTypes = {
  entry: PropTypes.shape,
  profile: PropTypes.shape
};

import styled from "styled-components";

export const InterestCardComponents = styled.div`
  font-family: Rubik;
  width: 100%;
  position: relative;
  z-index: 1;
  margin-top: 3rem;

  p,
  h2 {
    margin: 0rem;
    font-weight: ${props => props.theme.weight.regular};
    color: ${props => props.theme.colors.primaryBlack};
  }

  p {
    font-size: 1.25rem;
  }

  .card-container {
    border-radius: 10px;
    background: white;
    box-shadow: ${props => props.theme.boxShadow};
    color: ${props => props.theme.colors.primaryBlack};
    display: flex;
    flex-direction: column;
    justify-content: center;
    padding: 1rem 2rem;
    margin-top: 1rem;
  }

  .line-separator {
    width: 100%;
    background-color: darkgray;
    margin: 0.5rem 0;
    height: 2px;
  }

  .flex-row {
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
  }

  .title-section {
    margin-top: 1rem;
  }

  .title-section-up {
    margin-top: 0.75rem;
  }

  .edit-button {
    display: flex;
    justify-content: flex-end;
    margin: 1rem 0;
  }

  @media (max-width: 1216px) {
    h2 {
      font-size: 20px;
    }
    .title-section,
    .title-section-up {
      font-size: 16px;
    }
  }
`;

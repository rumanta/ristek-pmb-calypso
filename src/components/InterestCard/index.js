import React, { Component } from "react";
import PropTypes from "prop-types";
import EmptyCard from "components/EmptyCard";
import { InterestCardComponents } from "./style";
import DetailInterest from "components/DetailInterest";
import { Buttons } from "../Buttons";
import { isEmpty } from "lodash";

class InterestCard extends Component {
  filterInterest = (interests, category) =>
    interests.filter(interest => {
      if (interest.interest_category === category) {
        return interest;
      }

      return null;
    });

  renderListOfInterest = (interests, category) => {
    if (isEmpty(interests)) {
      return (
        <EmptyCard
          passive={true}
          text={`You haven't added any ${category} interests`}
        />
      );
    }

    return interests.map((interest, index) => (
      <DetailInterest key={index}>{interest.interest_name}</DetailInterest>
    ));
  };

  render() {
    const { interests } = this.props;

    return (
      <InterestCardComponents>
        <h2>Interest</h2>
        <div className="card-container">
          <p className="title-section-up">Technology</p>
          <div className="line-separator" />
          <div className="flex-row">
            {this.renderListOfInterest(
              this.filterInterest(interests, "Technology"),
              "Technology"
            )}
          </div>
          <p className="title-section">Non-Tech</p>
          <div className="line-separator" />
          <div className="flex-row">
            {this.renderListOfInterest(
              this.filterInterest(interests, "Non-Tech"),
              "Non-Tech"
            )}
          </div>
          <p className="title-section">Entertainment</p>
          <div className="line-separator" />
          <div className="flex-row">
            {this.renderListOfInterest(
              this.filterInterest(interests, "Entertainment"),
              "Entertainment"
            )}
          </div>
          <div className="edit-button">
            <Buttons
              name="Edit"
              wide="8rem"
              type="button"
              onPressed={() => this.props.push("/edit-interest")}
            />
          </div>
        </div>
      </InterestCardComponents>
    );
  }
}

InterestCard.propTypes = {
  push: PropTypes.func,
  interests: PropTypes.object
};
export default InterestCard;

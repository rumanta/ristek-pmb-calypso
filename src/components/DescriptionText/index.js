import React, { Component } from "react";
import PropTypes from "prop-types";
import { DescriptionEditable, DescriptionStatic } from "./style";

class DescriptionText extends Component {
  render() {
    const { isEditable } = this.props;

    return (
      <React.Fragment>
        {isEditable === true ? (
          <DescriptionEditable
            onChange={evt => this.props.descriptionChange(evt)}
            value={this.props.textValue}
          />
        ) : (
          <DescriptionStatic readOnly />
        )}
      </React.Fragment>
    );
  }
}

DescriptionText.propTypes = {
  isEditable: PropTypes.bool,
  descriptionChange: PropTypes.func,
  textValue: PropTypes.string
};

export default DescriptionText;

import styled from "styled-components";

export const EventListCardStyle = styled.div`
  .EventListCard__card__header {
    margin-bottom: 1rem;
  }

  .EventListCard__card__header > p {
    margin-top: 0;
    margin-bottom: 0.25rem;

    :last-child {
      margin-bottom: 0;
    }
  }

  .EventListCard__card__title {
    margin-top: 0;
    margin-bottom: 1rem;
    color: ${props => props.theme.colors.blue};
    font-weight: ${props => props.theme.weight.bold};
    font-size: 24px;
  }

  .EventListCard__card__footer {
    display: flex;
    justify-content: flex-end;
    margin-top: 1rem;
  }

  .EventCard__card__date {
    display: flex;
    flex-direction: row;
    align-items: center;
    color: ${props => props.theme.colors.primaryBlack};
    margin-bottom: 0.5rem;

    .EventCard__card__date__icon {
      margin-right: 0.5rem;
    }
  }

  .info {
    font-weight: ${props => props.theme.weight.bold};
  }

  .EventCard__card__location {
    display: flex;
    flex-direction: row;
    align-items: center;
    color: ${props => props.theme.colors.primaryBlack};
    margin-bottom: 0.5rem;

    .EventCard__card__location__icon {
      margin-right: 0.5rem;
      height: 24px;
    }
  }

  p {
    font-size: 14px;
  }

  @media (max-width: 440px) {
    .EventListCard__card__title {
      font-size: 20px;
    }
  }

  @media (max-width: 360px) {
    .EventListCard__card__title {
      font-size: 18px;
    }

    p {
      font-size: 12px;
    }
  }
`;

import React, { Component } from "react";
import PropTypes from "prop-types";
import xss from "xss";

import Button from "../Button";
import Card from "../Card";
import CalendarIcon from "assets/calendar_1.svg";
import LocationIcon from "assets/icons/location-80.png";
import { timestampParser } from "../../utils/timestampParser";
import { EventListCardStyle } from "./style";

class EventListCard extends Component {
  render() {
    const title = this.props.title;
    const date = timestampParser(this.props.date);
    const place = this.props.place;
    const content = this.props.content;

    return (
      <EventListCardStyle>
        <div className="EventListCard">
          <Card>
            <div className="EventListCard__card">
              <div className="EventListCard__card__header">
                <h1 className="EventListCard__card__title">{title}</h1>
                <div className="EventCard__card__date">
                  <img
                    src={CalendarIcon}
                    alt="calendar icon"
                    className="EventCard__card__date__icon"
                  />
                  <p className="info">{date}</p>
                </div>

                <div className="EventCard__card__location">
                  <img
                    src={LocationIcon}
                    alt="location icon"
                    className="EventCard__card__location__icon"
                  />
                  <p className="info">{place}</p>
                </div>
              </div>
              <div className="EventListCard__card__content">
                <p dangerouslySetInnerHTML={{ __html: xss(content) }} />
              </div>
              <div className="EventListCard__card__footer">
                <Button text="See more" onClick={this.props.onClick} />
              </div>
            </div>
          </Card>
        </div>
      </EventListCardStyle>
    );
  }
}

EventListCard.propTypes = {
  title: PropTypes.string.isRequired,
  date: PropTypes.instanceOf(Date).isRequired,
  place: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired
};

export default EventListCard;

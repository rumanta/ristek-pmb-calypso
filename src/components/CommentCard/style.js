import styled from "styled-components";

export const CommentCardContainer = styled.div`
  .card-container {
    width: 100%;
    padding: 0.5rem 0;
    background: white;
    border-bottom: 1px solid ${props => props.theme.colors.gray};
    margin: 0.25rem 0;
  }

  p {
    margin: 0;
  }

  .picture-cont {
    width: 40px;
    height: 40px;
    border-radius: 50%;
    background-position: center;
    background-size: cover;
    border-width: 0.5px;
    border-color: ${props => props.theme.colors.black};
    border-style: solid;
  }

  .card-header {
    display: flex;
    flex-direction: row;
    align-items: center;
    margin-top: 0.25rem;
  }

  .person-cont {
    margin-left: 0.5rem;
  }

  .name-cont p {
    font-weight: ${props => props.theme.weight.bold};
    font-size: 14px;
  }

  .title-cont {
    margin-top: 0.25rem;
    color: ${props => props.theme.colors.gray};
    font-size: 11px;
  }

  .card-body {
    margin-top: 1rem;
    margin-bottom: 0.5rem;
    font-size: 14px;
  }

  @media (max-width: 460px) {
    .name-cont p {
      font-size: 12px;
    }

    .title-cont {
      font-size: 9px;
    }

    .card-body {
      font-size: 12px;
    }

    .picture-cont {
      width: 35px;
      height: 35px;
    }
  })
`;

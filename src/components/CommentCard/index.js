import React, { Component } from "react";
import xss from "xss";
import { CommentCardContainer } from "./style";
import PropTypes from "prop-types";
import PlaceholderPicture from "../../assets/placeholder_profile.png";
import { countPastTime } from "utils/countPastTime";

class CommentCard extends Component {
  render() {
    return (
      <CommentCardContainer>
        <div className="card-container">
          <div className="card-header">
            <div
              className="picture-cont"
              style={{
                backgroundImage: `url(${
                  this.props.picture ? this.props.picture : PlaceholderPicture
                })`
              }}
            />
            <div className="person-cont">
              <div className="name-cont">
                <p>{this.props.name}</p>
              </div>
              <div className="title-cont">
                {this.props.title}&nbsp; • &nbsp;
                {`${countPastTime(new Date(this.props.time))} ago`}
              </div>
            </div>
          </div>
          <div className="card-body">
            <p dangerouslySetInnerHTML={{ __html: xss(this.props.body) }} />
          </div>
        </div>
      </CommentCardContainer>
    );
  }
}

CommentCard.propTypes = {
  picture: PropTypes.string,
  name: PropTypes.string,
  title: PropTypes.string,
  body: PropTypes.string,
  time: PropTypes.string
};

export default CommentCard;

import React, { Component } from "react";
import PropTypes from "prop-types";
import Dropzone from "react-dropzone";
import Button from "../Button";
import isEmpty from "lodash/isEmpty";
import { SubmissionFieldContainer } from "./style";

class SubmissionField extends Component {
  constructor(props) {
    super(props);
    this.state = {
      file: {}
    };
  }

  onFileDrop = files => {
    this.setState({ file: files[0] });
  };

  onSubmit = () => {
    const { onSubmitFile } = this.props;
    const { file } = this.state;
    if (!isEmpty(file)) {
      onSubmitFile(file);
    }
  };

  render() {
    const { file } = this.state;

    return (
      <SubmissionFieldContainer>
        <Dropzone
          onDrop={this.onFileDrop}
          className="dropzone"
          multiple={false}
        >
          <div className="text-container">
            {!isEmpty(file) ? (
              <span>File submitted: {file.name}</span>
            ) : (
              <span>Drag file or click to upload</span>
            )}
          </div>
        </Dropzone>
        <div className="button-container">
          <Button text="Submit" onClick={() => this.onSubmit()} />
        </div>
      </SubmissionFieldContainer>
    );
  }
}

SubmissionField.propTypes = {
  onSubmitFile: PropTypes.func.isRequired
};

export default SubmissionField;

import styled from "styled-components";

export const SubmissionFieldContainer = styled.div`
  width: 100%;
  padding: 1rem 0;

  .dropzone {
    width: 100%;
    padding: 2rem 1rem;
    border-radius: 0.5rem;
    background: ${props => props.theme.colors.ashWhite};

    .text-container {
      display: flex;
      justify-content: center;
      align-items: center;
      font-size: 1.5rem;
      text-align: center;
      color: ${props => props.theme.colors.lightGrey};
    }
  }

  .button-container: {
    width: 100%;
    padding: 1rem 0;
    display: flex;
    justify-content: flex-end;
  }

  @media screen and (max-width: 64em) {
    .textContainer {
      font-size: 0.8rem;
    }
  }
`;

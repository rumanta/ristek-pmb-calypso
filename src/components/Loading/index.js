import React from "react";

import { LoadingContainer } from "./style";

class Loading extends React.Component {
  render() {
    return (
      <LoadingContainer>
        <div className="spinner" />
      </LoadingContainer>
    );
  }
}

export default Loading;

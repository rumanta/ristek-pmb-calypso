import styled from "styled-components";

export const DropdownStyle = styled.div`
  font-size: 14px;

  .Card {
    padding: 0.25rem 0.75rem;
  }

  select {
    min-width: 100%;
    width: fit-content;
    padding: 0.5rem;
    color: ${props => props.theme.colors.gray};
    padding-right: 2rem;
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    background: url(${props => props.dropdownIcon}) no-repeat center right
      0.5rem;
    border: none;
    cursor: pointer;
  }

  select:focus {
    outline: none;
  }

  @media (max-width: 480px) {
    font-size: 12px;
  }
`;

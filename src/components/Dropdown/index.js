import React, { Component } from "react";
import PropTypes from "prop-types";

import { DropdownStyle } from "./style";
import DropdownIcon from "../../assets/icons/dropdown.svg";
import Card from "../Card";

class Dropdown extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.props.onDropdownChange(event.target.value);
  }

  render() {
    const haveAnyAsOption = this.props.anyAsOption || false;

    return (
      <DropdownStyle dropdownIcon={DropdownIcon}>
        <div className="Dropdown">
          <Card>
            <select onChange={evt => this.handleChange(evt)} defaultValue="">
              <option value="" disabled hidden>
                {this.props.placeholder}
              </option>
              {haveAnyAsOption && (
                <option value="">
                  {this.props.anyOptionPlaceholder || "Any"}
                </option>
              )}
              {this.props.options.map(option => (
                <option value={option} key={option}>
                  {option}
                </option>
              ))}
            </select>
          </Card>
        </div>
      </DropdownStyle>
    );
  }
}

Dropdown.propTypes = {
  placeholder: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(PropTypes.string),
  onDropdownChange: PropTypes.func.isRequired,
  anyAsOption: PropTypes.bool,
  anyOptionPlaceholder: PropTypes.string
};

export default Dropdown;

import styled from "styled-components";

export const ProgressBarContainer = styled.div`
  .progress-bar {
    background: #c4c4c4;
    position: relative;
    height: 8px;
    width: 100%;
  }

  .filler {
    background: ${props => props.theme.colors.blue};
    height: 100%;
    border-radius: inherit;
    transition: width 0.2s ease-in;
  }
`;

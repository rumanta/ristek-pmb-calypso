import React from "react";
import PropTypes from "prop-types";

import { ProgressBarContainer } from "./style";

class ProgressBar extends React.Component {
  render() {
    const Filler = ({ percentage }) => (
      <div className="filler" style={{ width: `${percentage}%` }} />
    );

    return (
      <ProgressBarContainer>
        <div className="progress-bar">
          <Filler percentage={this.props.percentage} />
        </div>
      </ProgressBarContainer>
    );
  }
}

ProgressBar.propTypes = {
  percentage: PropTypes.number.isRequired
};

export default ProgressBar;

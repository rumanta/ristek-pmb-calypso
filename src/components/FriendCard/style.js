import styled from "styled-components";

export const FriendCardStyle = styled.div`
  border-radius: 10px;
  background: white;
  box-shadow: ${props => props.theme.boxShadow};
  color: ${props => props.theme.colors.primaryBlack};
  font-family: Rubik;
  width: 100%;
  position: relative;
  z-index: 1;
  margin-bottom: 2rem;

  .cardContainer {
    display: flex;
    flex-direction: column;
    justify-content: center;
    padding: 1rem;
  }

  .profileContainer {
    display: flex;
    flex-direction: row;
    justify-content: center;
  }

  .profileSection {
    display: flex;
    flex-direction: column;
    width: 100%;
    justify-content: center;
    align-items: flex-start;
    padding: 1rem 1rem 0rem 1rem;
  }

  .titleSection {
    width: 100%;
  }

  .titleSection h1,
  h3 {
    margin: 0rem;
  }

  .titleSection h1 {
    font-weight: ${props => props.theme.weight.bold};
    font-size: 24px;
  }

  .titleSection h3 {
    font-weight: ${props => props.theme.weight.regular};
    color: ${props => props.theme.colors.gray};
    font-size: 16px;
  }

  .statusSection {
    display: flex;
    flex-direction: row;
    align-items: flex-start;
  }

  .interestSection {
    padding: 1rem;
  }

  .status {
    margin-right: 1rem;
  }

  .detailsSection {
    display: flex;
    flex-direction: row;
    padding: 1rem;
  }

  .interestContainer {
    display: flex;
    flex-direction: column;
    padding: 0 1rem;
  }

  .descriptionContainer {
    display: flex;
    flex-direction: column;
    flex-wrap: wrap;
    padding: 0 1rem 1rem 1rem;

    p {
      word-wrap: normal;
      word-break: break-word;
    }
  }

  .borderSection {
    display: flex;
    flex-direction: row;
    padding: 1rem 0 0 0;
    border-top: 1px solid gray;
  }

  .detailsSection div {
    align-items: center;
    width: 100%;
  }

  .detail {
    width: 100%;
    display: flex;
    flex-direction: row;
    align-items: center;
    margin-bottom: 0.75rem;
  }

  .btn-space {
    padding: 0;
  }

  .detail .detailImage {
    margin-right: 0.75rem;
    width: 1.25rem;
  }

  .detailContent {
    margin-bottom: 0;
  }

  .detail p {
    font-size: 14px;
  }

  .buttonContainer {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: stretch;
    padding: 1rem;
  }

  .buttonContainer__left,
  .buttonContainer__right {
    display: flex;
  }

  .buttonContainer__left > * {
    margin-right: 0.5rem;

    &:last-of-type {
      margin-right: 0;
    }
  }

  .buttonContainer__right > * {
    margin-left: 0.5rem;

    &:first-of-type {
      margin-left: 0;
    }
  }

  .buttonSection {
    width: 50%;
    display: flex;
    flex-direction: row;
    align-items: flex-start;
  }

  .grey-text {
    color: ${props => props.theme.colors.gray};
    align-self: center;
  }

  .buttonSection2 {
    width: 50%;
    display: flex;
    flex-direction: row;
    justify-content: flex-end;

    & > * {
      margin-right: 1rem;

      &:last-of-type {
        margin-right: 0;
      }
    }
  }

  @media (max-width: 1216px) {
    .detail p {
      font-size: 12px;
    }

    .detail .detailImage {
      width: 1.25rem;
    }

    .titleSection h1 {
      font-size: 20px;
    }

    .titleSection h3 {
      font-size: 14px;
    }

    .profileSection h4 {
      font-size: 14px;
    }
  }

  @media (max-width: 1024px) {
    .cardContainer {
      flex-direction: column;
    }

    .titleSection {
      display: flex;
      flex-direction: column;
      align-items: center;
      margin-bottom: 1rem;
      text-align: center;
    }

    .profileContainer {
      flex-direction: column;
    }

    .profileSection {
      padding: 0;
      margin-bottom: 0;
    }

    .detailsSection {
      flex-direction: column;
      padding: 1rem;
    }

    .borderSection {
      flex-direction: column;
      padding: 0.5rem 0;
    }

    .buttonContainer {
      flex-direction: column;
      align-items: stretch;
    }

    .buttonContainer__left,
    .buttonContainer__right {
      flex-direction: column;
    }

    .buttonContainer__left > *,
    .buttonContainer__right > * {
      margin-left: 0;
      margin-right: 0;
      margin-bottom: 0.5rem;
    }

    .buttonSection {
      width: 100%;
      align-items: center;
    }

    .btn-space {
      padding: 0.15rem 0;
    }

    .buttonSection2 {
      width: 100%;
      align-items: center;
      display: flex;
      flex-direction: column;
      align-items: stretch;

      & > * {
        margin-right: 0;
        margin-bottom: 1rem;

        &:last-of-type {
          margin-bottom: 0;
        }
      }
    }

    .grey-text {
      margin-bottom: 1rem;
    }

    .descriptionContainer {
      p {
        font-size: 12px;
      }
    }

    .detail .detailImage {
      width: 1.5rem;
    }
  }

  @media (max-width: 580px) {
  }

  @media (max-width: 480px) {
    .titleSection h1 {
      font-size: 1.25rem;
    }

    .titleSection h3 {
      font-size: 0.75rem;
    }

    .detailsSection {
      padding: 0 0.5rem 1rem 0.5rem;
    }

    .detail {
      margin-bottom: 0.5rem;
    }

    .detail p {
      font-size: 0.8rem;
    }

    .descriptionContainer {
      padding: 0 0.5rem 1rem 0.5rem;

      p {
        font-size: 0.75rem;
      }
    }

    .detail .detailImage {
      width: 1rem;
    }
  }
`;

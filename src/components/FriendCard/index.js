/* eslint-disable complexity */
import React, { Component } from "react";
import PropTypes from "prop-types";
import swal from "sweetalert";
import { withRouter } from "react-router-dom";
import { compose } from "redux";
import { connect } from "react-redux";
import { push } from "connected-react-router";
import { FriendCardStyle } from "./style";

import { editApproveKenalan } from "../../containers/FormFriendPage/actions";
import { deleteKenalan } from "../../containers/FriendPage/actions";

import CakeIcon from "assets/icons/birthday.svg";
import BookIcon from "assets/icons/book.svg";
import SchoolIcon from "assets/icons/college.svg";
import EmailIcon from "assets/icons/email.svg";
import LineIcon from "assets/icons/line.svg";
import WebIcon from "assets/icons/web.svg";
import TechnologyIcon from "assets/icons/technology.svg";
import NontechIcon from "assets/icons/nontech.svg";
import EntertainmentIcon from "assets/icons/entertainment.svg";
import { Buttons } from "../Buttons";
import ProfilePicture from "../ProfilePicture";
import StatusInfo from "../StatusInfo";
import DescriptionText from "../DescriptionText";

class FriendCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status: this.props.friend ? this.props.friend.status : ""
    };
  }

  static action = {
    CAN_EDIT: "CAN_EDIT",
    CAN_APPROVE: "CAN_APPROVE",
    CAN_REJECT: "CAN_REJECT",
    CAN_DELETE: "CAN_DELETE"
  };

  static status = {
    NOT_FRIEND: "NOT_FRIEND",
    SAVED: "SAVED",
    PENDING: "PENDING",
    REJECTED: "REJECTED",
    ACCEPTED: "ACCEPTED"
  };

  static redirectLinks = {
    edit: id => `/formfriend/${id}`,
    reject: id => `/formfriend/${id}`
  };

  componentDidUpdate(prevProps) {
    if (prevProps.friend !== this.props.friend) {
      this.setState({ status: this.props.friend.status });
    }
  }

  renderListOfDetails = details =>
    details.map((detail, index) => (
      <div key={index} className="detail">
        <img className="detailImage" src={detail.icon} />
        <p className="detailContent">{detail.content}</p>
      </div>
    ));

  getTopInterests = interests => {
    const topInterests = {
      technology: null,
      nontech: null,
      entertainment: null
    };

    if (!interests) {
      return topInterests;
    }

    // eslint-disable-next-line array-callback-return
    interests.map(interest => {
      // eslint-disable-next-line no-unused-expressions
      interest.is_top_interest &&
        (topInterests[
          interest.interest_category.toLowerCase().replace(/-/g, "")
        ] = interest.interest_name);
    });

    return topInterests;
  };

  getAllowedActions = () => {
    const status = this.state.status;
    const isUserMaba = this.props.isUserMaba;
    const allowedActions = {};
    Object.values(FriendCard.action).forEach(
      action => (allowedActions[action] = false)
    );

    if (isUserMaba) {
      switch (status) {
        case FriendCard.status.SAVED:
          allowedActions[FriendCard.action.CAN_DELETE] = true;
          allowedActions[FriendCard.action.CAN_EDIT] = true;
          break;
        case FriendCard.status.REJECTED:
          allowedActions[FriendCard.action.CAN_EDIT] = true;
          break;
        default:
          break;
      }
    } else {
      switch (status) {
        case FriendCard.status.PENDING:
          allowedActions[FriendCard.action.CAN_APPROVE] = true;
          allowedActions[FriendCard.action.CAN_REJECT] = true;
          break;
        default:
          break;
      }
    }

    return allowedActions;
  };

  getButtons = () => {
    const friend = this.props.friend;
    const allowedActions = this.getAllowedActions();
    const buttons = {
      buttonsLeftSide: [],
      buttonsRightSide: []
    };
    const width = this.props.isDesktop ? "7.5rem" : "100%";
    if (allowedActions[FriendCard.action.CAN_DELETE]) {
      buttons.buttonsLeftSide.push(
        <Buttons
          key="delete"
          name="Remove"
          wide={width}
          type="button"
          delete={true}
          isDelete={true}
          onPressed={() => {
            swal(
              `Remove ${
                friend.user_elemen.profil.nama_lengkap
              } from friend list?`,
              {
                title: "Are you sure?",
                dangerMode: true,
                buttons: [true, "Remove"]
              }
            ).then(willAccept => {
              if (willAccept) {
                this.props.deleteKenalan(friend.id);
                swal(
                  "Success",
                  `You have removed ${
                    friend.user_elemen.profil.nama_lengkap
                  } from your friend list!`,
                  "success"
                );
              }
            });
          }}
        />
      );
    }
    if (allowedActions[FriendCard.action.CAN_REJECT]) {
      buttons.buttonsLeftSide.push(
        <Buttons
          key="reject"
          name="Reject"
          wide={width}
          type="button"
          delete={true}
          isDelete={true}
          onPressed={() =>
            this.props.push(FriendCard.redirectLinks.reject(friend.id))
          }
        />
      );
    }
    if (allowedActions[FriendCard.action.CAN_EDIT]) {
      buttons.buttonsRightSide.push(
        <Buttons
          key="edit"
          name="Edit"
          wide={width}
          type="button"
          onPressed={() =>
            this.props.push(FriendCard.redirectLinks.edit(friend.id))
          }
        />
      );
    }
    if (allowedActions[FriendCard.action.CAN_APPROVE]) {
      buttons.buttonsRightSide.push(
        <Buttons
          key="accept"
          name="Accept"
          wide={width}
          type="button"
          onPressed={() => {
            swal(`Accept ${friend.user_maba.profil.nama_lengkap} as friend?`, {
              title: "Ready to have a new friend?",
              buttons: [true, "Accept"]
            }).then(willAccept => {
              if (willAccept) {
                this.props.editApproveKenalan(friend.id, true, "");
                swal(
                  "Success",
                  `You are now friends with ${
                    friend.user_maba.profil.nama_lengkap
                  }!`,
                  "success"
                );
              }
            });
          }}
        />
      );
    }

    return buttons;
  };

  render() {
    if (!this.props.friend) {
      return <React.Fragment />;
    }
    // eslint-disable-next-line react/no-direct-mutation-state
    // this.state.status = this.props.friend.status;

    const friend = this.props.friend;
    const elemen = this.props.isUserMaba
      ? friend.user_elemen
      : friend.user_maba;
    const interestDecider = this.props.editable
      ? this.props.interests
      : elemen.interests;

    const {
      foto,
      nama_lengkap,
      jurusan,
      asal_sekolah,
      email,
      line_id,
      linkedin,
      tempat_lahir,
      tanggal_lahir
    } = elemen.profil;

    const { technology, nontech, entertainment } = this.getTopInterests(
      interestDecider
    );

    const { description, rejection_message } = friend;
    const status = this.state.status;
    const { title } = elemen;
    const { tahun } = elemen.profil.angkatan;

    const placeDateOfBirth = `${tempat_lahir}, ${tanggal_lahir}`;

    const leftData = [
      {
        icon: BookIcon,
        content: `${jurusan || "Fasilkom"} ${tahun || "-"}`
      },
      {
        icon: SchoolIcon,
        content: asal_sekolah || "-"
      },
      {
        icon: CakeIcon,
        content: placeDateOfBirth || "-"
      }
    ];

    const rightData = [
      {
        icon: EmailIcon,
        content: email || "-"
      },
      {
        icon: LineIcon,
        content: line_id || "-"
      },
      {
        icon: WebIcon,
        content: linkedin || "-"
      }
    ];

    const technologies = [
      {
        icon: TechnologyIcon,
        content: !technology ? "-" : technology
      }
    ];

    const nontechs = [
      {
        icon: NontechIcon,
        content: !nontech ? "-" : nontech
      }
    ];

    const entertainments = [
      {
        icon: EntertainmentIcon,
        content: !entertainment ? "-" : entertainment
      }
    ];

    const allowedActions = this.getAllowedActions(
      status,
      this.props.isUserMaba
    );

    const { buttonsLeftSide, buttonsRightSide } = this.props.showActionButtons
      ? this.getButtons(allowedActions)
      : {
          buttonsLeftSide: null,
          buttonsRightSide: null
        };

    const savedFriendsCountText = `Saved friends forms ${
      this.props.savedFriendsCount || this.props.savedFriendsCount === 0
        ? this.props.savedFriendsCount
        : "-"
    }/10`;

    return (
      <FriendCardStyle>
        <div className="cardContainer">
          <div className="profileContainer">
            <ProfilePicture src={foto} />
            <div className="profileSection">
              <div className="titleSection">
                <h1>{nama_lengkap}</h1>
                <h3>{title}</h3>
                <div className="statusSection">
                  <h4 className="status">Status:</h4>
                  <StatusInfo status={status} />
                </div>
              </div>
            </div>
          </div>
          <div className="detailsSection">
            <div className="leftDetails">
              {this.renderListOfDetails(leftData)}
            </div>
            <div className="rightDetails">
              {this.renderListOfDetails(rightData)}
            </div>
          </div>
          {this.props.isDesktop || this.props.editable ? (
            <React.Fragment>
              <div className="interestContainer">
                <h3>Top Interest:</h3>
                <div className="borderSection">
                  {this.renderListOfDetails(technologies)}
                  {this.renderListOfDetails(nontechs)}
                  {this.renderListOfDetails(entertainments)}
                </div>
              </div>
            </React.Fragment>
          ) : (
            <React.Fragment />
          )}
          <div className="descriptionContainer">
            <h3>Description:</h3>
            {this.props.editable &&
            (status === "NOT_FRIEND" ||
              status === "SAVED" ||
              status === "REJECTED") &&
            this.props.isEditMaba ? (
              <React.Fragment>
                <div className="borderSection">
                  <DescriptionText
                    isEditable
                    descriptionChange={this.props.descriptionChange}
                    textValue={this.props.textValue}
                  />
                </div>
              </React.Fragment>
            ) : (
              <React.Fragment>
                <div className="borderSection">
                  <p>{description || "-"}</p>
                </div>
              </React.Fragment>
            )}
          </div>
          <div className="descriptionContainer">
            {status === "REJECTED" ? (
              <React.Fragment>
                <h3 style={{ color: "red" }}>Rejection message:</h3>
                <div className="borderSection">
                  <p>
                    <i>{rejection_message || "-"}</i>
                  </p>
                </div>
              </React.Fragment>
            ) : status === "PENDING" &&
              !this.props.isEditMaba &&
              this.props.editable ? (
              <React.Fragment>
                <h3 style={{ color: "red" }}>Rejection message:</h3>
                <div className="borderSection">
                  <DescriptionText
                    isEditable
                    descriptionChange={this.props.descriptionChange}
                    textValue={this.props.textValue}
                  />
                </div>
              </React.Fragment>
            ) : null}
          </div>
          <div className="buttonContainer">
            {this.props.editable ? (
              <React.Fragment>
                <div className="buttonSection">
                  {this.props.isUserMaba ? (
                    <span className="grey-text">{savedFriendsCountText}</span>
                  ) : null}
                </div>
                <div className="buttonSection2">
                  {this.props.isUserMaba && status !== "ACCEPTED" && (
                    <Buttons
                      key={0}
                      name="Save"
                      wide={this.props.isDesktop ? "8rem" : "100%"}
                      type="button"
                      onPressed={this.props.onSave}
                    />
                  )}
                  {status !== "ACCEPTED" && (
                    <Buttons
                      key={1}
                      name="Send"
                      wide={this.props.isDesktop ? "8rem" : "100%"}
                      type="button"
                      style={{ marginLeft: "5rem" }}
                      onPressed={this.props.submit}
                    />
                  )}
                </div>
              </React.Fragment>
            ) : (
              <>
                {window.matchMedia("(max-width: 1024px)").matches ? (
                  <>
                    <div className="buttonContainer__right">
                      {buttonsRightSide}
                    </div>
                    <div className="btn-space" />
                    <div className="buttonContainer__left">
                      {buttonsLeftSide}
                    </div>
                  </>
                ) : (
                  <>
                    <div className="buttonContainer__left">
                      {buttonsLeftSide}
                    </div>
                    <div className="btn-space" />
                    <div className="buttonContainer__right">
                      {buttonsRightSide}
                    </div>
                  </>
                )}
              </>
            )}
          </div>
        </div>
      </FriendCardStyle>
    );
  }
}

FriendCard.defaultProps = {
  savedFriendsCount: "-"
};

FriendCard.propTypes = {
  friend: PropTypes.object,
  push: PropTypes.func,
  // editable refers to description box being editable, not edit button
  editable: PropTypes.bool,
  isDesktop: PropTypes.bool,
  isUserMaba: PropTypes.bool,
  showActionButtons: PropTypes.bool,
  interests: PropTypes.array,
  submit: PropTypes.func,
  onSave: PropTypes.func,
  textValue: PropTypes.string,
  descriptionChange: PropTypes.func,
  editApproveKenalan: PropTypes.func,
  deleteKenalan: PropTypes.func,
  isEditMaba: PropTypes.bool,
  savedFriendsCount: PropTypes.string || PropTypes.number
};

function mapDispatchToProps(dispatch) {
  return {
    push: url => dispatch(push(url)),
    editApproveKenalan: (id, is_approved, rejection_message) =>
      dispatch(editApproveKenalan(id, is_approved, rejection_message)),
    deleteKenalan: id => dispatch(deleteKenalan(id))
  };
}

export default compose(
  withRouter,
  connect(
    null,
    mapDispatchToProps
  )
)(FriendCard);

import styled from "styled-components";

export const SearchBarStyle = styled.div`
  font-size: 14px;

  .SearchBar__input {
    border: none;
    flex-grow: 1;
    height: 1.25rem;
  }

  .SearchBar__input::placeholder {
    color: ${props => props.theme.colors.gray};
  }

  .SearchBar__input:focus {
    outline: none;
  }

  .Card {
    display: flex;
    align-items: center;
  }

  .SearchBar__searchIcon {
    margin-right: 1rem;
  }

  @media (max-width: 480px) {
    font-size: 12px;
  }
`;

import styled from "styled-components";

export const InternalEventSectionContainer = styled.div`
  .event-wrap {
    margin: 1rem 0;
  }

  .logoset-container {
    display: none;
  }

  .title {
    font-size: 20px;
  }

  @media screen and (max-width: 1024px) {
    .EventCard__card__footer {
      justify-content: flex-end;
    }
  }

  @media (max-width: 600px) {
    .title {
      font-size: 16px;
    }
  }
`;

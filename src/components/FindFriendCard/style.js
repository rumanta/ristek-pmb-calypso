import styled from "styled-components";

export const FindFriendCardStyle = styled.div`
  border-radius: 10px;
  background: white;
  box-shadow: ${props => props.theme.boxShadow};
  color: ${props => props.theme.colors.primaryBlack};
  font-family: Rubik;
  width: 100%;
  position: relative;
  z-index: 1;
  margin-bottom: 2rem;

  .FindFriendCard {
    display: flex;
    flex-direction: column;
    justify-content: center;
    padding: 1rem;
  }

  .FindFriendCard__profile {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
  }

  .FindFriendCard__profile .imageSection {
    width: 5rem;
    height: 5rem;
  }

  .FindFriendCard__profile__statusInfo > * {
    margin-top: 0;
  }

  .FindFriendCard__details__row {
    display: flex;
  }

  .FindFriendCard__profile__name {
    padding-left: 0.5rem;
  }

  .FindFriendCard__profile__name-container {
    display: flex;
    align-items: center;
  }

  .FindFriendCard__profile__name h1,
  h3 {
    margin: 0rem;
  }

  .FindFriendCard__profile__name h1 {
    font-weight: ${props => props.theme.weight.bold};
    font-size: 1.75rem;
  }

  .FindFriendCard__profile__name h3 {
    font-weight: ${props => props.theme.weight.regular};
    color: ${props => props.theme.colors.gray};
    font-size: 1rem;
  }

  .FindFriendCard__details {
    display: flex;
    flex-direction: row;
    flex-direction: column;
    padding: 1rem;
  }

  .FindFriendCard__horizontal-rule {
    border-top: 1px solid black;
    margin: 0.5rem 0;
  }

  .FindFriendCard__details div {
    align-items: center;
    width: 100%;
  }

  .FindFriendCard__details__item {
    width: 100%;
    display: flex;
    flex-direction: row;
  }

  .FindFriendCard__details__item .FindFriendCard__details__item-icon {
    margin-right: 0.75rem;
    width: 1.25rem;
  }

  .FindFriendCard__details__content {
    margin-bottom: 0;
  }

  .FindFriendCard__details__item p {
    font-size: 1rem;
  }

  @media (max-width: 1216px) {
    .FindFriendCard__details__item p {
      font-size: 0.75rem;
    }

    .FindFriendCard__details__item .FindFriendCard__details__item-icon {
      width: 1.25rem;
    }

    .FindFriendCard__profile__name h1 {
      font-size: 1.5rem;
    }

    .FindFriendCard__profile__name h3 {
      font-size: 0.75rem;
    }

    .FindFriendCard__profile__name h4 {
      font-size: 0.75rem;
    }
  }

  @media (max-width: 1024px) {
    .FindFriendCard {
      flex-direction: column;
    }

    .FindFriendCard__profile__name {
      display: flex;
      flex-direction: column;
      align-items: center;
      margin-bottom: 1rem;
      text-align: center;
    }

    .FindFriendCard__profile__name-container {
      flex-direction: column;
      padding-bottom: 1rem;
    }

    .FindFriendCard__profile {
      flex-direction: column;
    }

    .FindFriendCard__profile__name {
      padding: 0;
      margin-bottom: 0;
    }

    .FindFriendCard__details__item p {
      font-size: 0.85rem;
    }

    .descriptionContainer {
      p {
        font-size: 0.85rem;
      }
    }

    .FindFriendCard__details__item .FindFriendCard__details__item-icon {
      width: 1.5rem;
    }
  }

  @media (max-width: 580px) {
  }

  @media (max-width: 480px) {
    .FindFriendCard__profile__name h1 {
      font-size: 1.25rem;
    }

    .FindFriendCard__profile__name h3 {
      font-size: 0.75rem;
    }

    .FindFriendCard__details__item {
      margin-bottom: 0.5rem;
    }

    .FindFriendCard__details__item p {
      font-size: 0.8rem;
    }

    .descriptionContainer {
      p {
        font-size: 0.75rem;
      }
    }

    .FindFriendCard__details__item .FindFriendCard__details__item-icon {
      width: 1rem;
    }
  }
`;

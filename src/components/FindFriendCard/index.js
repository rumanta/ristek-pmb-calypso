import React, { Component } from "react";
import PropTypes from "prop-types";
import { FindFriendCardStyle } from "./style";
import BookIcon from "assets/icons/book.svg";
import SchoolIcon from "assets/icons/college.svg";
import LineIcon from "assets/icons/line.svg";
import TechnologyIcon from "assets/icons/technology.svg";
import NontechIcon from "assets/icons/nontech.svg";
import EntertainmentIcon from "assets/icons/entertainment.svg";
import ProfilePicture from "../ProfilePicture";

class FindFriendCard extends Component {
  renderListOfDetails = details =>
    details.map((detail, index) => (
      <div key={index} className="FindFriendCard__details__item">
        <img
          className="FindFriendCard__details__item-icon"
          src={detail.icon}
          alt="profile picture image"
        />
        <p className="FindFriendCard__details__content">{detail.content}</p>
      </div>
    ));

  getTopInterests = interests => {
    const topInterests = {
      technology: null,
      nontech: null,
      entertainment: null
    };

    // eslint-disable-next-line array-callback-return
    interests.map(interest => {
      // eslint-disable-next-line no-unused-expressions
      interest.is_top_interest &&
        (topInterests[interest.interest_category.toLowerCase()] =
          interest.interest_name);
    });

    return topInterests;
  };

  render() {
    if (!this.props.friend) {
      return <React.Fragment />;
    }

    const friend = this.props.friend;
    const elemen = friend.user_detail;
    const {
      foto,
      nama_lengkap,
      jurusan,
      asal_sekolah,
      line_id
    } = elemen.profil;

    const { technology, nontech, entertainment } = this.getTopInterests(
      elemen.profil.interests
    );

    const { title } = elemen;
    const { tahun } = elemen.profil.angkatan;

    const topRowData = [
      {
        icon: BookIcon,
        content: `${jurusan || "-"}, ${tahun || "-"}`
      },
      {
        icon: SchoolIcon,
        content: asal_sekolah || "-"
      },
      {
        icon: LineIcon,
        content: line_id || "-"
      }
    ];

    const bottomRowData = [
      {
        icon: TechnologyIcon,
        content: !technology ? "-" : technology
      },
      {
        icon: NontechIcon,
        content: !nontech ? "-" : nontech
      },
      {
        icon: EntertainmentIcon,
        content: !entertainment ? "-" : entertainment
      }
    ];

    return (
      <FindFriendCardStyle>
        <div className="FindFriendCard">
          <div className="FindFriendCard__profile">
            <div className="FindFriendCard__profile__name-container">
              <ProfilePicture src={foto} />
              <div className="FindFriendCard__profile__name">
                <h1>{nama_lengkap}</h1>
                <h3>{title}</h3>
              </div>
            </div>
          </div>
          <div className="FindFriendCard__details">
            <div className="FindFriendCard__details__row">
              {this.renderListOfDetails(topRowData)}
            </div>
            <div className="FindFriendCard__horizontal-rule" />
            <div className="FindFriendCard__details__row">
              {this.renderListOfDetails(bottomRowData)}
            </div>
          </div>
        </div>
      </FindFriendCardStyle>
    );
  }
}

FindFriendCard.propTypes = {
  friend: PropTypes.object,
  isDesktop: PropTypes.bool
};

export default FindFriendCard;

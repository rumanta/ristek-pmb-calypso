/* eslint-disable no-confusing-arrow */
import React, { Component } from "react";
import PropTypes from "prop-types";
import Card from "components/Card";
import { StatisticsCardStyle, Progress, ProgressAngkatan } from "./style";

const propTypes = {
  quanta: PropTypes.shape({
    approved: PropTypes.number,
    total: PropTypes.number,
    required: PropTypes.number
  }),
  tarung: PropTypes.shape({
    approved: PropTypes.number,
    total: PropTypes.number,
    required: PropTypes.number
  }),
  omega: PropTypes.shape({
    approved: PropTypes.number,
    total: PropTypes.number,
    required: PropTypes.number
  }),
  alumni: PropTypes.shape({
    approved: PropTypes.number,
    total: PropTypes.number,
    required: PropTypes.number
  }),
  title: PropTypes.string,
  totalApproved: PropTypes.number,
  allTotal: PropTypes.number,
  mainRequirement: PropTypes.number
};

const defaultProp = {
  quanta: {
    approved: 0,
    total: 0,
    required: 0
  },
  tarung: {
    approved: 0,
    total: 0,
    required: 0
  },
  omega: {
    approved: 0,
    total: 0,
    required: 0
  },
  alumni: {
    approved: 0,
    total: 0,
    required: 0
  }
};

class StatisticsCard extends Component {
  colorPicker = num => (num >= 100 ? "green" : "red");

  render() {
    const { quanta, tarung, omega, alumni, title } = this.props;
    const allBatches = [quanta, tarung, omega, alumni];
    const [
      approvedQuanta,
      approvedTarung,
      approvedOmega,
      approvedAlumni
    ] = allBatches.map(info => info.approved);
    const [totalQuanta, totalTarung, totalOmega, totalAlumni] = allBatches.map(
      info => info.total
    );
    const [
      requireQuanta,
      requireTarung,
      requireOmega,
      requireAlumni
    ] = allBatches.map(info => info.required);
    const [
      percentageQuanta,
      percentageTarung,
      percentageOmega,
      percentageAlumni
    ] = allBatches.map(info =>
      info.required !== 0
        ? Math.round((info.approved * 100) / info.required)
        : 0
    );

    const { totalApproved, allTotal, mainRequirement } = this.props;

    // const [totalApproved, allTotal, mainRequirement] = [
    //   info => info.approved,
    //   info => info.total,
    //   info => info.required
    // ].map(getField =>
    //   [quanta, tarung, omega, alumni]
    //     .map(friendCountInfo => getField(friendCountInfo))
    //     .reduce((partialSum, nextValue) => partialSum + nextValue)
    // );

    return (
      <StatisticsCardStyle>
        {this.props.title !== null ? (
          <Card>
            <h3 className="mengenalText">
              {title}
              <span>
                {" "}
                ({totalApproved} Approved / {allTotal} Total / {mainRequirement}{" "}
                required)
              </span>
            </h3>
            <div className="Quanta progress">
              <h3>
                Progress Quanta :
                <span style={{ color: this.colorPicker(percentageQuanta) }}>
                  {" "}
                  {percentageQuanta}%{" "}
                </span>
                <AngkatanData
                  angkatanApproved={approvedQuanta}
                  angkatanTotal={totalQuanta}
                  angkatanRequirement={requireQuanta}
                />
              </h3>
              <ProgressBar percentage={percentageQuanta} />
            </div>
            <div className="tarung progress">
              <h3>
                Progress Tarung :{" "}
                <span style={{ color: this.colorPicker(percentageTarung) }}>
                  {percentageTarung}%{" "}
                </span>
                <AngkatanData
                  angkatanApproved={approvedTarung}
                  angkatanTotal={totalTarung}
                  angkatanRequirement={requireTarung}
                />
              </h3>
              <ProgressBar percentage={percentageTarung} />
            </div>
            <div className="omega progress">
              <h3>
                Progress Omega :
                <span style={{ color: this.colorPicker(percentageOmega) }}>
                  {" "}
                  {percentageOmega}%
                </span>
                <AngkatanData
                  angkatanApproved={approvedOmega}
                  angkatanTotal={totalOmega}
                  angkatanRequirement={requireOmega}
                />
              </h3>
              <ProgressBar percentage={percentageOmega} />
            </div>
            <div className="Alumni progress">
              <h3>
                Progress 2015-- :
                <span style={{ color: this.colorPicker(percentageAlumni) }}>
                  {" "}
                  {percentageAlumni}%
                </span>
                <AngkatanData
                  angkatanApproved={approvedAlumni}
                  angkatanTotal={totalAlumni}
                  angkatanRequirement={requireAlumni}
                />
              </h3>
              <ProgressBar percentage={percentageAlumni} />
            </div>
          </Card>
        ) : (
          <></>
        )}
      </StatisticsCardStyle>
    );
  }
}

const AngkatanData = props => (
  <ProgressAngkatan>
    <span>
      {" "}
      ({props.angkatanApproved} approved / {props.angkatanTotal} Total /{" "}
      {props.angkatanRequirement} required)
    </span>
  </ProgressAngkatan>
);

const ProgressBar = props => (
  <Progress>
    <div className="progress-bar">
      <div
        className="filler"
        style={{
          width: props.percentage <= 100 ? `${props.percentage}%` : "100%"
        }}
      />
    </div>
  </Progress>
);

StatisticsCard.propTypes = propTypes;
StatisticsCard.defaultProps = defaultProp;
AngkatanData.propTypes = {
  angkatanApproved: PropTypes.number,
  angkatanRequirement: PropTypes.number,
  angkatanTotal: PropTypes.number
};

ProgressBar.propTypes = {
  percentage: PropTypes.number
};

export default StatisticsCard;

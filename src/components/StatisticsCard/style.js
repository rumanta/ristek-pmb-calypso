import styled from "styled-components";

export const StatisticsCardStyle = styled.div`
  @import url("https://fonts.googleapis.com/css?family=Rubik:400,500,700&display=swap");

  .Card {
    padding-bottom: 2rem;
  }

  .mengenalText {
    display: flex;
    width: 100%;
    height: 40px;
    font-size: 1.5rem;
    font-weight: 400;
    font-family: "Rubik";
    color: black;
    padding: 0.5rem 0rem 0rem 1rem;
    padding-top: 0;
    margin: 0;

    br {
      display: none;
    }

    span {
      font-size: 1rem;
      padding: 0.4rem 0rem 0rem 0.5rem;
      color: ${props => props.theme.colors.blue};
    }
  }
  .progress {
    padding: 0rem 1rem 0rem 1rem;
    h3 {
      font-size: 1.1rem;
      font-weight: 400;
      font-family: "Rubik";
      color: black;
      margin-block-start: 0.75rem;
      margin-block-end: 0.75rem;
    }
  }

  @media (min-width: 1025px) and (max-width: 1349px) {
    .mengenalText {
      font-size: 1.5rem;

      span {
        font-size: 0.7rem;
      }
    }
    .progress {
      h3 {
        font-size: 1.1rem;
      }
    }
  }

  @media (max-width: 64rem) {
    .mengenalText {
      font-size: 1.5rem;
      display: block;
      padding: 0.5rem 0 1.75rem 0;
      text-align: center;

      br {
        display: block;
      }

      span {
        display: block;
        font-size: 0.75rem;
        padding: 0.5rem 0 1rem 0;
        text-align: center;
      }
      .progress {
        h3 {
          font-size: 0.8rem;
        }
      }
    }
  }

  @media (max-width: 500px) {
    .progress {
      padding: 0;
    }
  }
`;
export const Progress = styled.div`
  .progress-bar {
    position: relative;
    height: 0.5rem;
    width: 100%;
    border-radius: 50px;
    background: ${props => props.theme.colors.semiGray};
  }

  .filler {
    background: ${props => props.theme.colors.blue};
    height: 100%;
    border-radius: inherit;
  }

  @media (min-width: 1025px) and (max-width: 1349px) {
    .progress-bar {
      height: 0.5rem;
    }
  }

  @media (max-width: 64rem) {
    .progress-bar {
      height: 0.4rem;
    }
  }
`;

export const ProgressAngkatan = styled.span`
  span {
    font-size: 1rem;
    color: grey;
    display: inline;
  }

  @media (min-width: 1025px) and (max-width: 1349px) {
    span {
      font-size: 0.75rem;
    }
  }

  @media (max-width: 64rem) {
    span {
      display: block;
      padding: 0.15rem 0 -0.15rem 0;
      margin-bottom: -0.25rem;
      font-size: 0.75rem;
    }
  }
`;

import footerImage from "../../assets/desktop-footer-fade.png";
import styled from "styled-components";

export const FooterComponent = styled.div`
  .footer-background {
    width: 100%;
    height: 150px;
    background-image: url(${footerImage});
    background-color: ${props => props.theme.colors.boneWhite};
    background-repeat: no-repeat;
    background-position: center bottom;
    background-size: cover;

    @media (max-width: ${props => props.theme.sizes.desktop}) {
      height: 100px;
    }
  }
`;

import React, { Component } from "react";

import { FooterComponent } from "./style";

class Footer extends Component {
  render() {
    return (
      <FooterComponent>
        <div className="footer-background" />
      </FooterComponent>
    );
  }
}

export default Footer;

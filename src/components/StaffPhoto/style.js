import styled from "styled-components";

export const StaffComponents = styled.div`
  .department-label {
    background-color: ${props => props.theme.colors.white};
    width: 240px;
    height: 32px;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    padding: 0.5rem;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 20px;
    color: ${props => props.theme.colors.blue};
    font-weight: ${props => props.theme.weight.bold};
  }

  .staff-photo {
    margin: 2rem;
    width: 200px;
    height: 200px;
  }

  .staff-box {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    margin: 1.5rem;
  }

  @media screen and (max-width: 1024px) {
    .department-label {
      border-radius: 8px;
    }
  }
`;

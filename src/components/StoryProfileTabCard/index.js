import React from "react";
import { StoryProfileTabCardContainer } from "./style";
import { PropTypes } from "prop-types";
import Card from "components/Card";
import ProfilePicture from "components/ProfilePicture";
import Moment from "react-moment";
import EditIcon from "assets/icons/pencil-edit-button.svg";
import DeleteIcon from "assets/icons/rubbish-delete-button.svg";
import NoImage from "assets/no-image.png";
import swal from "sweetalert";

export default class StoryProfileTabCard extends React.Component {
  constructor(props) {
    super(props);
    this.deleteStory = this.deleteStory.bind(this);
  }

  renderContent = detail => (
    <div className="content">
      {detail.image ? (
        <img className="feedImage" src={detail.image} />
      ) : (
        <img className="feedImage" src={NoImage} />
      )}
      <div className="desc">{detail.contents}</div>
      <div className="urlSection">
        <a
          className="blog-link"
          target="_blank"
          rel="noopener noreferrer"
          href={detail.link}
        >
          ... More
        </a>
      </div>
    </div>
  );

  deleteStory() {
    swal({
      title: "Are you sure?",
      icon: "warning",
      buttons: true,
      dangerMode: true
    }).then(willDelete => {
      if (willDelete) {
        this.props.delete(this.props.entry.id);
      }
    });
  }

  render() {
    const { id, title, subtitle, thumbnail, link, date } = this.props.entry;

    const { profileImage, name } = this.props.profile;

    const contentData = {
      contents: subtitle,
      image: thumbnail,
      link
    };

    return (
      <StoryProfileTabCardContainer>
        <Card>
          <div className="profileSection">
            <ProfilePicture src={profileImage} />
            <div className="profileContent">
              <p className="nama">{name}</p>
              <Moment className="time" fromNow>
                {date}
              </Moment>
            </div>
            {this.props.isModifiable && (
              <div className="modifySection">
                <div
                  className="editSection"
                  onClick={() =>
                    this.props.push({
                      pathname: `/stories/edit/${id}`,
                      state: { link }
                    })
                  }
                >
                  <img className="editIcon" src={EditIcon} />
                </div>
                <div className="deleteSection" onClick={this.deleteStory}>
                  <img className="deleteIcon" src={DeleteIcon} />
                </div>
              </div>
            )}
          </div>
          <div className="titleSection">
            <h1>{title}</h1>
          </div>
          <div className="feed">{this.renderContent(contentData)}</div>
        </Card>
      </StoryProfileTabCardContainer>
    );
  }
}

StoryProfileTabCard.propTypes = {
  entry: PropTypes.shape,
  profile: PropTypes.shape,
  isModifiable: PropTypes.bool,
  push: PropTypes.func.isRequired,
  delete: PropTypes.func.isRequired
};

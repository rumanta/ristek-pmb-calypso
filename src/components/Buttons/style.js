/* eslint-disable no-confusing-arrow */
import styled from "styled-components";

export const ButtonsContainer = styled.div`
@import url("https://fonts.googleapis.com/css?family=Rubik:400,500,700&display=swap");

.button {
    width: ${props => props.wide};
    height: 2.2rem;
    border: 2px solid ${props =>
      props.delete ? props.theme.colors.deleteRed : props.theme.colors.blue};
    box-sizing: border-box;
    border-radius: 5px
    background: #FFFFFF;
    line-height: 19px;
    display: flex;
    align-items: center;
    text-align: center;
    flex-direction: row;
    justify-content: center;
    font-style: normal;
    font-weight: ${props => props.theme.weight.regular};
    font-size: 14px;
    color: #333333;
    transition: 0.3s;
}

.button:hover {
  background: ${props =>
    props.delete ? props.theme.colors.deleteRed : props.theme.colors.blue};
  color: #FFFFFF;
}

.button:focus {
  outline: 0;
}

.delete {
  border-color: ${props => props.theme.colors.redBorder}
}

@media only screen and (max-width: 1280px) {
  .button {
    height: 2rem;
    font-size: 14px;
  }
}

@media (max-width: 580px) {
  .button {
    font-size: 0.75rem;
  }
}

@media (max-width: 480px) {
  .button {
    height: 1.5rem;
    /* font-size: 0.6rem; */
    border-width: thin;
  }
}

`;

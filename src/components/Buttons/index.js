import React, { Component } from "react";
import { ButtonsContainer } from "../Buttons/style";
import PropTypes from "prop-types";

export class Buttons extends Component {
  render() {
    let classNames = "button";
    if (this.props.isDelete) {
      classNames += " delete";
    }

    return (
      <ButtonsContainer delete={this.props.delete} wide={this.props.wide}>
        <button
          type={this.props.type}
          className={classNames}
          onClick={this.props.onPressed}
        >
          {this.props.name}
        </button>
      </ButtonsContainer>
    );
  }
}

Buttons.propTypes = {
  delete: PropTypes.bool,
  wide: PropTypes.string,
  type: PropTypes.string,
  name: PropTypes.string,
  isDelete: PropTypes.bool,
  onPressed: PropTypes.func
};

export default Buttons;

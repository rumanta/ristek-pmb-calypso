/* eslint-disable global-require */
import React, { Component } from "react";
import PropTypes from "prop-types";
import xss from "xss";

import Card from "components/Card";
import CalendarIcon from "assets/calendar_1.svg";
import LocationIcon from "assets/icons/location-80.png";
import AttachmentIcon from "assets/attachment_icon.svg";
import Spinner from "components/Loading";
import { timestampParser } from "../../utils/timestampParser";

import Button from "../Button";
import { EventDetailCard } from "./style";

class DetailCard extends Component {
  render() {
    // const title = this.props.title;
    const date = timestampParser(new Date(this.props.date));
    // const type = this.props.type;
    // const location = this.props.location;
    // const description = this.props.description;
    // const attachmentLink = this.props.attachmentLink;
    const {
      title,
      type,
      location,
      description,
      attachmentLink,
      organizer,
      coverImage,
      organizerImage
    } = this.props;

    return this.props.isLoading ? (
      <Spinner />
    ) : (
      <EventDetailCard image={coverImage} logo={organizerImage}>
        <div className="EventCard">
          <Card>
            <div className="back-button-wrap">
              <Button text="<<< Back" onClick={this.props.onClick} />
            </div>

            <div className="EventCard__card">
              {this.props.coverImage ? (
                <div className="EventCard__card__image" />
              ) : (
                ""
              )}

              <div className="EventCard__card__content">
                <div className="row">
                  <h1>{title}</h1>

                  {type === "event" &&
                    (organizer ? (
                      <div className="desktop logoset-container">
                        <div className="EventCard__card__logo" />
                        <div className="logoset-description">
                          <p>{organizer}</p>
                        </div>
                      </div>
                    ) : (
                      ""
                    ))}
                </div>

                {type === "event" && (
                  <React.Fragment>
                    <div className="EventCard__card__date">
                      <img
                        src={CalendarIcon}
                        alt="calendar icon"
                        className="EventCard__card__date__icon"
                      />
                      <p className="info">{date}</p>
                    </div>

                    <div className="EventCard__card__location">
                      <img
                        src={LocationIcon}
                        alt="location icon"
                        className="EventCard__card__location__icon"
                      />
                      <p className="info">{location}</p>
                    </div>
                  </React.Fragment>
                )}

                {type === "task" && (
                  <React.Fragment>
                    <div className="EventCard__card__date">
                      <div className="task-deadline-label">
                        Deadline: <b>{date}</b>
                      </div>
                    </div>
                  </React.Fragment>
                )}

                {type === "announcement" && (
                  <React.Fragment>
                    <div className="EventCard__card__date gray">
                      <div className="announcement-deadline-label">
                        Announced: <b>{date}</b>
                      </div>
                    </div>
                  </React.Fragment>
                )}

                <div className="EventCard__card__text">
                  <p dangerouslySetInnerHTML={{ __html: xss(description) }} />
                </div>
              </div>
              <div className="EventCard__card__footer">
                {attachmentLink ? (
                  <div className="attachment-container">
                    <img
                      src={AttachmentIcon}
                      alt="attachment icon"
                      className="EventCard__card__attachment__icon"
                    />
                    <a href={attachmentLink}>attachment</a>
                  </div>
                ) : null}
                {type === "event" &&
                  (organizer ? (
                    <div className="mobile logoset-container">
                      <div className="EventCard__card__logo" />
                      <div className="logoset-description">
                        <p>{organizer}</p>
                      </div>
                    </div>
                  ) : (
                    ""
                  ))}
              </div>
            </div>
          </Card>
        </div>
      </EventDetailCard>
    );
  }
}

DetailCard.defaultProps = {
  push: PropTypes.func.isRequired
};

DetailCard.propTypes = {
  title: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  coverImage: PropTypes.string,
  description: PropTypes.string,
  location: PropTypes.string,
  organizer: PropTypes.string,
  organizerImage: PropTypes.string,
  push: PropTypes.func,
  type: PropTypes.string,
  attachmentLink: PropTypes.string,
  onClick: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired
};

export default DetailCard;

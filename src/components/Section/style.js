import styled from "styled-components";

export const SectionStyle = styled.div`
  .Section {
    margin-bottom: 1rem;
  }

  .Section__title {
    font-size: 20px;
    margin-bottom: 1rem;
    text-transform: capitalize;
    color: ${props => props.theme.colors.primaryBlack};
  }
`;

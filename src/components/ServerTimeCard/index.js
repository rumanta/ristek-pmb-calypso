import React, { Component } from "react";
import { ServerTimeCardStyle } from "./style";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import getLocalTime from "utils/localTime";

import Card from "../Card";
import moment from "moment-timezone";
import { getTime, isLoadingServerTime } from "selectors/user";

class ServerTimeCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      time: getLocalTime()
    };
  }

  componentDidMount() {
    this.timer = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  tick = () => {
    this.setState({
      time: getLocalTime()
    });
  };

  render() {
    const timeString = moment(this.state.time).format("HH:mm:ss");
    const dateString = moment(this.state.time).format("dddd, D MMMM YYYY");

    return (
      <ServerTimeCardStyle>
        <div className="ServerTimeCard">
          <Card>
            <div className="ServerTimeCard__title">Server Time</div>
            <div className="ServerTimeCard__time">
              {this.state.time ? timeString : "-------"}
            </div>
            <div className="ServerTimeCard__date">
              {this.state.time ? dateString : "-------"}
            </div>
          </Card>
        </div>
      </ServerTimeCardStyle>
    );
  }
}

ServerTimeCard.propTypes = {
  serverTime: PropTypes.string,
  isServerTimeLoading: PropTypes.bool.isRequired
};

function mapStateToProps(state) {
  return {
    serverTime: getTime(state),
    isServerTimeLoading: isLoadingServerTime(state)
  };
}

export default connect(
  mapStateToProps,
  null
)(ServerTimeCard);

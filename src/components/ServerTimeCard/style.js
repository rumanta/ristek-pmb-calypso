import styled from "styled-components";

export const ServerTimeCardStyle = styled.div`
  .ServerTimeCard__title {
    margin-bottom: 0.5rem;
    color: ${props => props.theme.colors.gray};
  }

  .ServerTimeCard__time {
    color: ${props => props.theme.colors.black};
    font-weight: ${props => props.theme.weight.bold};
    font-size: 3rem;

    @media (max-width: ${props => props.theme.sizes.widescreen}) {
      font-size: 2rem;
    }
  }

  .ServerTimeCard__date {
    color: ${props => props.theme.colors.darkGray};
  }
`;

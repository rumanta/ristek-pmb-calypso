import React from "react";
import { NotFoundPageStyle } from "./style";
export default class NotFoundPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fixedText: "404 Not Found",
      text: "",
      charIndex: 0,
      speed: 200
    };
  }

  componentDidMount() {
    this.handleType();
  }

  handleType = () => {
    const { text, speed, fixedText, charIndex } = this.state;
    this.setState({
      text: fixedText.substring(0, text.length + 1)
    });

    if (text === "") {
      this.setState({
        charIndex: charIndex + 1
      });
    }

    setTimeout(this.handleType, speed);
  };

  render() {
    return (
      <NotFoundPageStyle>
        <div className="NotFoundPage">
          <h1 className="NotFoundPage__header">
            {this.state.text}
            <span className="NotFoundPage__cursor" />
          </h1>
          <div className="NotFoundPage__text">
            Are you lost? Just open this <a href="/">door</a> that leads to a
            new world.
          </div>
        </div>
      </NotFoundPageStyle>
    );
  }
}

import styled from "styled-components";

export const NotFoundPageStyle = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  justify-content: center;

  .NotFoundPage__header {
    font-size: 5rem;
    font-weight: normal;
    margin-block-start: 0;
    margin-block-end: 0.2em;
    letter-spacing: -2px;

    @media (max-width: ${props => props.theme.sizes.desktop}) {
      font-size: 3rem;
    }
  }

  .NotFoundPage__text {
    font-size: 1.5rem;

    a {
      color: ${props => props.theme.colors.blue};
    }

    @media (max-width: ${props => props.theme.sizes.desktop}) {
      font-size: 1rem;
    }
  }

  .NotFoundPage__cursor {
    height: 5.5rem;
    display: inline-block;
    border-left: 0.085rem solid white;
    animation: blink 0.7s steps(1) infinite;
    background: black;

    @media (max-width: ${props => props.theme.sizes.desktop}) {
      height: 3.25rem;
    }
  }

  @keyframes blink {
    50% {
      border-color: transparent;
    }
  }
`;

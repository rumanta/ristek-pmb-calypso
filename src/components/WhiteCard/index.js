import React, { Component } from "react";

import { WhiteCardComponents } from "./style";
import PropTypes from "prop-types";

class WhiteCard extends Component {
  render() {
    return (
      <WhiteCardComponents>
        <div className="white-card"> {this.props.children} </div>
      </WhiteCardComponents>
    );
  }
}
WhiteCard.propTypes = { children: PropTypes.node };
export default WhiteCard;

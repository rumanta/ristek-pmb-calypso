import React, { Component } from "react";
import { ButtonContainer } from "./style";
import PropTypes from "prop-types";

class Button extends Component {
  render() {
    return (
      <ButtonContainer>
        <div
          className="detail-btn"
          onClick={this.props.onClick ? () => this.props.onClick() : ""}
        >
          <p>{this.props.text}</p>
        </div>
      </ButtonContainer>
    );
  }
}

Button.propTypes = {
  text: PropTypes.string,
  onClick: PropTypes.func
};

export default Button;

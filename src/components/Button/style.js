import styled from "styled-components";

export const ButtonContainer = styled.div`
  .detail-btn {
    border-style: solid;
    border-width: 1px;
    color: ${props => props.theme.colors.primaryBlack};
    border-color: ${props => props.theme.colors.blue};
    border-radius: 4px;
    text-align: center;
    padding: 0.3rem;
    width: 5.5rem;
    font-size: 14px;
    cursor: pointer;
  }

  p {
    margin: 0;
  }

  @media (max-width: 1280px) {
    p {
      font-size: 14px;
    }
  }

  @media (max-width: 880px) {
    p {
      font-size: 12px;
    }

    .detail-btn {
      width: 4rem;
    }
  }

  @media (max-width: 440px) {
    p {
      font-size: 11px;
    }
  }

  @media (max-width: 360px) {
    p {
      font-size: 11px;
    }

    .detail-btn {
      width: 3.5rem;
    }
  }
`;

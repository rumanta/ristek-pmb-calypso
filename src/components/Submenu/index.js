import React, { Component } from "react";
import Tab from "react-bootstrap/Tab";
import Tabs from "react-bootstrap/Tabs";
import { SubmenuContainer } from "./style";
import { ProfilePageContainer } from "../../containers/ProfilePage/style";
import ProfileTab from "../../containers/ProfileTab/index";
import ProfileImage from "../../assets/profile.png";

class ControlledTabs extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      key: "profile"
    };
  }

  render() {
    const profile = {
      profileImage: ProfileImage,
      name: "Muhammad Fathurziki Herlando",
      position: "Wakil PJ HPK PMB Fasilkom UI 2019",
      major: "Ilmu Komputer 2019",
      highSchool: "SMAN 26 Jakarta",
      placeDateOfBirth: "Bandung, 17-07-2000",
      email: "muhfathurz@gmail.com",
      line: "mufalandoWibu",
      linkedin: "linkedin.com/mufalando"
    };

    return (
      <SubmenuContainer>
        <Tabs
          id="controlled-tab-example"
          activeKey={this.state.key}
          onSelect={key => this.setState({ key })}
        >
          <Tab eventKey="profile" title="Profile">
            <ProfilePageContainer>
              <ProfileTab profile={profile} />
            </ProfilePageContainer>
          </Tab>
          <Tab eventKey="feedback" title="Feedback">
            feedback
          </Tab>
          <Tab eventKey="story" title="Story">
            story
          </Tab>
        </Tabs>
      </SubmenuContainer>
    );
  }
}
export default ControlledTabs;

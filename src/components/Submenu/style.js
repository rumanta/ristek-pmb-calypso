import styled from "styled-components";

export const SubmenuContainer = styled.div`
  @import url("https://fonts.googleapis.com/css?family=Rubik:400,500,700&display=swap");

  .nav-tabs {
    border-bottom: 2px solid #1d4266;
  }
  .nav-tabs .nav-link {
    border: 0px;
    border-top-left-radius: 0rem;
    border-top-right-radius: 0rem;
    border-right: 1px solid #1d4266;
    width: 10rem;
    justify-content: center;
  }

  a {
    color: #1d4266;
    font-family: Rubik;
    font-style: normal;
    font-weight: ${props => props.theme.weight.bold};
    font-size: 2.75vh;
    text-align: center;
  }

  .nav-tabs .nav-item.show .nav-link,
  .nav-tabs .nav-link.active {
    color: #ffffff;
    background-color: #1d4266;
  }
`;

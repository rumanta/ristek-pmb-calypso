import styled from "styled-components";

export const PhotoProfileContainer = styled.div`
  display: flex;
  padding: 0.75rem;
  justify-content: center;

  .imageSection {
    width: 6.5rem;
    height: 6.5rem;
    overflow: hidden;
    border-radius: 50%;
    border-style: solid;
    border-color: ${props => props.theme.colors.blue};
    border-width: medium;
  }

  .imageSection img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }

  @media (max-width: 1216px) {
    .imageSection {
      width: 6.5rem;
      height: 6.5rem;
    }
  }

  @media (max-width: 1024px) {
    .imageSection {
      display: flex;
      justify-content: center;
      align-items: center;
    }

    .imageSection {
      width: 10rem;
      height: 10rem;
    }
  }

  @media (max-width: 480px) {
    padding: 0.75rem 0;
    .imageSection {
      width: 7.5rem;
      height: 7.5rem;
    }
  }
`;

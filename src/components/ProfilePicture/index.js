import React from "react";
import PropTypes from "prop-types";
import placeholderPhotoProf from "assets/icons/placeholderPhotoProf.png";
import { PhotoProfileContainer } from "./style";
class PhotoProfile extends React.Component {
  render() {
    return (
      <PhotoProfileContainer>
        <div className="imageSection">
          <img
            src={this.props.src ? this.props.src : placeholderPhotoProf}
            alt="Image Error"
          />
        </div>
      </PhotoProfileContainer>
    );
  }
}

PhotoProfile.propTypes = { src: PropTypes.string };

PhotoProfile.defaultProps = { src: "" };

export default PhotoProfile;

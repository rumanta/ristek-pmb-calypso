import styled from "styled-components";

export const AnnouncementListCardContainer = styled.div`
  .card-container {
    position: relative;
    z-index: 1;
    padding: 1rem;
    background: white;
    border-radius: 7px;
    box-shadow: ${props => props.theme.boxShadow};
    margin-bottom: 1rem;
  }

  .date-container {
    display: flex;
    align-items: center;
    margin: 1rem 0;
    color: ${props => props.theme.colors.gray};
  }

  .date {
    margin-left: 0.5rem;
  }

  p {
    font-size: 14px;
  }

  h1 {
    color: ${props => props.theme.colors.blue};
    font-size: 24px;
  }

  .btn-container {
    display: flex;
    justify-content: flex-end;
    margin-top: 1.5rem;
  }

  @media (max-width: 1280px) {
    img {
      width: 20px;
    }
  }

  @media (max-width: 880px) {
    .btn-container {
      margin-top: 1rem;
    }
  }

  @media (max-width: 440px) {
    h1 {
      font-size: 20px;
    }

    .btn-container {
      margin-top: 0;
    }
  }

  @media (max-width: 360px) {
    h1 {
      font-size: 18px;
    }

    .date-container {
      margin-top: 0.5rem;
    }

    img {
      width: 16px;
    }

    p {
      font-size: 12px;
    }
  }
`;

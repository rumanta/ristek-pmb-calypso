import React, { Component } from "react";
import PropTypes from "prop-types";
import { AnnouncementListCardContainer } from "./style";
import CalendarIcon from "../../assets/calendar_2.svg";
import { timestampParser } from "../../utils/timestampParser";
import Button from "../Button";

class AnnouncementListCard extends Component {
  render() {
    const { title, date, onClick } = this.props;

    return (
      <AnnouncementListCardContainer>
        <div className="card-container">
          <h1>{title}</h1>
          <div className="date-container">
            <img src={CalendarIcon} />
            <p className="date">{timestampParser(new Date(date))}</p>
          </div>
          <div className="btn-container">
            <Button text="Details" onClick={onClick} />
          </div>
        </div>
      </AnnouncementListCardContainer>
    );
  }
}

AnnouncementListCard.propTypes = {
  date: PropTypes.string,
  title: PropTypes.string,
  onClick: PropTypes.func
};

export default AnnouncementListCard;

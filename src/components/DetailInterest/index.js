import React, { Component } from "react";
import { DetailInterestComponents } from "./style";
import PropTypes from "prop-types";

class DetailInterest extends Component {
  render() {
    return (
      <DetailInterestComponents>
        <div className="interest-detail">
          <p>{this.props.children}</p>
        </div>
      </DetailInterestComponents>
    );
  }
}
DetailInterest.propTypes = { children: PropTypes.string };
export default DetailInterest;

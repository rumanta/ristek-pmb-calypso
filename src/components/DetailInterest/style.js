import styled from "styled-components";

export const DetailInterestComponents = styled.div`
  .interest-detail {
    background-color: lightgray;
    border-radius: 10px;
    padding: 0.5rem 1rem;
    margin: 0.5rem 1rem 0.5rem 0;
  }

  p {
    font-size: 14px;
  }

  @media (max-width: 880px) {
    p {
      font-size: 12px;
    }
  }
`;

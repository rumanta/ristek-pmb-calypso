import React, { Component } from "react";
import PropTypes from "prop-types";

import { CardStyle } from "./style";

class Card extends Component {
  render() {
    return (
      <CardStyle>
        <div className="Card">{this.props.children}</div>
      </CardStyle>
    );
  }
}

Card.propTypes = {
  children: PropTypes.node.isRequired
};

export default Card;

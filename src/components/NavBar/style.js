import styled from "styled-components";

export const NavBarStyle = styled.div`
  @import url("https://fonts.googleapis.com/css?family=Rubik:400,500,700&display=swap");
  background: ${props => props.theme.colors.boneWhite};

  a {
    cursor: pointer;
  }

  .link-ristek {
    margin-top: 0.3rem;
  }

  .image-logo {
    display: flex;
    justify-content: center;
    ${props =>
      !props.isDesktop &&
      `
    justify-content: flex-start;
  `}
    flex: 0.75;
    margin: 0 1rem;
  }

  .ristek-logo,
  .pmb-logo {
    display: flex;
    align-self: center;
    cursor: pointer;
  }

  .icon-ristek,
  .icon-pmb {
    height: auto;
    width: auto;
    max-width: 2.5rem;
    max-height: 2.5rem;
    margin-right: 1rem;
  }

  .icon-pmb {
    max-width: 7.25rem;
    max-height: 3.25rem;
  }

  .navbar-profile {
    display: flex;
    justify-content: flex-end;
    flex: 0.75;
    overflow: visible;
    height: 3.5rem;
    width: 100%;
    margin: 0 1rem;

    .notification {
      display: flex;
      align-self: center;
      cursor: pointer;
    }
    .notification-image {
      height: auto;
      width: auto;
      max-width: 2rem;
      max-height: 2rem;
    }

    .profile-container {
      display: flex;
      justify-content: center;
    }
    .profile-image {
      align-self: center;
      width: 2.5rem;
      height: 2.5rem;
      border-radius: 50%;
      margin: 0 1rem;
      background-position: center;
      background-size: cover;
      border-width: 0.5px;
      border-color: ${props => props.theme.colors.black};
      border-style: solid;
    }

    .dropdown-profile {
      width: 20%;
      z-index: 10;
    }

    .dropdown-profile a {
      width: 100%;
      margin: 0;
      padding: 1rem 0;
      display: block;
      color: #222;
      text-align: left;
      font-size: 1.25rem;
    }

    .drop-menu-profile {
      width: 8rem;
      transform: translate(-90%, 0);

      a {
        font-family: "Rubik";
        font-size: 1rem;
        font-weight: ${props => props.theme.weight.regular};
        text-align: left;
        vertical-align: middle;
        padding: 0.5rem;
      }
    }
  }

  * {
    box-sizing: border-box;
    list-style-type: none;
    padding: 0;
    margin: 0;
    text-decoration: none;
  }

  .header-container {
    width: 100%;
    display: flex;
    align-items: center;
  }

  .main-menu {
    width: 100%;
    margin: 0 1rem;
    height: 3.5rem;
    display: flex;
    flex-direction: row;
    justify-content: space-around;
    overflow: visible;
    flex: 3;
  }

  .main-menu a {
    width: 100%;
    text-align: center;
    padding: 1rem 1rem;
    display: block;
    color: #222;
    font-family: "Rubik";
    font-size: 1.25rem;
    font-weight: ${props => props.theme.weight.bold};
    text-align: center;
    vertical-align: middle;
  }

  .dropdown-image {
    height: auto;
    width: auto;
    max-width: 1.25rem;
    max-height: 1.25rem;
    margin-right: 1rem;
    z-index: 10;
  }

  .drop-menu {
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 200%;
    transition: max-height 0.4s, opacity 0.5s;
    max-height: 0;
    opacity: 0;
    overflow: hidden;
    position: relative;
    z-index: 10;
  }

  .drop-menu a {
    font-family: "Rubik";
    font-size: 1rem;
    font-weight: ${props => props.theme.weight.regular};
    text-align: left;
    vertical-align: middle;
    padding: 0.5rem;
  }

  .drop-menu a:first-child {
    margin-top: 1rem;
    border-top-left-radius: 10px;
    border-top-right-radius: 10px;
    border-top: 1.25px solid ${props => props.theme.colors.darkGray};
  }
  .drop-menu a:last-child {
    border-bottom-left-radius: 10px;
    border-bottom-right-radius: 10px;
    border-bottom: 1.25px solid ${props => props.theme.colors.darkGray};

    img {
      max-width: 1rem;
      max-height: 1rem;
    }
  }

  .drop-menu a:not(:last-child) {
    border-bottom: 1.25px solid ${props => props.theme.colors.darkGray};
  }

  .drop-menu > .menu-button {
    height: 100%;
    transition: transform 0.4s;
    transform: translateY(-200%);
    background: white;
    border-right: 1.25px solid ${props => props.theme.colors.darkGray};
    border-left: 1.25px solid ${props => props.theme.colors.darkGray};
    box-shadow: 0px 1px 1px 1px rgba(0, 0, 0, 0.15);
  }

  .dropdown-wrapper:hover .drop-menu {
    max-height: 300px;
    opacity: 1;
  }

  .dropdown-wrapper:hover > .drop-menu .menu-button {
    transform: translateY(0%);
  }

  .Black__Background {
    position: absolute;
    left: 50%;
    top: 45%;
    transform: translate(-50%, -50%);
    background-color: rgba(0, 0, 0, 0.7);
    min-width: 100%;
    min-height: 100%;
    z-index: 9999999999999;
    display: flex;
    justify-content: center;
    align-items: center;

    .PopUp__Container {
      display: flex;
      padding-top: 3rem;
      align-items: flex-start;
      justify-content: center;
    }
  }

  @media screen and (max-width: 69rem) {
    .header-container {
      height: 2.95rem;
    }

    .navbar-profile {
      ${props => props.isDesktop && `justify-content: center;`}
      margin: 0.5rem;

      .notification-image {
        max-width: 1.75rem;
        max-height: 1.75rem;
        margin-right: 0.75rem;
      }
    }

    .icon-ristek {
      max-width: 1.85rem;
      max-height: 1.85rem;
    }

    .icon-pmb {
      max-width: 5.25rem;
      max-height: 2.25rem;
    }

    .hamburger-image {
      width: 2rem;
      height: 2rem;
    }
  }
`;

export const PopUpStyle = styled.div`
  width: 250px;
  height: 300px;

  .image_container {
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    margin-bottom: 1.5rem;

    img {
      max-height: 6rem;
      object-fit: cover;
      object-position: center;
    }
  }
  .message {
    font-weight: ${props => props.theme.weight.bold};

    .top_text {
      font-size: 0.75rem;
      margin-left: 2rem;
    }

    span {
      font-size: 0.7rem;
      text-align: center;
      margin-left: 3rem;
    }
  }
`;

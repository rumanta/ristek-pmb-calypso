/* eslint-disable no-unused-vars */
import React, { Component } from "react";
import { NavBarStyle, PopUpStyle } from "./style";
import IconRistek from "../../assets/logos/ristek-logo.png";
import IconPMB from "../../assets/logos/pmb-logo.png";
import IconBell from "assets/icons/bell.png";
import IconPie from "../../assets/icons/pie-chart.png";
import IconGallery from "../../assets/icons/gallery.png";
import IconSpeech from "../../assets/icons/speech-bubble.png";
import IconAbout from "../../assets/icons/about.png";
import IconProfile from "../../assets/icons/profile.png";
import IconLogout from "../../assets/icons/logout.png";
import IconHamburger from "../../assets/icons/NavBar.png";
import IconFeedback from "../../assets/icons/feedback.png";
import SadPicture from "../../assets/icons/sad.png";
import placeholderPhotoProf from "assets/icons/placeholderPhotoProf.png";

import SideMenu from "../SideMenu";

import { withRouter } from "react-router-dom";
import { compose } from "redux";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { loggingOutApi } from "api";
import { logout } from "globalActions";
import { push } from "connected-react-router";
import Card from "../Card";
import swal from "sweetalert";

import { fetchProfile } from "containers/ProfilePage/actions";

class NavBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isDesktop: true,
      isOpened: false,
      isMaintenance: false
    };
    this.changePage = this.changePage.bind(this);
  }

  UNSAFE_componentWillMount() {
    this.resize();
  }

  componentDidMount() {
    window.addEventListener("resize", this.resize.bind(this));
    this.props.fetchProfile();
  }

  resize = () => {
    this.setState({ isDesktop: window.innerWidth >= 1024 });
    this.setState({
      isOpened: !this.state.isDesktop && this.state.isOpened
    });
  };

  openMenu = () => {
    this.setState({ isOpened: !this.state.isOpened });
  };

  maintenancePopUp = () => {
    this.setState({ isMaintenance: !this.state.isMaintenance });
  };

  handleLogout() {
    const logoutWindow = window.open(loggingOutApi, "_self");
    const getUserDataInterval = setInterval(() => {
      if (logoutWindow.closed) {
        clearInterval(getUserDataInterval);
      }

      logoutWindow.postMessage("MadeByWebdev2019", loggingOutApi);
    }, 1000);

    this.props.logout();
  }

  // eslint-disable-next-line consistent-return
  changePage(page) {
    const { isOpened } = this.state;
    const currentlyMaintenanceRoute = [
      "/statistics",
      "/elemen",
      "/gallery",
      "/statistics"
    ];
    let allowChangePage = true;

    for (const maintenanceRoute of currentlyMaintenanceRoute) {
      if (maintenanceRoute === page) {
        allowChangePage = false;

        return swal({
          title: "Error",
          text: "Page is currently unavailable",
          icon: "error",
          button: "ok"
        });
      }
    }

    if (allowChangePage) {
      if (isOpened) {
        this.openMenu();
      }
      this.props.push(page);
    }
  }

  // eslint-disable-next-line consistent-return
  currentURLCheck(url) {
    const grey = { color: "grey" };
    if (
      url.replace(/[^a-zA-Z-]/g, "") ===
      this.props.location.pathname.replace(/[^a-zA-Z-]/g, "")
    ) {
      return grey;
    }
  }

  render() {
    const sideMenu = (
      <SideMenu
        isOpened={this.state.isOpened}
        closeMenu={this.openMenu}
        onLogout={this.handleLogout}
        changePage={this.changePage}
      />
    );

    return (
      <NavBarStyle isDesktop={this.state.isDesktop}>
        <header>
          <div className="header-container">
            <div className="image-logo">
              <a
                className="link-ristek"
                href="https://ristek.cs.ui.ac.id"
                target="__blank"
              >
                <div className="ristek-logo">
                  <img
                    className="icon-ristek"
                    src={IconRistek}
                    alt="icon ristek"
                  />
                </div>
              </a>
              <div onClick={() => this.changePage("/")} className="pmb-logo">
                <img className="icon-pmb" src={IconPMB} alt="icon pmb" />
              </div>
            </div>
            {this.state.isDesktop ? (
              <React.Fragment>
                <div className="main-menu">
                  <a
                    style={this.currentURLCheck("/tasks")}
                    onClick={() => this.changePage("/tasks")}
                  >
                    Task
                  </a>
                  <a
                    style={this.currentURLCheck("/events")}
                    onClick={() => this.changePage("/events")}
                  >
                    Event
                  </a>
                  <a
                    style={this.currentURLCheck(`/announcements`)}
                    onClick={() => this.changePage("/announcements")}
                  >
                    Announcement
                  </a>
                  <a
                    style={this.currentURLCheck("/friends")}
                    onClick={() => this.changePage("/friends")}
                  >
                    Friends
                  </a>
                  <a
                    style={this.currentURLCheck("/stories")}
                    onClick={() => this.changePage("/stories")}
                  >
                    Story
                  </a>
                  <div className="dropdown-wrapper menu-button dropdown-profile">
                    <a href="#">More&nbsp;&#9660;</a>
                    <div className="drop-menu">
                      <a
                        className="menu-button"
                        onClick={() => this.changePage("/statistics")}
                      >
                        <img
                          className="dropdown-image"
                          src={IconPie}
                          alt="pie-chart"
                        />
                        Statistics
                      </a>
                      <a
                        className="menu-button"
                        onClick={() => this.changePage("/gallery")}
                      >
                        <img
                          className="dropdown-image"
                          src={IconGallery}
                          alt="gallery"
                        />
                        Gallery
                      </a>
                      <a
                        className="menu-button"
                        onClick={() => this.changePage("/elemen")}
                      >
                        <img
                          className="dropdown-image"
                          src={IconSpeech}
                          alt="speech-bubble"
                        />
                        Apa Kata Elemen
                      </a>
                      <a
                        className="menu-button"
                        onClick={() => this.changePage("/need-help")}
                      >
                        <img
                          className="dropdown-image"
                          src={IconFeedback}
                          alt="speech-bubble"
                        />
                        Need Help
                      </a>
                      <a
                        className="menu-button"
                        onClick={() => this.changePage("/about")}
                      >
                        <img
                          className="dropdown-image"
                          src={IconAbout}
                          alt="about"
                        />
                        &nbsp;&nbsp;About
                      </a>
                    </div>
                  </div>
                </div>
              </React.Fragment>
            ) : (
              <h1 />
            )}
            <div className="navbar-profile">
              <div
                className="notification"
                onClick={() => this.changePage("/notifications")}
              >
                <img
                  className="notification-image"
                  src={IconBell}
                  alt="notification-image"
                />
              </div>
              {this.state.isDesktop ? (
                <React.Fragment>
                  <div className="profile-container">
                    <div
                      className="profile-image"
                      style={{
                        backgroundImage: `url(${
                          this.props.profil.foto
                            ? this.props.profil.foto
                            : placeholderPhotoProf
                        })`
                      }}
                    />
                    <div className="drop-down-profile" />
                  </div>
                  <div className="dropdown-wrapper menu-button dropdown-profile">
                    <a className="profile-a" href="#">
                      &#9660;
                    </a>
                    <div className="drop-menu drop-menu-profile">
                      <a
                        className="menu-button"
                        onClick={() => this.changePage("/profile")}
                      >
                        <img
                          className="dropdown-image"
                          src={IconProfile}
                          alt="profile"
                        />
                        Profile
                      </a>
                      <a
                        className="menu-button"
                        onClick={() => this.handleLogout()}
                      >
                        <img
                          className="dropdown-image"
                          src={IconLogout}
                          alt="logout"
                        />
                        Logout
                      </a>
                    </div>
                  </div>
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <div className="notification">
                    <img
                      className="hamburger-image"
                      src={IconHamburger}
                      alt="hamburger-image"
                      onClick={this.openMenu}
                    />
                  </div>
                </React.Fragment>
              )}
            </div>
          </div>
        </header>
        {!this.state.isDesktop ? sideMenu : <React.Fragment />}
        {this.state.isMaintenance ? (
          <React.Fragment>
            <div className="Black__Background" onClick={this.maintenancePopUp}>
              <div className="PopUp__Container">
                <PopUpStyle>
                  <Card>
                    <div className="image_container">
                      <img src={SadPicture} />
                    </div>
                    <div className="message">
                      <p>
                        <span className="top_text">
                          Currently Under Development :)
                        </span>{" "}
                        <br /> <span>(Press anywhere to close)</span>
                      </p>
                    </div>
                  </Card>
                </PopUpStyle>
              </div>
            </div>
          </React.Fragment>
        ) : (
          <React.Fragment />
        )}
      </NavBarStyle>
    );
  }
}

NavBar.propTypes = {
  logout: PropTypes.func.isRequired,
  push: PropTypes.func.isRequired,
  location: PropTypes.shape({
    pathname: PropTypes.string
  }),
  profil: PropTypes.shape().isRequired,
  fetchProfile: PropTypes.func.isRequired
};

function mapStateToProps(state) {
  return {
    profil: state.profileReducer.get("profil").toJS()
  };
}

function mapDispatchToProps(dispatch) {
  return {
    logout: () => dispatch(logout()),
    push: url => dispatch(push(url)),
    fetchProfile: () => dispatch(fetchProfile())
  };
}

export default compose(
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(NavBar);

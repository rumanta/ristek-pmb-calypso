import React from "react";
import { FeedbackCardContainer } from "./style";
import { PropTypes } from "prop-types";
import Card from "components/Card";

export default class FeedbackCard extends React.Component {
  renderContent = detail => (
    <div className="content">
      <p className="feedContent">{detail.contents}</p>
      <img className="feedImage" src={detail.image} />
    </div>
  );

  dateFormatter = date => {
    const months = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec"
    ];
    const newDate = new Date(date);

    return `${
      months[newDate.getMonth()]
    } ${newDate.getDate()} • ${this.timeFormatter(
      newDate.getHours()
    )}.${this.timeFormatter(newDate.getMinutes())}`;
  };

  timeFormatter = minute => (minute < 10 ? `0${minute}` : minute);

  render() {
    const {
      title,
      content,
      image_link,
      initial,
      created_at
    } = this.props.feedback;

    const contentData = {
      contents: content,
      image: image_link
    };

    return (
      <FeedbackCardContainer>
        <Card>
          <div className="titleSection">
            <h1>{title}</h1>
          </div>
          <div className="feed">{this.renderContent(contentData)}</div>
          <div className="footer">
            <p className="date">{this.dateFormatter(created_at)}</p>
            <p className="initial">- {initial} -</p>
          </div>
        </Card>
      </FeedbackCardContainer>
    );
  }
}

FeedbackCard.propTypes = {
  feedback: PropTypes.shape
};

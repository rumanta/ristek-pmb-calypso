import styled from "styled-components";

export const FeedbackCardContainer = styled.div`
  .titleSection {
    width: 100%;
    margin-bottom: 1rem;
  }

  .titleSection h1 {
    margin: 0rem;
    color: ${props => props.theme.colors.blue};
  }

  .feedContent {
    display: flex;
    flex-direction: column;
  }

  .feedContent div {
    align-items: center;
    width: 100%;
    justify-contents: center;
  }

  .content {
    width: 100%;
    display: flex;
    flex-direction: column;
    margin-bottom: 1.5rem;
  }

  .content .feedImage {
    margin-right: 0.75rem;
    margin-top: 0.5rem;
    width: 20rem;
  }

  .feedContent {
    margin-bottom: 0;
  }

  .content p {
    font-size: 1rem;
    color: ${props => props.theme.colors.primaryBlack};
  }

  .date {
    color: ${props => props.theme.colors.gray};
    justify-content: flex-end;
    text-align: left;
  }
  .initial {
    color: ${props => props.theme.colors.gray};
    justify-content: flex-end;
    text-align: right;
  }

  @media (max-width: 1216px) {
  }

  @media (max-width: 1024px) {
  }

  @media (max-width: 580px) {
  }

  @media (max-width: 480px) {
    .titleSection h1 {
      font-size: 1.5rem;
    }
    .content p {
      font-size: 0.75rem;
    }
    .date {
      font-size: 0.5rem;
      display: flex;
      flex-direction: column;
    }

    .initial {
      font-size: 0.5rem;
      display: flex;
      flex-direction: column;
    }

    .content .feedImage {
      width: 15rem;
    }
  }

  @media (max-width: 350px) {
    .content .feedImage {
      width: 12rem;
    }
  }
`;

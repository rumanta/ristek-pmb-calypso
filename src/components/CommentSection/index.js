import React, { Component } from "react";
import { CommentSectionContainer } from "./style";
import { isEmpty } from "lodash";
import PropTypes from "prop-types";
import CommentCard from "../CommentCard";
import TextEditor from "../TextEditor";
import EmptyCard from "components/EmptyCard";

class CommentSection extends Component {
  renderCommentCards = () => {
    if (isEmpty(this.props.comments)) {
      return <EmptyCard text="There isn't any comment yet" />;
    }

    return this.props.comments.map(comment => (
      <CommentCard
        key={`Comment-card-${comment.user.user_detail.profil.nama_lengkap}-${
          comment.created_at
        }`}
        name={comment.user.user_detail.profil.nama_lengkap}
        title={comment.user.user_detail.title}
        picture={comment.user.user_detail.profil.foto}
        time={comment.created_at}
        body={comment.comment}
      />
    ));
  };

  changeStyleIfCommentsEmpty = () => {
    if (isEmpty(this.props.comments)) {
      return {
        background: "none",
        boxShadow: "none",
        padding: "1rem 0"
      };
    }

    return null;
  };

  render() {
    return (
      <CommentSectionContainer>
        <div style={{ width: "100%" }}>
          <h2 className="header">Question and Answer</h2>
          <div
            className="comments-container"
            style={this.changeStyleIfCommentsEmpty()}
          >
            {this.renderCommentCards()}
          </div>
          <TextEditor onSubmitComment={this.props.onSubmitComment} />
        </div>
      </CommentSectionContainer>
    );
  }
}

CommentSection.propTypes = {
  comments: PropTypes.arrayOf(PropTypes.shape).isRequired,
  onSubmitComment: PropTypes.func.isRequired
};

export default CommentSection;

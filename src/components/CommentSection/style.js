import styled from "styled-components";

export const CommentSectionContainer = styled.div`
  width: 100%;

  .comments-container {
    padding: 0.5rem 1rem 1rem 1rem;
    background: white;
    box-shadow: ${props => props.theme.boxShadow};
    border-radius: 10px;
    overflow-y: scroll;
    max-height: 420px;
    z-index: 3;
    position: relative;

    /* width */
    ::-webkit-scrollbar {
      width: 9px;
    }

    /* Handle */
    ::-webkit-scrollbar-thumb {
      background: #7fbbdd;
    }
  }

  .header {
    font-size: 20px;
    padding-top: 1.5rem;
    padding-bottom: 1rem;
    font-weight: ${props => props.theme.weight.regular};
  }

  @media (max-width: 460px) {
    .comments-container {
      height: auto;
    }
  }
`;

import React, { useState } from "react";
import TokenContainer from "./style";
import PropTypes from "prop-types";
import Card from "components/Card";
import ProgressBar from "components/ProgressBar";
import moment from "moment-timezone";
import FullscreenIcon from "assets/fullscreen-icon.svg";
import HourglassIcon from "assets/hourglass.svg";
import CloseIcon from "assets/icons/close.svg";
import { isEmpty } from "lodash";

const TokenGeneratedDefault = ({ time, token, percentage }) => {
  const [isFullscreen, setFullscreen] = useState(false);

  if (isFullscreen) {
    return (
      <TokenContainer>
        <div className="overlay">
          <div className="overlay-content">
            <div className="wrapper">
              <h1 className="token">{isEmpty(token) ? "------" : token}</h1>
              <ProgressBar percentage={percentage} />
              <div className="bottom-section">
                <img
                  className="img-close"
                  src={CloseIcon}
                  onClick={() => setFullscreen(false)}
                />
                <div className="clock-cont">
                  <img className="img-hourglass" src={HourglassIcon} />
                  <Clock time={time} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </TokenContainer>
    );
  }

  return (
    <TokenContainer>
      <Card>
        <div className="cardContainer">
          <h1 className="token">{isEmpty(token) ? "------" : token}</h1>
          <ProgressBar percentage={percentage} />
          <div className="bottom-section">
            <img
              className="img-fullscreen"
              src={FullscreenIcon}
              onClick={() => setFullscreen(true)}
            />
            <div className="clock-cont">
              <img className="img-hourglass" src={HourglassIcon} />
              <Clock time={time} />
            </div>
          </div>
        </div>
      </Card>
    </TokenContainer>
  );
};

const Clock = ({ time }) => {
  function convertToTime(difference) {
    const durationFormat = moment.duration(difference, "seconds");

    if (difference <= 0) {
      return "00:00";
    }

    const minute =
      durationFormat.get("minutes") > 9
        ? durationFormat.get("minutes")
        : `0${durationFormat.get("minutes")}`;

    const seconds =
      durationFormat.get("seconds") > 9
        ? durationFormat.get("seconds")
        : `0${durationFormat.get("seconds")}`;

    return `${minute}:${seconds}`;
  }

  return (
    <TokenContainer>
      <div className="ServerTimeCard__time">
        {time ? convertToTime(time) : "----"}
      </div>
    </TokenContainer>
  );
};

TokenGeneratedDefault.propTypes = {
  time: PropTypes.number,
  token: PropTypes.string,
  percentage: PropTypes.number
};

Clock.propTypes = {
  time: PropTypes.number
};

export default TokenGeneratedDefault;

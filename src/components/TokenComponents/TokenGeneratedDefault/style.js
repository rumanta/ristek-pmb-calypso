import styled from "styled-components";

const TokenContainer = styled.div`
  .cardContainer {
    width: 100%;
    display: flex;
    flex-direction: column;
  }

  .overlay {
    height: 100%;
    width: 100%;
    position: fixed;
    z-index: 100;
    left: 0;
    top: 0;
    background-color: ${props => props.theme.colors.boneWhite};
    overflow-x: hidden;

    .token {
      font-size: 125px;
    }

    .ServerTimeCard__time {
      width: 109px;
      font-size: 40px;
    }

    .img-close {
      width: 2rem;
    }

    .img-hourglass {
      width: 1.5rem;
    }

    .wrapper {
      padding: 0 2.5rem;
    }
  }

  .overlay-content {
    position: relative;
    top: 30%;
    width: 100%;
    margin-top: 30px;
  }

  @media (max-width: 840px) and (orientation: landscape) {
    .overlay-content {
      top: 10%;
    }
  }

  .token {
    margin: 0;
    font-size: 42px;
    font-weight: 900;
    color: ${props => props.theme.colors.primaryBlack};
    font-weight: ${props => props.theme.weight.bold};
    margin-bottom: 0.5rem;
  }

  .bottom-section {
    margin-top: 1rem;
    display: flex;
    justify-content: space-between;
  }

  .clock-cont {
    display: flex;
    align-items: center;
  }

  .img-close {
    width: 1.4rem;
    cursor: pointer;
  }

  .img-fullscreen {
    cursor: pointer;
  }

  .ServerTimeCard__time {
    color: ${props => props.theme.colors.black};
    font-weight: ${props => props.theme.weight.regular};
    font-size: 24px;
    margin-left: 0.3rem;
    width: 65px;

    @media screen and (max-width: 500px) {
      width: 44px;
      font-size: 16px;
    }
  }

  @media screen and (max-width: 500px) {
    .overlay {
      .token {
        font-size: 80px;
      }

      .wrapper {
        padding: 0 1rem;
      }

      .img-hourglass {
        width: 1.2rem;
      }
    }
    .cardContainer {
      .token {
        font-size: 30px;
      }

      p {
        font-size: 12px;
      }
    }

    .img-fullscreen {
      width: 17px;
    }

    .img-hourglass {
      width: 10px;
    }
  }
`;

export default TokenContainer;

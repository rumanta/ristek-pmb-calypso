import styled from "styled-components";

const TokenGeneratorContainer = styled.div`
  .body-cont {
    display: flex;
    flex-direction: column;
    align-items: center;
  }

  .desc-text {
    color: ${props => props.theme.colors.secondaryBlack};
  }

  .input-cont {
    display: flex;
    justify-content: space-between;
    margin: 1rem 0;
  }

  .input-field {
    border: 1px solid ${props => props.theme.colors.blue};
    background: ${props => props.theme.colors.fieldGray};
    border-radius: 2px;
    width: 50%;
    text-align: center;
  }

  input[type="number"] {
    -moz-appearance: textfield; /* Firefox */
  }

  input[type="number"]::-webkit-outer-spin-button,
  input[type="number"]::-webkit-inner-spin-button {
    /* display: none; <- Crashes Chrome on hover */
    -webkit-appearance: none;
  }

  .button {
    color: ${props => props.theme.colors.darkBlue};
    border-radius: 50%;
    width: 36px;
    height: 36px;
    border: 1px solid ${props => props.theme.colors.blue};
    box-sizing: border-box;
    text-align: center;
    line-height: 32px;
    cursor: pointer;

    p {
      font-size: 28px;
    }
  }

  .submit-btn {
    border: 1px solid ${props => props.theme.colors.blue};
    color: ${props => props.theme.colors.primaryBlack};
    box-sizing: border-box;
    border-radius: 4px;
    padding: 0.5rem;
    font-size: 12px;
    cursor: pointer;
  }

  @media (max-width: 880px) {
    .desc-text {
      font-size: 14px;
    }
  }

  @media (max-width: 360px) {
    .desc-text {
      font-size: 13px;
    }
  }
`;

export default TokenGeneratorContainer;

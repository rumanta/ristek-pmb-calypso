/* eslint-disable no-confusing-arrow */
import styled from "styled-components";

export const ButtonInputContainer = styled.div`
@import url("https://fonts.googleapis.com/css?family=Rubik:400,500,700&display=swap");

  input[type="file"] {
    display: none;
  }

  .uploadLabel {
    width: ${props => props.wide};
    height: 2.5rem;
    border: 2px solid ${props =>
      props.delete ? props.theme.colors.deleteRed : props.theme.colors.blue};
    box-sizing: border-box;
    border-radius: 5px
    background: #FFFFFF;
    line-height: 19px;
    display: flex;
    align-items: center;
    text-align: center;
    flex-direction: row;
    justify-content: center;
    font-family: Rubik;
    font-style: normal;
    font-weight: ${props => props.theme.weight.regular};
    font-size: 1rem;
    color: #333333;
    transition: 0.3s;
  }

  .uploadLabel:hover {
    background: ${props =>
      props.delete ? props.theme.colors.deleteRed : props.theme.colors.blue};
    color: #FFFFFF;
  }

  .uploadLabel:focus {
    outline: 0;
  }

@media only screen and (max-width: 1000px) {
  .uploadLabel {
    height: 3rem;
    font-size: 1rem;
  }
}

@media (max-width: 580px) {
  .uploadLabel {
    font-size: 0.75rem;
  }
}

@media (max-width: 480px) {
  .uploadLabel {
    height: 1.5rem;
    /* font-size: 0.6rem; */
    border-width: thin;
  }
}

`;

import React, { Component } from "react";
import PropTypes from "prop-types";
import { ButtonInputContainer } from "./style";

export class ButtonInput extends Component {
  render() {
    return (
      <ButtonInputContainer wide={this.props.wide}>
        <label className="uploadLabel">
          <input
            type="file"
            name="file"
            onChange={this.props.onChange}
            accept={this.props.accept}
          />
          {this.props.name}
        </label>
      </ButtonInputContainer>
    );
  }
}

ButtonInput.propTypes = {
  wide: PropTypes.string,
  name: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  accept: PropTypes.string
};

export default ButtonInput;

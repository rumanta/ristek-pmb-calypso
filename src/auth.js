/* eslint-disable sort-keys, valid-jsdoc, lines-around-comment */

import axios from "axios";

const auth = {
  /**
   * Logs a user in, returning a promise with `true` when done
   * @param  {string} email The email of the user
   * @param  {string} password The password of the user
   */
  login(user) {
    if (auth.loggedIn()) {
      return Promise.resolve(true);
    }

    localStorage.pmb2019 = JSON.stringify(user);

    return Promise.resolve(true);
  },

  /**
   * Logs the current user out
   */
  logout() {
    localStorage.removeItem("pmb2019");
    localStorage.removeItem("generated_token_pmb_2019");
    Reflect.deleteProperty(axios.defaults.headers.common, "Authorization");
  },

  /**
   * Checks if a user is logged in
   */
  loggedIn() {
    const notUser = Boolean(localStorage.pmb2019);

    return notUser ? JSON.parse(localStorage.pmb2019) : false;
  }
};

export default auth;

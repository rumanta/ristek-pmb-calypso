export const getNamaAngkatan = tahun => {
  const angkatan = {
    2013: "Angklung",
    2014: "Orion",
    2015: "Capung",
    2016: "Omega",
    2017: "Tarung",
    2018: "Quanta"
  };

  if (!(tahun in angkatan)) {
    return "Fasilkom";
  }

  return angkatan[tahun];
};

/* eslint-disable prettier/prettier */
import { combineReducers } from "redux";
import { routerReducer } from "connected-react-router";
import globalReducers from "globalReducers";

export default combineReducers({
  global: globalReducers,
  routing: routerReducer
});
